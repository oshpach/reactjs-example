import axios from 'axios';
import Account from '../Account';
import * as aws from '../Account/aws'
const jwtDecode = require('jwt-decode');
let environment = null;
let config = null;

export function getAuthenticationHeader() {
    return aws.getIdToken()
        .then((token) => `Bearer ${token}`)
        .catch(() => '');
}

export function getMediaUrl(uri) {
    return config.mediaUrl + uri;
}

export function getMediaUrlNew(uri) {
    return config.mediaUrl + uri;
}

export function authHeaders() {
    return getAuthenticationHeader()
        .then((authHeader) => {
            return {
                'Accept': "application/json",
                'Authorization': authHeader,
                'Content-Type': "application/json"
            };
        })
        .catch(() => {
            return {
                'Accept': "application/json",
                'Content-Type': "application/json"
            };
        })
}

export function userAuthenticated() {
    return aws.isUserAuthenticated();
}

export function initEnvironment(store) {

    environment = process.env.REACT_APP_ENVIRONMENT || 'local';
    config = require(`./config.${environment}.js`);

    axios.defaults.baseURL = config.webApiUrl;
    axios.defaults.headers.common = {};
    axios.defaults.headers.put = {};
}

export function requireAuth(store) {  // this is called by the router only - ie. not ajax calls.
    return function (nextState, replace) {

        if (!userAuthenticated()) {
            replace({
                // Redirect to index to prevent unauthorized access to user pages
                pathname: '/',
                // store where to redirect after successful login
                state: { redirectPath: nextState.location }
            });
            
            store.dispatch(Account.actions.account.showLoginModal());
        }
    }
}

export function getUserId() {
   return aws.getIdToken()
        .then((token) => {
            const decode = jwtDecode(token);
            return Promise.resolve(decode['cognito:username'])
        })
        .catch(() => {
            return Promise.reject('');
        })
}

