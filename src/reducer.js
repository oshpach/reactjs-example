import { combineReducers } from 'redux';
import {routerReducer} from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';

import Account from './Account';
import Tributes from './Tributes';

const rootReducer = combineReducers({
    account: combineReducers({
        login: Account.reducers.login,
        register: Account.reducers.register,
        user: Account.reducers.user
    }),

    ...Tributes.reducers,
    routing: routerReducer,
    form: formReducer
});

export default rootReducer;