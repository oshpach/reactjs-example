// Actions
import * as actions from './actions';

// Components
import * as components from './components';

// Reducer
import {default as reducers} from './reducers';

// Selectors
import * as selectors from './selectors';

export default { actions, components, reducers, selectors};