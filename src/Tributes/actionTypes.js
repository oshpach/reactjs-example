export const REQUEST_BEGIN = '@@liveon/tributes/REQUEST_BEGIN';
export const REQUEST_END = '@@liveon/tributes/REQUEST_END';

export const ADD_TRIBUTE = '@@liveon/tributes/ADD_TRIBUTE';
export const REMOVE_TRIBUTE = '@@liveon/tributes/REMOVE_TRIBUTE';

export const SUCCESS_TRIBUTE = '@@liveon/tributes/SUCCESS_TRIBUTE';

export const SUCCESS_CREATE_TRIBUTE = '@@liveon/tributes/SUCCESS_CREATE_TRIBUTE';

export const SUCCESS_SAVE_TRIBUTE_PART = '@@liveon/tributes/SUCCESS_SAVE_TRIBUTE_PART';

export const SUCCESS_TRIBUTE_MEDIA = '@@liveon/tributes/SUCCESS_TRIBUTE_MEDIA';

export const REMOVE_TRIBUTE_MEDIA = '@@liveon/tributes/REMOVE_TRIBUTE_MEDIA';

export const SUCCESS_CONNECT_TO_TRIBUTE = '@@liveon/tributes/SUCCESS_CONNECT_TO_TRIBUTE';
export const SUCCESS_DISCONNECT_TO_TRIBUTE = '@@liveon/tributes/SUCCESS_DISCONNECT_TO_TRIBUTE';

export const SUCCESS_GET_CONNECT_STATUS = '@@liveon/tributes/SUCCESS_GET_CONNECT_STATUS';
export const REMOVE_CONNECT_STATUS = '@@liveon/tributes/REMOVE_CONNECT_STATUS';

export const REMOVE_ALBUM = '@@liveon/tributes/REMOVE_ALBUM';
export const REMOVE_ALBUM_MEDIA = '@@liveon/tributes/REMOVE_ALBUM_MEDIA';

export const SUCCESS_GET_TRIBUTE = '@@liveon/albums/SUCCESS_GET_TRIBUTE';

export const SUCCESS_GET_TRIBUTE_PART = '@@liveon/albums/SUCCESS_GET_TRIBUTE_PART';

export const SUCCESS_GET_ALBUM = '@@liveon/albums/SUCCESS_GET_ALBUM';

export const SUCCESS_CREATE_ALBUM = '@@liveon/albums/SUCCESS_CREATE_ALBUM';

export const SUCCESS_SAVE_ALBUM_MEDIA = '@@liveon/albums/SUCCESS_SAVE_ALBUM_MEDIA';

export const SUCCESS_GET_ALBUM_MEDIA = '@@liveon/albums/SUCCESS_GET_ALBUM_MEDIA';

export const SUCCESS_GET_COMMENTS = '@@liveon/albums/SUCCESS_GET_COMMENTS';

export const SUCCESS_SAVE_COMMENT = '@@liveon/tributes/SUCCESS_SAVE_COMMENT';

export const SUCCESS_EDIT_COMMENT = '@@liveon/tributes/SUCCESS_EDIT_COMMENT';

export const SUCCESS_GET_COUNT_OF_COMMENTS = '@@liveon/tributes/SUCCESS_GET_COUNT_OF_COMMENTS';

export const SUCCESS_REMOVE_COMMENT = '@@liveon/tributes/SUCCESS_REMOVE_COMMENT';

export const SUCCESS_GET_ALBUMS = '@@liveon/tributes/SUCCESS_GET_ALBUMS';

export const SUCCESS_CROP_ALBUM_IMAGE = '@@liveon/tributes/SUCCESS_CROP_ALBUM_IMAGE';

export const SUCCESS_SAVE_TRIBUTE_AVATAR = '@@liveon/tributes/SUCCESS_SAVE_TRIBUTE_AVATAR';
export const SUCCESS_DELETE_TRIBUTE_AVATAR = '@@liveon/tributes/SUCCESS_DELETE_TRIBUTE_AVATAR';
