import moment from 'moment'
import {validDate} from '../../Common/validators';

export default values => {
    const errors = {
        generalInfo: {},
        interests: {},
    };

    //general info validation
    if (!values.firstName || values.firstName.trim() === '') {
        errors.firstName = 'First name is required';
    }

    if (!values.lastName || values.lastName.trim() === '') {
        errors.lastName = 'Last name is required';
    }

    let dateBorn = null;
    let datePassed = null;

    if (values.generalInfo) {
        if (values.generalInfo.dateBorn) {
            if (!validDate(values.generalInfo.dateBorn)) {
                errors.generalInfo.dateBorn = 'Invalid date';
            } else {
                dateBorn = moment(values.generalInfo.dateBorn);
            }
        }

        if (!values.generalInfo || values.generalInfo.datePassed) {
            if (!validDate(values.generalInfo.datePassed)) {
                errors.generalInfo.datePassed = 'Invalid date';
            } else {
                datePassed = moment(values.generalInfo.datePassed)
            }
        }

        if (dateBorn && datePassed && dateBorn > datePassed) {
            errors.generalInfo.datePassed = 'Date must be after date born';
        }
    }

    //interest validation
    if (!values.interests || !values.interests.books) {
        errors.interests.books = 'Books is required';
    }

    return errors;
};