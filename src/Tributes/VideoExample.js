import React from 'react'
import './VideoExample.css'
import { Player, BigPlayButton } from 'video-react';
// Document
// https://video-react.js.org/components/player/


import "../../node_modules/video-react/dist/video-react.css"; // import css

const VideoExample = props => {


    return (
            <Player
                src="https://s3.amazonaws.com/liveon-media-dev/video/cac26a426a6843a580a58ddb2970fe00/cf72f2557c5e4949b6d847ee3940090e.mp4"
                // fluid={false}
                // width={640}
                // height={500}
                style={{position: 'absolute'}}
                playsInline
                // poster="https://lh3.ggpht.com/OOXV4V9YyovafA10xZhq0QgWNwFwCEhMI9kWJ2FDkjMmLa7rDWJmSmnsgOtMWdDGg3A=w300"
            >
                <BigPlayButton position="center" />
            </Player>
    );
};

export default VideoExample;
// {/*<div className="myClass">*/}
// {/*<BigPlayButton position="center" />*/}
// {/*<LoadingSpinner />*/}
// {/*<ControlBar autoHide={false}>*/}
// {/*<PlaybackRateMenuButton*/}
// {/*rates={[2.5, 2, 1.5, 1, 0.5, 0.25]}*/}
// {/*order={7.1}*/}
// {/*/>*/}
// {/*</ControlBar>*/}
// </div>