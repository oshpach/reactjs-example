import React from 'react';
import {Grid, Row,  Col} from 'react-bootstrap';
import {StyleSheet, css} from 'aphrodite/no-important';
import Common from '../../../Common'
import {defaultAvatar} from '../../../Common/images'

const {components: {ImageLoader}} = Common;

const styles = StyleSheet.create({

    messagesTitle: {
        color: '#3498DB',
        fontSize: '34px',
        width: '80%',
        margin: '0 auto'
    },
    notifItem: {
        listStyle: 'none',
        display: 'flex',
        justifyContent: 'space-between'
    },
    photo: {
        borderRadius: '50%',
        width: 50,
        height: 50,
        "@media screen and (max-width: 700px)": {
            width: 40,
            height: 40
        },
        "@media screen and (max-width: 500px)": {
            width: 30,
            height: 30
        }
    },
    photoContainer: {
        width: 60,
        height: 60,
        display: 'flex !important',
        justifyContent: 'center',
        borderRadius: '50%',
        "@media screen and (max-width: 700px)": {
            width: 50,
            height: 50
        },
        "@media screen and (max-width: 500px)": {
            width: 40,
            height: 40
        }
    },
    photoWrapper: {
        alignSelf: 'center'
    },
    description: {
        alignSelf: 'center',
        paddingLeft: 10,
        "@media screen and (max-width: 700px)": {
            fontSize: '14px',
            maxWidth: 300
        },
        "@media screen and (max-width: 500px)": {
            fontSize: '12px',
            maxWidth: '200px'
        }
    },
    flexContainer: {
        display: 'flex'
    },
    time: {
        alignSelf: 'center',
        "@media screen and (max-width: 700px)": {
            fontSize: '14px'
        },
        "@media screen and (max-width: 500px)": {
            fontSize: 10
        }
    },
    listWrapper: {
        width: '80%',
        margin: '50px auto',
        paddingLeft: 0,
        "@media screen and (max-width: 995px)": {
            width: '100%'
        }
    },
    divider: {
        width: '100%',
        display: 'block',
        height: 1,
        backgroundColor: '#CECECE',
        marginTop: 10,
        marginBottom: 10
    },
    uniqueText: {
        color: '#3498DB',
        textDecoration: 'underline'
    }

});



class Notifications extends React.Component{


    render(){
        return(
            <div>
                <Row style={{marginTop: 90}}>
                    <Col lg={12} md={12} sm={12} xs={12}>
                        <h2 className={css(styles.messagesTitle)}><span style={{paddingRight: 5}}><i className="fa fa-bell" aria-hidden="true"></i></span>Notifications</h2>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12} md={12} sm={12} xs={12}>
                        <ul className={css(styles.listWrapper)}>
                            <li className={css(styles.notifItem)}>
                                <div className={css(styles.flexContainer)}>
                                    <ImageLoader containerStyle={{backgroundColor: 'green'}} imageWrapperStyle={css(styles.photoWrapper)} containerClassName={css(styles.photoContainer)} className={css(styles.photo)} previewSrc={defaultAvatar}/>
                                    <p className={css(styles.description)}>
                                        <span className={css(styles.uniqueText)}>John Brew</span> will have a birthday.
                                    </p>
                                </div>
                                <span className={css(styles.time)}>
                                    Tomorrow
                                </span>
                            </li>
                            <li style={{listStyle: 'none'}}>
                                <span className={css(styles.divider)}></span>
                            </li>
                            <li className={css(styles.notifItem)}>
                                <div className={css(styles.flexContainer)}>
                                    <ImageLoader containerStyle={{backgroundColor: 'red'}} imageWrapperStyle={css(styles.photoWrapper)} containerClassName={css(styles.photoContainer)} className={css(styles.photo)} previewSrc={defaultAvatar}/>
                                    <p className={css(styles.description)}>
                                        <span className={css(styles.uniqueText)}>Mellisa Bianca</span> share a new story about <span className={css(styles.uniqueText)}>Muhammad Ali</span>
                                    </p>
                                </div>
                                <span className={css(styles.time)}>Today</span>
                            </li>
                            <li style={{listStyle: 'none'}}>
                                <span className={css(styles.divider)}></span>
                            </li>
                            <li className={css(styles.notifItem)}>
                                <div className={css(styles.flexContainer)}>
                                    <ImageLoader containerStyle={{backgroundColor: '#3498DB'}} imageWrapperStyle={css(styles.photoWrapper)} containerClassName={css(styles.photoContainer)} className={css(styles.photo)} previewSrc={defaultAvatar}/>
                                    <p className={css(styles.description)}>
                                        <span className={css(styles.uniqueText)}>Jesica Brown</span> had a birthday.
                                    </p>
                                </div>
                                <span className={css(styles.time)}>
                                    Three days ago
                                </span>
                            </li>
                        </ul>
                    </Col>
                </Row>
            </div>
        )
    }

}


export default Notifications;