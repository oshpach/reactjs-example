// import React from 'react'
// import {connect} from 'react-redux'
// import {withRouter} from 'react-router'
// import {StyleSheet, css} from 'aphrodite/no-important'
// import classNames from 'classnames'
// import TributeInformationTile from './TributeInformationTile'
// import TributePageTile from './TributePageTile'
// import * as selectors from '../selectors'
// import moment from 'moment'
// import TributeGallery from './TributeGallery'
// import Footer from '../../Footers/Footer'
//
// import actions from '../actions'
// const {media: {saveMediaAttributes, saveMediaComment}} = actions;
//
// const dzStyle = {
//     width: '100%',
//     maxWidth: 600,
//     padding: 15,
//     border: '1px #E0E0E0 solid',
//     boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',
//     borderRadius: 5,
//     margin: '30px auto 0',
//     cursor: 'pointer',
//     backgroundColor: 'white'
// };
//
// const dzActiveStyle = {
//     borderStyle: 'dashed'
// };
//
// const styles = StyleSheet.create({
//     TributeInfo: {
//         marginTop: '50px !important',
//         maxWidth: '100% !important',
//         padding: '0',
//         "@media screen and (max-width: 768px)": {
//             marginTop: '50px !important'
//         }
//     },
//     editButtons: {
//         display: 'none',
//         borderRadius: 5,
//         position: 'absolute',
//         top: 20,
//         right: 20,
//         backgroundColor: '#FAFAFA',
//         border: '#E0E0E0 solid 2px',
//         padding: 10,
//     },
//
//     editTributeButton: {
//         cursor: 'pointer'
//     },
//
//     mainTitle: {
//         color: '#428bca',
//         marginTop: 0
//     },
//
//     faCircle: {
//         color: '#E0E0E0',
//         textShadow: '-1px 0 #A9A9A9, 0 1px #A9A9A9, 1px 0 #A9A9A9, 0 -1px #A9A9A9'  // adds a border around the circle
//     },
//
//     faPicture: {
//         color: 'white'
//     },
//
//
//     image: {
//         height: '108px',
//         width: '108px',
//         display: 'inline-block',
//         borderRadius: '50%'
//     },
//
//     datesText: {
//         color: '#428bca',
//         marginTop: 10
//     },
//
//     summary: {
//         width: 480,
//         margin: '15px auto',
//         textAlign: 'left',
//         border: '#F0F0F0 solid 1px',
//         padding: '10px 15px'
//     },
//     gallery: {
//         marginTop: '100px'
//     },
//     editButton: {
//         position: 'absolute',
//         left: '30px',
//         top: '130px',
//         zIndex: '899'
//     }
// });
//
// export class TributePage extends React.Component {
//     constructor(props) {
//         super(props);
//
//         this.state = {
//             file: this.props.file || (this.props.location && this.props.location.state && this.props.location.state.file) || null,
//
//             tributePageRoute: {
//                 pathname: '/tribute',
//                 state: {tributeId: this.props.tributeId}
//             }
//         };
//
//         // Bind this to functions
//         this.handleDeleteTributeClick = this.handleDeleteTributeClick.bind(this);
//         this.handleDeletePhotoClick = this.handleDeletePhotoClick.bind(this);
//     }
//
//     componentWillMount() {
//         if (!this.props.tributeId) // if no tributeId cannot upload files
//         {
//             this.props.router.push("/dashboard");
//             return;
//         }
//
//         if(!this.props.tribute) {
//              this.props.getTribute();
//         }
//
//         this.props.getAlbums();
//
//         if(this.props.media.length === 0) {
//             this.props.getFiles();
//         }
//     }
//
//     handleDeleteTributeClick() {
//         this.setState({
//             showConfirmModal: true
//         })
//     }
//
//
//     handleDeletePhotoClick(){
//         this.setState({
//             shouldDelete: !this.state.shouldDelete
//         });
//     }
//
//
//
//
//     formatDates(born, passed) {
//         const dateBorn = moment(born, moment.ISO_8601);
//         const datePassed = moment(passed, moment.ISO_8601);
//
//         if (dateBorn.isValid() && datePassed.isValid()) {
//             return <div className={css(styles.datesText)}>{dateBorn.format('LL')} - {datePassed.format('LL')}</div>
//         }
//
//         if (dateBorn.isValid()) {
//             return <div className={css(styles.datesText)}>Born:&nbsp;{dateBorn.format('LL')}&nbsp;
//                 (age {moment().diff(dateBorn, 'years')})</div>
//         }
//
//         if (datePassed.isValid()) {
//             return <div>Died: {dateBorn.format('LL')}</div>
//         }
//     }
//
//     render() {
//         const {tribute = {}, media = [], tributeId=''} = this.props;
//         return (
//             <div>
//
//                 <TributeInformationTile className={css(styles.TributeInfo)} tributeId={tribute.tributeId} allowEdit={true} allowDelete={true}/>
//                 <TributeGallery shouldDelete={this.state.shouldDelete} media={media} tribute={tribute} containerStyle={css(styles.gallery)}/>
//                 <Footer bottom="-250px"/>
//             </div>
//         );
//     }
// }
//
// const mapStateToProps = (state, props) => {
//
//     const tributeId = props.location.state && props.location.state.tributeId;
//
//     return {
//         tributeId,
//         tribute: selectors.tributeById(state, tributeId),
//         media: selectors.tributeMediaById(state, tributeId)
//     };
// };
//
// const mapDispatchToProps = (dispatch, props) => {
//     const tributeId = props.location.state && props.location.state.tributeId;
//
//     return {
//         getTribute: () => {
//             dispatch(actions.tributes.getTribute(tributeId))
//         },
//         getFiles: () => {
//             dispatch(actions.media.getTributeMediaById(tributeId))
//         },
//         saveFile: (file) => {
//             return dispatch(actions.media.saveImage(tributeId, file))
//         },
//         cropImage: (media, crop, scale) => {
//             dispatch(actions.media.cropImage(media.mediaId, crop, scale))
//                 .then(() => {
//                     dispatch(actions.media.getTributeMediaById(media.tributeId))
//                 })
//                 .catch(() => {
//                     alert('error')
//                 })
//         },
//         saveMediaComment: (media, comment) => dispatch(saveMediaComment(media.mediaId, comment)),
//         saveFileAttributes: (media, attributes) => dispatch(saveMediaAttributes(media.mediaId, attributes)),
//         getAlbums: () => {
//             return dispatch(actions.albums.getAlbumsByTributeId(tributeId));
//         }
//     }
// };
//
// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TributePage));
//

import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {DropdownButton, MenuItem} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import classNames from 'classnames'
import {userAuthenticated} from '../../utils'
import ConfirmDeleteModal from './ConfirmDeleteModal'
import actions from '../actions'
import * as selectors from '../selectors'
import TributeGallery from './TributeGallery'
import TributeInformationTile from './TributeInformationTile'
import Footer from '../../Footers/Footer'


const dzStyle = {
    width: '100%',
    maxWidth: 600,
    padding: 15,
    border: '1px #E0E0E0 solid',
    boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',
    borderRadius: 5,
    margin: '30px auto 0',
    cursor: 'pointer',
    backgroundColor: 'white'
};

const dzActiveStyle = {
    borderStyle: 'dashed'
};

const styles = StyleSheet.create({
    TributeInfo: {
        marginTop: '50px !important',
        maxWidth: '100% !important',
        "@media screen and (max-width: 768px)": {
            marginTop: '50px !important'
        }
    },
    marginTop: {
        bottom: '-200px'
    },
    tributePhoto: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        cursor: 'pointer'
    },
    editButton: {
        position: 'absolute',
        right: '30px',
        top: '130px',
        zIndex: '899',
        '@media screen and (max-width: 450px)': {
            top: '105px',
            left: '10px'
        }
    },
    faCircle: {
        color: '#E0E0E0',
        textShadow: '-1px 0 #A9A9A9, 0 1px #A9A9A9, 1px 0 #A9A9A9, 0 -1px #A9A9A9'  // adds a border around the circle
    },

    faPicture: {
        color: 'white'
    },
    photosTitle: {
        textAlign: 'center',
        color: '#3498DB',
        marginTop: '40px',
        marginBottom: '30px',
        fontWeight: 'bold',
        fontSize: '30px'
    }

});


export class DisplayTribute extends React.Component {


    constructor(props){
        super(props);
        this.state = {
            showConfirmModal: false,
            shouldDelete: false
        };
        this.handleDrop = this.handleDrop.bind(this);
        this.handleEditTributeClick = this.handleEditTributeClick.bind(this);
        this.handleCreateAlbumClick = this.handleCreateAlbumClick.bind(this);
        this.handleDeleteTributeClick = this.handleDeleteTributeClick.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
    }


    componentWillMount() {

        window.scrollTo(0, 0);
        // need to do this in case the page is refreshed.

        if (!this.props.tribute) {
            this.props.getTribute();  // get tribute from db
        }

        this.props.getAlbums();

        if (this.props.media.length === 0) {
            this.props.getTributeMedia();  // get content from db
        }
    }

    handleDrop(acceptedFiles, e) {

        acceptedFiles.forEach((file, index) => {
            this.props.saveFile(file)
                .then(() => {
                    if (index + 1 === acceptedFiles.length) {
                        this.props.getFiles();
                    }
                });
        });
    }


    handleEditTributeClick() {
        this.props.router.push({
            pathname: '/edittribute',
            state: {
                tributeId: this.props.tributeId,
                referrer: this.props.location
            }
        })
    }

    handleCreateAlbumClick(tributeId){
        this.props.router.push({
            pathname: '/createalbum',
            state: {
                tributeId: tributeId,
                referrer: this.props.location
            }
        })
    }

    handleDeleteTributeClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.deleteTribute()
            .then(() => {
                this.props.router.push({
                    pathname: '/mytributes'
                })
            });
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    render() {
        console.log(this.props);
        const {media = [], tribute = {}} = this.props;
        return (
            <div>
                {userAuthenticated() && tribute.role === 'owner' ? <div className={css(styles.editButton)}>
                    <DropdownButton style={{background: '#3498DB', border: '1px solid white', color: 'white'}} title={<span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>} id="EditTribute">
                        <MenuItem className={css(styles.dropDown)} onClick={this.handleEditTributeClick} eventKey="1">Modify a Tribute</MenuItem>
                        <MenuItem className={css(styles.dropDown)} onClick={() => {this.handleCreateAlbumClick(tribute.tributeId)}} eventKey="2">Create an album</MenuItem>
                        <MenuItem  divider/>
                        <MenuItem className={css(styles.dropDown)} onClick={this.handleDeleteTributeClick} eventKey="3">Delete</MenuItem>
                    </DropdownButton>
                </div> : ''}
                <TributeInformationTile fontSize="24px" className={css(styles.TributeInfo)} tributeId={this.props.tributeId}/>
                { tribute.role === 'owner' ? <Dropzone
                    onDrop={this.handleDrop}
                    style={dzStyle}
                    activeStyle={dzActiveStyle}
                >
                    <div style={{textAlign: 'center'}}>
                        <span className="fa-stack fa-3x">
                            <i className={classNames(css(styles.faCircle), "fa fa-circle fa-stack-2x")}/>
                            <i className={classNames(css(styles.faPicture), "fa fa-picture-o fa-stack-1x fa-inverse")}/>
                        </span>

                        <div>Drop Images here</div>
                        <div>or click to add</div>
                    </div>
                </Dropzone> : ''}
                {this.props.media.length ? <p className={css(styles.photosTitle)}>PHOTOS/VIDEOS</p> : ''}
                <TributeGallery access="tributePhoto" shouldDelete={this.state.shouldDelete} media={media} tribute={tribute}/>
                <ConfirmDeleteModal title="Delete Tribute" body={<div><p>Confirm delete tribute?</p><p style={{fontWeight: 'bold', color: 'red'}}>This action can not be undone.</p></div>}
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled} show={this.state.showConfirmModal}/>
                <Footer className={css(styles.marginTop)}/>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const tributeId = props.params.tributeSlug;
    return {
        tributeId,
        tribute: selectors.tributeById(state, tributeId),   // need to pass Slug instead of id
        media: selectors.tributeMediaById(state, props.params.tributeSlug)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const tributeId = props.params.tributeSlug;
    return {
        getTribute: () => dispatch(actions.tributes.getTribute(props.params.tributeSlug)),
        getTributeMedia: () => dispatch(actions.media.getTributeMediaById(props.params.tributeSlug, true)),
        getAlbums: () => {
            return dispatch(actions.albums.getAlbumsByTributeId(props.params.tributeSlug));
        },
        deleteTribute: () => {
            return dispatch(actions.tributes.deleteTribute(tributeId));
        },
        saveFile: (file) => {
            return dispatch(actions.media.saveImage(tributeId, file))
        },
        getFiles: () => {
            dispatch(actions.media.getTributeMediaById(tributeId))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayTribute);

