import React from 'react'
import { connect } from 'react-redux'
import { ButtonToolbar, Button } from 'react-bootstrap'
import { StyleSheet, css } from 'aphrodite/no-important'
import TextareaAutosize from 'react-textarea-autosize'
import ConfirmDeleteModal from './ConfirmDeleteModal'
import classNames from 'classnames'
import actions from '../actions'
import { commentsByMediaId } from '../selectors'
import { getUserId } from '../../utils'
import { defaultPersona } from '../../Common/images'
import Common from '../../Common'
const { components: { ImageLoaderFit, ImageCrop, DisplayAvatar, BlockTile }, helpers: { windowDimension } } = Common;

const styles = StyleSheet.create({
    tileContent: {
        width: 480,
        margin: 'auto',

        '@media (max-width: 525px)': {
            width: '100%'
        }
    },

    descriptionButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 50,
        right: 0,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: '10px',
        zIndex: 10
    },

    descriptionContainer: {
        position: 'relative'
    },

    description: {
        resize: 'none',
        display: 'block',
        width: '100%',
        marginBottom: '15px',
        padding: '10px 15px',
        //overflow: 'hidden'
    },

    showDescriptionButtons: {
        display: 'block'
    },

    editButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: 10,
    },

    editImageButton: {
        marginRight: '15px',
        cursor: 'pointer'
    },

    deleteImageButton: {
        cursor: 'pointer'
    },

    imageContainer: {
        position: 'relative',

        ':hover .editButtons': {
            display: 'block'
        }
    },

    image: {
        // height: '100%',
        // maxHeight: '480px',
        // width: '100%',
        // maxWidth: '480px',
        // display: 'block',
        border: '#E0E0E0 solid 2px',
        width: 480,

         '@media (max-width: 530px)': {
                maxWidth: 'calc(100vw - 50px)',
                height: 'auto'
            }
    },

    commentContainer: {
        marginTop: 20
    },

    inputCommentWrapper: {
        overflow: 'hidden',
        paddingRight: 12,
    },

    inputComment: {
        resize: 'none',
        width: '100%',
        padding: '5px 10px',
    },

    buttonSaveComment: {
        float: 'right',
        paddingTop: 9,
        paddingBottom: 9
    },

    commentTextContainer: {
        marginTop: 10,

        ':after': {
            content: '""',
            clear: 'both',
            display: 'block'
        }
    },

    commentAvatarWrapper: {
        float: 'left'
    },

    commentTextWrapper: {
        marginLeft: 40
    },

    commentHr: {
        backgroundColor: '#e0e0e0',
        height: 1,
        marginTop: 25,
        marginBottom: 10
    }
});


export class TributePageTile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showCropEditor: false,
            showDescriptionButtons: false,
            showConfirmModal: false,
            descriptionHeight: 0
        };

        // Bind this to functions
        this.handleDescriptionFocus = this.handleDescriptionFocus.bind(this);
        this.handleDescriptionSave = this.handleDescriptionSave.bind(this);
        this.handleDescriptionCancel = this.handleDescriptionCancel.bind(this);
        this.doShowCropEditor = this.doShowCropEditor.bind(this);
        this.handleDeleteImageClick = this.handleDeleteImageClick.bind(this);
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleImageCancelCrop = this.handleImageCancelCrop.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
        this.saveComment = this.saveComment.bind(this);
        this.handleDescriptionHeightChange = this.handleDescriptionHeightChange.bind(this);
    }

    componentWillMount() {
        this.props.getMediaComments();
    }

    componentDidMount() {
        // getUserId()
        //     .then((userId) => {
        //         this.setState({
        //             userId: userId
        //         })
        //     })
        //     .catch(() => {
        //         this.setState({
        //             userId: null
        //         })
        //     })

        // This is a hack to get the text area box to display correctly onload
        if (this.inputDescription) {
            window.setTimeout(() => this.inputDescription._resizeComponent(), 1);

            this.setState({ descriptionHeight: this.inputDescription.clientHeight })
        }
    }

    handleDescriptionFocus(e) {
        console.log('focus again');
        this.setState({
            descriptionOriginalValue: this.inputDescription.value,
            showDescriptionButtons: true
        })
    }

    handleDescriptionHeightChange(height) {
        this.setState({ descriptionHeight: height });
    }

    handleDescriptionSave(e) {
        if (this.inputDescription.value !== this.state.descriptionOriginalValue) {
            this.props.saveDescription(this.inputDescription.value);
        }

        this.setState({
            showDescriptionButtons: false
        })
    }

    handleDescriptionCancel(e) {
        console.log('canceled');
        this.inputDescription.value = this.state.descriptionOriginalValue;

        this.setState({
            showDescriptionButtons: false
        })
    }

    doShowCropEditor() {
        this.setState({
            showCropEditor: true
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.deleteImage();
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    handleDeleteImageClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    handleCropImage(crop, scale) {
        this.props.onImageCrop(this.props.media, crop, scale);

        this.setState({
            showCropEditor: false
        })
    }

    handleImageCancelCrop() {
        this.setState({
            showCropEditor: false
        })
    }

    editorWidth() {
        return windowDimension().width < 525 ? windowDimension().width - 30 : 480;
    }

    saveComment() {
        const comment = (this.inputComment && this.inputComment.value && this.inputComment.value.trim()) || '';
        if (comment === '') return;

        this.props.saveComment(comment)
            .then(() => {
                this.inputComment.value = ''
            });
    }

    canEditMedia() {
        const { tribute, media } = this.props;
        const isTributeOwnerManager = tribute.role && (tribute.role.toLowerCase() === 'owner' || tribute.role.toLowerCase() === 'manager');

        //const isMediaCreator = media.userId === this.state.userId;

        //return isTributeOwnerManager || isMediaCreator;
        return false;
    }

    render() {
        const { media } = this.props;

        return (
            <BlockTile>
                <div className={css(styles.tileContent)}>
                    <div className={css(styles.descriptionContainer)} onBlur={this.handleDescriptionBlur}>
                        {this.canEditMedia() ?
                            <div>
                                <TextareaAutosize
                                    ref={node => {
                                        this.inputDescription = node
                                    }}
                                    style={styles.description._definition}
                                    minRows={1}
                                    maxRows={6}
                                    placeholder="Enter a description"
                                    defaultValue={media.description || ''}
                                    onFocus={this.handleDescriptionFocus}
                                    onHeightChange={this.handleDescriptionHeightChange}
                                />

                                <div style={{ top: this.state.descriptionHeight }} className={classNames(css(styles.descriptionButtons), this.state.showDescriptionButtons ? css(styles.showDescriptionButtons) : '')}>
                                    <ButtonToolbar>
                                        <Button onClick={this.handleDescriptionSave} bsStyle="primary" bsSize="small">Save</Button>
                                        <Button onClick={this.handleDescriptionCancel} bsSize="small">Cancel</Button>
                                    </ButtonToolbar>
                                </div>
                            </div>
                            :
                            media.description && <div className={css(styles.description)}>{media.description}</div>
                        }

                    </div>
                    <div className={css(styles.imageContainer)}>
                        {!(this.state.showCropEditor) ?
                            (<ImageLoaderFit
                                className={css(styles.image)}
                                src={media.cropUri || media.uri}
                                previewSrc={media.preview || defaultPersona}
                            />)
                            :
                            (<ImageCrop
                                src={"http://liveonmediaupload.s3.amazonaws.com/" + media.uri}
                                height={this.editorWidth()}
                                width={this.editorWidth()}
                                border={0}
                                onCrop={this.handleCropImage}
                                onCancel={this.handleImageCancelCrop}
                            />)
                        }

                        {this.props.onImageCrop && this.canEditMedia &&
                            <div className={classNames(css(styles.editButtons), this.state.showCropEditor ? '' : 'editButtons')}>
                                <i className={classNames(css(styles.editImageButton), "fa fa-pencil-square-o fa-lg")} onClick={this.doShowCropEditor} />
                                <i className={classNames(css(styles.deleteImageButton), "fa fa-trash-o fa-lg")} onClick={this.handleDeleteImageClick} />
                            </div>
                        }
                    </div>

                    <div>
                        {this.props.comments.map(comment => (

                            <div className={css(styles.commentTextContainer)} key={comment.commentId}>
                                <div className={css(styles.commentAvatarWrapper)}>
                                    <DisplayAvatar user={comment.user} size={30} />
                                </div>
                                <div className={css(styles.commentTextWrapper)}>
                                    <div>{comment.comment}</div>
                                </div>

                                <hr className={css(styles.commentHr)} />
                            </div>
                        )
                        )}
                    </div>

                    <div className={css(styles.commentContainer)}>
                        <Button className={css(styles.buttonSaveComment)} onClick={this.saveComment} bsStyle='primary' bsSize='sm'>Save</Button>
                        <div className={css(styles.inputCommentWrapper)}>
                            <TextareaAutosize
                                ref={node => {
                                    this.inputComment = node
                                }}
                                style={styles.inputComment._definition}
                                minRows={1}
                                maxRows={6}
                                placeholder="Enter a comment"
                                defaultValue=''
                            />
                        </div>
                    </div>


                </div>
                <ConfirmDeleteModal title="Delete Image" body="Confirm delete image?" onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled} show={this.state.showConfirmModal} />
            </BlockTile>
        )

    }
}

const mapStateToProps = (state, props) => {
    return {
        comments: commentsByMediaId(state, props.media.mediaId)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getMediaComments: () => {
            return dispatch(actions.media.getMediaComments(props.media.mediaId))
        },

        deleteImage: () => {
            return dispatch(actions.media.deleteImage(props.media.tributeId, props.media.mediaId))
        },

        saveComment: (comment) => {
            return dispatch(actions.media.saveMediaComment(props.media.mediaId, comment))
        },
        saveDescription: (description) => {
            return dispatch(actions.media.saveMediaAttributes(props.media.mediaId, { description }))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TributePageTile);

