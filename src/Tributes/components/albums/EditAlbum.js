import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import AlbumForm from './AlbumForm'
import {SubmissionError} from 'redux-form'
import actions from '../../actions'
import {getAlbumById} from '../../selectors'
import Common from '../../../Common'
const {
    helpers: {responseFormError}
} = Common;


const EditAlbum = props => {

    const referrerPage = () => {
        if (props.location.state.referrer) {
            props.router.push(props.location.state.referrer);
            return;
        }

        props.router.push("/mytributes");
    };

    const handleCancelClick = () => {
        referrerPage();
    };

    const submit = (model) => {
        return props.onSubmit(model)
            .then((album) => {
                referrerPage();
            })
            .catch((error) => {
                throw new SubmissionError(responseFormError(error));
            });
    };

    return (
        <AlbumForm
                albumId={props.albumId}
                initialValues={(props.album && Object.keys(props.album).length > 0 && {...props.album})|| null}
                headerText='Edit Album'
                primaryButtonText='Edit Album'
                onSubmit={submit}
                cancelButtonOnClick={handleCancelClick}
            />
    );
};

const mapStateToProps = (state, props) => {
    const {albumId} = props.location.state;

    return {
        albumId,
        album: getAlbumById(state, albumId)
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSubmit: (model) => {
            return dispatch(actions.albums.createAlbum({...model, albumId: props.location.state.albumId, tributeId: props.location.state.tributeId}))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(EditAlbum));
