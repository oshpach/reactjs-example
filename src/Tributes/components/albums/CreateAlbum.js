import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import AlbumForm from './AlbumForm'
import {SubmissionError} from 'redux-form'
import actions from '../../actions'
import Common from '../../../Common'

const {
    helpers: {responseFormError}
} = Common;


const CreateAlbum = props => {

    const handleCancelClick = () => {
        this.props.router.push("/mytributes");
    };

    const submit = (model) => {
        return props.onSubmit(model)
            .then((album) => {
                props.router.push({
                    pathname: '/tribute/album',
                    state: {
                        albumId: album.albumId,
                        tributeId: album.tributeId
                    }
                });
            })
            .catch((error) => {
                throw new SubmissionError(responseFormError(error));
            });
    };

    return (
        <AlbumForm
            headerText='Create Album'
            primaryButtonText='Create Album'
            onSubmit={submit}
            cancelButtonOnClick={handleCancelClick}
        />
    );
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        onSubmit: (model) => {
            return dispatch(actions.albums.createAlbum({...model, tributeId: props.location.state.tributeId}))
        }
    }
};

export default withRouter(connect(null, mapDispatchToProps)(CreateAlbum));
