import React from 'react';
import {Grid, Col, Row, Modal} from 'react-bootstrap';
import {SubmissionError} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import Dropzone from 'react-dropzone'
import classNames from 'classnames'
import AlbumForm from './AlbumForm'
import Common from '../../../Common'
import './CreateAlbumPopUp.css'

const {
    helpers: {responseFormError},
} = Common;
const styles = StyleSheet.create({
    buttonStyle: {
        backgroundColor: '#3498DB',
        border: '1px solid #3498DB',
        width: 120
    },
    buttonAddPhotos: {
        backgroundColor: 'white',
        border: '1px solid #3498DB',
        color: '#3498DB',
        width: 120
    },
    closeButton: {
        position: 'absolute',
        right: 10,
        top: 0,
        color: 'lightgrey',
        cursor: 'pointer'
    },
    faCircle: {
        color: '#E0E0E0',
        textShadow: '-1px 0 #A9A9A9, 0 1px #A9A9A9, 1px 0 #A9A9A9, 0 -1px #A9A9A9'  // adds a border around the circle
    },

    faPicture: {
        color: 'white'
    },
    hidePhotos: {
        opacity: 0,
        display: 'none',
        transition: 'all ease 0.3s'
    },
    showPhotos: {
        opacity: 1,
        display: 'block',
        transition: 'all ease 0.3s'
    }
});

const dzStyle = {
    width: '100%',
    maxWidth: 600,
    padding: 15,
    border: '1px #E0E0E0 solid',
    boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',
    borderRadius: 5,
    margin: '30px auto 0',
    cursor: 'pointer',
    backgroundColor: 'white'
};

const dzActiveStyle = {
    borderStyle: 'dashed'
};

class CreateAlbumPopUp extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            showPhotos: false
        };
        this.onShowPhotos = this.onShowPhotos.bind(this);
    }

    handleCancelClick = () => {
        this.props.onHide();
    }

   submit = (model) => {
        return this.props.onSubmit(model)
            .then((album) => {
                this.props.router.push({
                    pathname: '/tribute/album',
                    state: {
                        albumId: album.albumId,
                        tributeId: album.tributeId
                    }
                });
            })
            .catch((error) => {
                throw new SubmissionError(responseFormError(error));
            });
    };

    onShowPhotos(){
        this.setState({
            showPhotos: !this.state.showPhotos
        });
    }


    render(){
        const {tribute=[], show, onHide, album = {}} = this.props;
        console.log(this.state);
        return (
            <Modal show={show} onHide={onHide} id="createAlbumPopUp" className={css(styles.wrapper)}>
                <span onClick={this.handleCancelClick} className={css(styles.closeButton)}><i className="fa fa-times" aria-hidden="true"></i></span>
                <AlbumForm
                    buttonStyle={css(styles.buttonStyle)}
                    buttonAddPhotos={css(styles.buttonAddPhotos)}
                    primaryButtonText={album ? 'Edit Album' : 'Create Album'}
                    onSubmit={this.submit}
                    initialValues={(album && Object.keys(album).length > 0 && {...album})|| null}
                    cancelButtonOnClick={this.onShowPhotos}
                    album={album}
                />
                { tribute.role === 'Owner' ? <Dropzone
                    onDrop={this.handleDrop}
                    style={dzStyle}
                    activeStyle={dzActiveStyle}
                    className={this.state.showPhotos ? css(styles.showPhotos) : css(styles.hidePhotos)}
                >
                    <div style={{textAlign: 'center', fontSize: '12px'}}>
                        <span className="fa-stack fa-3x">
                            <i className={classNames(css(styles.faCircle), "fa fa-circle fa-stack-2x")}/>
                            <i className={classNames(css(styles.faPicture), "fa fa-picture-o fa-stack-1x fa-inverse")}/>
                        </span>

                        <div>Drop Images here</div>
                        <div>or click to add</div>
                    </div>
                </Dropzone> : ''}
            </Modal>
        )
    }
};

export default CreateAlbumPopUp;