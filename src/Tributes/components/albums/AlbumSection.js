import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import defaultAlbum from '../../../Common/createTribute/images/default-album.png'
import {StyleSheet, css} from 'aphrodite/no-important'
import Common from '../../../Common'
import actions from '../../actions'
import {getAlbumsByTributeId, tributeMediaByUri, tributeByUri, tributes} from '../../selectors'
const {components: {ImageLoaderFit}} = Common;

const styles = StyleSheet.create({
    wrap: {
        width: '80%',
        margin: '10px auto',
        position: 'relative',
        overflow: 'hidden',
        minHeight: '400px',
        '@media screen and (max-width: 1190px)':{
            minHeight: '350px'
        },
        '@media screen and (max-width: 1000px)':{
            minHeight: '340px'
        },
        '@media screen and (max-width: 950px)':{
            minHeight: '320px'
        },
        '@media screen and (max-width: 900px)':{
            minHeight: '300px'
        },
        '@media screen and (max-width: 850px)':{
            minHeight: '250px'
        },
        '@media screen and (max-width: 750px)':{
            minHeight: '200px'
        },
        '@media screen and (max-width: 747px)':{
            width: '80%',
            height: '710px',
            margin: '4px auto',
        },
        '@media screen and (max-width: 660px)':{
            height: '670px'
        },
        '@media screen and (max-width: 600px)':{
            height: '590px'
        },
        '@media screen and (max-width: 520px)':{
            width: '80%',
            height: '490px'
        },
        '@media screen and (max-width: 450px)':{
            height: '410px'
        }
    },
    first: {
        maxWidth: '50%',
        minHeight: '200px',
        height: '100%',
        width: '50%',
        display: 'inline-block',
        position: 'absolute',
        overflow: 'hidden',
        left: '0',
        top: '0',
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        '@media screen and (max-width: 747px)':{
            width: '100%',
            maxWidth: '100%',
            height: '300px',
            left: '0',
            top: '0',
            right: '0'
        },
        '@media screen and (max-width: 520px)':{
            width: '100%',
            height: '200px'
        },
        '@media screen and (max-width: 450px)':{

        }
    },
    second: {
        maxWidth: '24%',
        height: '49%',
        width: '100%',
        right: '25%',
        position: 'absolute',
        overflow: 'hidden',
        '@media screen and (max-width: 747px)':{
            width: '49%',
            maxWidth: '100%',
            height: '200px',
            left: '0',
            top: '305px'
        },
        '@media screen and (max-width: 660px)':{
            height: '180px'
        },
        '@media screen and (max-width: 600px)':{
            height: '140px'
        },
        '@media screen and (max-width: 520px)':{
            top: '205px'
        },
        '@media screen and (max-width: 450px)':{
            top: '205px',
            height: '100px'
        }
    },
    third: {
        maxWidth: '24%',
        height: '49%',
        width: '100%',
        top: '0',
        right: '0',
        position: 'absolute',
        overflow: 'hidden',
        '@media screen and (max-width: 747px)':{
            width: '50%',
            maxWidth: '100%',
            height: '200px',
            top: '305px'
        },
        '@media screen and (max-width: 660px)':{
            height: '180px'
        },
        '@media screen and (max-width: 600px)':{
            height: '140px'
        },
        '@media screen and (max-width: 520px)':{
            top: '205px'
        },
        '@media screen and (max-width: 450px)':{
            top: '205px',
            height: '100px'
        }
    },
    fourth: {
        maxWidth: '24%',
        height: '49%',
        width: '100%',
        right: '25%',
        bottom: '0',
        position: 'absolute',
        overflow: 'hidden',
        '@media screen and (max-width: 747px)':{
            width: '49%',
            maxWidth: '100%',
            height: '200px',
            left: '0',
            bottom: '0'
        },
        '@media screen and (max-width: 660px)':{
            height: '180px'
        },
        '@media screen and (max-width: 600px)':{
            height: '140px'
        },
        '@media screen and (max-width: 450px)':{
            height: '100px'
        }
    },
    fifth: {
        maxWidth: '24%',
        height: '49%',
        width: '100%',
        right: '0',
        bottom: '0',
        position: 'absolute',
        overflow: 'hidden',
        '@media screen and (max-width: 747px)':{
            width: '50%',
            maxWidth: '100%',
            height: '200px',
            right: '0',
            bottom: '0'
        },
        '@media screen and (max-width: 660px)':{
            height: '180px'
        },
        '@media screen and (max-width: 600px)':{
            height: '140px'
        },
        '@media screen and (max-width: 450px)':{
            height: '100px'
        }
    },
    photo: {
        position: 'absolute',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%'
    },
    hoverBlock: {
        '@media screen and (min-width: 747px)': {
            position: 'absolute',
            top: '0',
            left: '0',
            right: '0',
            bottom: '0',
            background: 'rgba(52, 152, 219, 0)',
            opacity: '0',
            zIndex: '900',
            transition: 'all ease 0.4s',
            ':hover':{
                background: 'rgba(52, 152, 219, 0.7)',
                opacity: '1'
            }
        },
        '@media screen and (max-width: 746px)':{
            position: 'absolute',
            top: '0',
            left: '0',
            right: '0',
            bottom: '0',
            background: 'linear-gradient(to top, black, transparent)',
            zIndex:'900'
        }
    },
    albumsTitle: {
        textAlign: 'center',
        textTransform: 'uppercase',
        marginBottom: '50px',
        marginTop: '50px',
        fontFamily: 'Raleway',
        fontSize: '36px',
        color: '#3498DB'
    },
    imageWrapper: {
        width: '80%',
        margin: '0 auto',
        textAlign: 'center',
        marginTop: '50px'
    },
    imageTribute: {
        borderRadius: '50%',
    },
    albumTitle: {
        color: 'white',
        fontSize: '20px',
        textAlign: 'center',
        position: 'absolute',
        top: '40%',
        left: '50%',
        transform: 'translate(-50%)',
        width: '100%',
        '@media screen and (max-width: 850px)':{
            top: '30%',
            fontSize: '14px'
        },
        '@media screen and (max-width: 746px)': {
            fontSize: '20px'
        },
        '@media screen and (max-width: 600px)': {
            fontSize: '14px',
            top: '20%'
        }
    },
    albumTitleBig: {
        color: 'white',
        fontSize: '35px',
        textAlign: 'center',
        position: 'absolute',
        top: '40%',
        left: '50%',
        transform: 'translate(-50%)',
        width: '100%',
        '@media screen and (max-width: 600px)': {
            top: '20%',
            fontSize: '25px'
        }
    },
    albumButton: {
        position: 'absolute',
        border: '1px solid white',
        borderRadius: '20px',
        fontSize: '12px',
        color: 'white',
        bottom: '20px',
        right: '20px',
        padding: '5px 10px',
        cursor: 'pointer',
        '@media screen and (max-width: 850px)':{
            padding:'4px 7px',
            fontSize:'10px',
            right: '10px'
        },
        '@media screen and (max-width: 746px)':{
            padding: '5px 10px',
            fontSize: '15px',
            right: '20px'
        },
        '@media screen and (max-width: 600px)': {
            fontSize: '12px',
            padding: '3px 7px',
        },'@media screen and (max-width: 450px)':{
            right: '5px',
            bottom: '5px',
            fontSize: '9px'
        }


    },
    albumWrapper: {
        cursor: 'pointer',
    },
    previewPhoto: {
        maxWidth: '100%',
        width: '100%',
    }
});

class AlbumSection extends React.Component{

    constructor(props){
        super(props);
        this.linkToAlbum = this.linkToAlbum.bind(this);
    }


    componentWillMount() {
        // need to do this in case the page is refreshed.

        if (this.props.tributes.length === 0) {
            this.props.getTributes();  // get tribute from db
        } // get tribute from db


        if (this.props.mediaTributes.length === 0) {
            this.props.getTributeMedia(0);  // get content from db
        }
        if(this.props.albums.length === 0){
            this.props.getAlbums();
        }


    }

    getRemnant(index){
        switch ((index+1)%5){
            case 1:
                return css(styles.first);
            case 2:
                return css(styles.second);
            case 3:
                return css(styles.third);
            case 4:
                return css(styles.fourth);
            case 0:
                return css(styles.fifth);
            default :
                return css(styles.first);
        }
    }

    getRangedSrc(arr){


        let urls = arr.map((current)=> current.uri);


        let result = [];
        for(let i = 0; i < urls.length; i += 1){
            if(i % 5 === 0){
                result.push(urls.slice(i, i + 5));
            }
        }
        return result;

    }

    linkToAlbum(album, tribute) {
        console.log(tribute);
        this.props.router.push({
            pathname: `/album/${tribute.tributeUri}/${album.albumUri}`,
            state: {
                albumId: album.albumId,
                tributeId: tribute.tributeId
            }
        });
    };


    render(){
        const {tribute={}, albums = []} = this.props;
        return (
            <div>
                {this.getRangedSrc(albums).map((media, albumIndex)=>{
                    return (<div style={{height: `${this.props.height}`}} className={css(styles.wrap)} key={albumIndex}>
                        {media.map((current, index)=>{
                            return (
                                <div className={this.getRemnant(index) + ` ${css(styles.albumWrapper)}`} key={index} onClick={() => {this.linkToAlbum(albums[index], tribute)}}>
                                    <span className={css(styles.hoverBlock)}>
                                    <p className={index === 0 ? css(styles.albumTitleBig) : css(styles.albumTitle)}>{albums[index].name}</p>
                                    <span className={css(styles.albumButton)}>SEE ALBUM <i className="fa fa-arrow-right"></i></span>
                                    </span>
                                    {current ?
                                        <ImageLoaderFit className={css(styles.photo)} src={current}/>
                                            :
                                    <img
                                        className={css(styles.previewPhoto)}
                                        src={defaultAlbum}
                                        alt=''
                                    />}
                                </div>
                            )
                        })}
                    </div>)
                })}
            </div>
        )
    }

}

const mapStateToProps = (state, ownProps) => {
    const {tributeUri} = ownProps.params;
    return {
        albumsState: state,
        tribute: tributeByUri(state, tributeUri),
        albums: getAlbumsByTributeId(state, tributeUri),
        mediaTributes: tributeMediaByUri(state, tributeUri),
        tributes: tributes(state)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    const {tributeUri} = ownProps.params;
    return {
        getTributeMedia: (pageNumber) => dispatch(actions.media.getTributeMediaById(tributeUri, pageNumber)),
        getTribute: ()=>{
            return dispatch(actions.tributes.getTribute(tributeUri));
        },
        getTributes: () => dispatch(actions.tributes.getTributes()),
        getAlbums: () => dispatch(actions.albums.getAlbumsByTributeId(tributeUri))
    }
};



export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AlbumSection));