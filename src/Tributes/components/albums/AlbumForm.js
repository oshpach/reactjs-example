import React from 'react'
import {withRouter} from 'react-router'
import {Button, ButtonToolbar, Grid, Row, Col} from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import {css, StyleSheet} from 'aphrodite/no-important'
import Common from '../../../Common'
import {PhotoCamera} from '../../../Common/images'
const {
    components: {BlockInput, EditorRichText, ImageCropUploader},
    helpers: {defaultStyles}
} = Common;

const elementStyles = {
    header: {
        margin: '0 auto 15px',
        ...defaultStyles.textBlue
    },

    description: {
        ...defaultStyles.textBlue,
        fontSize: 22
    }
};

const styles = StyleSheet.create({
    buttonsWrapper: {
        minHeight: 105,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    formWrapper: {
        transition: 'all ease 0.3s'
    },
    dropzoneImage: {
        width: '150px !important',
        height: '150px !important',
        border: '1px solid #a1a6b0 !important',
        borderRadius: '5px !important',

    },
    dropzoneBox: {
        width: '150px !important',
        height: '150px !important',
        borderRadius: '5px !important',
    },
    dropzone: {
        width: '150px !important',
        height: '150px !important',
        borderRadius: '5px !important',
        border: '1px solid #a1a6b0 !important'
    },
    imageLoaderLine: {
        position: 'absolute',
        bottom: 0,
        left: 8,
        right: 8,
        height: 50,
        backgroundColor: '#3498DB',
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5,
        color: 'white',
        fontSize: 15,
        display: 'flex',
        justifyContent: 'center'
    },
    imageLoaderWrap: {
        position: 'relative'
    }
});

class CreateAlbumForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            previewAvatar: null,
            album: this.props.album,
            avatarUri: null,
            previewAvatarBlob: null,
            columnNumber: 12
        };
        this.handleDrop = this.handleDrop.bind(this);
        this.addAvatar = this.addAvatar.bind(this);
    }


    handleDrop = (acceptedFiles) => {
        if (this.state.album.albumId) {
            this.props.saveAvatar(acceptedFiles)
                .then((data) => {
                    this.setState({
                        tributeId: data.tributeId,
                        avatarUri: data.avatarUri,
                        previewAvatarBlob: null,
                        avatarCropUri: null
                    });
                })
                .catch(() => {
                    console.log('**** ERROR ****')
                });
        } else {
            console.log('hellllllllloooo');
            this.setState({
                previewAvatar: acceptedFiles,
                previewAvatarBlob: acceptedFiles.preview
            });
        }
    }

    handleDeleteImage = () => {
        if (this.state.album.albumId) {
            this.props.deleteAvatar(this.state.tributeId)
                .then((data) => {
                    this.setState({
                        avatarUri: null,
                        avatarCropUri: null
                    });
                })
                .catch((error) => {
                    console.log('**** ERROR ****')
                });
        } else {
            this.setState({
                previewAvatarBlob: null
            })
        }
    }

    addAvatar(){
        this.setState({
            columnNumber: this.state.columnNumber === 8 ? 12 : 8
        });
    }

    render(){
        const {handleSubmit, error, pristine, submitting, album = {}} = this.props;
        const pageNumber = this.state.columnNumber === 8 ? 6 : 4;
        return (
            <div style={{textAlign: 'center', alignSelf: 'center', width: '100%',  marginLeft: 10}}>
                {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                <form onSubmit={handleSubmit}>
                    <Row style={{marginTop: 10}}>
                        <Col lg={this.state.columnNumber} md={this.state.columnNumber} sm={this.state.columnNumber} xs={12}>
                            <Col lg={12} md={12} sm={12} xs={12} style={{marginBottom: 10}}>
                                <div className={css(styles.formWrapper)}>
                                    <Field name="name"
                                           component={BlockInput}
                                           type="text"
                                           placeholder="Album Name"/>

                                    <Field name='description' style={{height: this.state.columnNumber === 8 ? '85px' : ''}} component={EditorRichText} placeholder='Description...'/>
                                </div>
                            </Col>
                            <Col lg={4} md={4} sm={4} xs={12} style={{display: this.state.columnNumber === 8 ? 'none' : ''}}>
                                <Button
                                    bsStyle='default'
                                    style={{width: '100%'}}
                                    type="button"
                                    className={`btn-blue center-block`}
                                    onClick={this.addAvatar}
                                >
                                    Add avatar
                                </Button>
                            </Col>
                            <Col lg={pageNumber} md={pageNumber} sm={pageNumber} xs={12}>
                                <Button
                                    bsStyle='default'
                                    style={{width: '100%'}}
                                    className={`center-block ${this.props.buttonAddPhotos}`}
                                    onClick={this.props.cancelButtonOnClick}
                                    disabled={(pristine || submitting)}
                                    type="button"
                                >
                                    Add photos
                                </Button>
                            </Col>
                            <Col lg={pageNumber} md={pageNumber} sm={pageNumber} xs={12}>
                                <Button
                                    type="submit"
                                    bsStyle='default'
                                    style={{width: '100%', color: 'white'}}
                                    className={`center-block ${this.props.buttonStyle}`}
                                    onClick={this.props.cancelButtonOnClick}
                                    disabled={(pristine || submitting)}
                                >
                                    {this.props.primaryButtonText}
                                </Button>
                            </Col>
                        </Col>
                        <Col lg={4} md={4} sm={4} xs={12} style={{display: this.state.columnNumber === 8 ? 'block' : 'none'}}>
                            <div className={css(styles.imageLoaderWrap)}>
                                <ImageCropUploader imageUri={album.avatarUri || this.state.avatarUri}
                                                   cropUri={album.avatarCropUri || this.state.avatarCropUri}
                                                   previewAvatarBlob={this.state.previewAvatarBlob}
                                                   height={150}
                                                   borderRadius={5}
                                                   border={1}
                                                   width={150}
                                                   onDrop={this.handleDrop}
                                                   onCrop={this.handleCropImage}
                                                   onDelete={this.handleDeleteImage}
                                                   styles={styles}
                                />
                                <div className={css(styles.imageLoaderLine)}>
                                    <img src={PhotoCamera} alt="" width={40}/>
                                    <span style={{alignSelf: 'center'}}>Add an avatar</span>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </form>
            </div>
        );
    }
};


const validate = values => {
    const errors = {};

    if (!values.ablumName || values.albumName.trim() === '') {
        errors.firstName = 'Album name is required';
    }

    return errors;
};

export default withRouter(reduxForm({
    form: 'CreateAlbumForm',  // a unique identifier for this form
    validate
})(CreateAlbumForm));
