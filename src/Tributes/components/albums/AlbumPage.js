import React from 'react'
import {connect} from 'react-redux'
import {Map, List} from 'immutable'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'
import {getAlbumById, getAlbumMediaByAlbumId} from '../../selectors'
import AlbumInformationTile from './AlbumInformationTile'
import {defaults, imageContainers} from './settings'


const styles = StyleSheet.create({

    gutter: {
        display: 'inline-block',
        width: defaults.gutterSize,
        height: 0,

        '@media (max-width: 1078px)': {
            display: 'block',
            width: 0,
            height: defaults.gutterSize,
        }
    },

    row: {
        marginBottom: defaults.gutterSize
    }
});


export class AlbumPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            containers: imageContainers
        };

        // Bind this to functions
        this.handleSaveMedia = this.handleSaveMedia.bind(this);
        this.handleCrop = this.handleCrop.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.groupAlbumMedia = this.groupAlbumMedia.bind(this);
        this.getEmptyContainer = this.getEmptyContainer.bind(this);
        this.getEmptyMediaObjectContainer = this.getEmptyMediaObjectContainer.bind(this);
        this.getContainerForMedia = this.getContainerForMedia.bind(this);
    }

    componentDidMount() {
        if (!this.props.album) {
            this.props.getAlbum();
        }

        if (this.props.albumMedia.length === 0) {
            this.props.getAlbumMedia();
        }
    }


    handleSaveMedia(mediaId, row, col, container, position) {
        this.props.saveAlbumMedia({mediaId, row, col, container, position});
    }

    handleCrop(albumMediaId, crop, scale, size) {
        this.props.cropImage(albumMediaId, crop, scale, size);
    }

    handleDelete(albumMediaId) {
        this.props.removeMedia(albumMediaId);
    }

    getEmptyContainer(container, row, col) {
        return this.state.containers[container](row, col, this.props.tributeId, this.props.albumId);
    }

    getEmptyMediaObjectContainer(container, row, col) {
        return {
            albumId: this.props.albumId,
            tributeId: this.props.tributeId,
            container: container,
            row: row,
            col: col,
            albumMediaId: null,
            cropUri: null,
            mediaId: null,
            position: null,
            uri: null
        }
    }

    groupAlbumMedia(albumMedia) {
        // Create default layout
        const layoutMedia = new List()
            .push(this.getEmptyMediaObjectContainer(0, 0, 0))
            .push(this.getEmptyMediaObjectContainer(3, 0, 1))
            .push(this.getEmptyMediaObjectContainer(2, 1, 0))
            .push(this.getEmptyMediaObjectContainer(1, 1, 1))
            .push(this.getEmptyMediaObjectContainer(1, 2, 0))
            .push(this.getEmptyMediaObjectContainer(2, 2, 1));

        // Merge default layout with album media
        let am = layoutMedia.reduce((reducer, lm) => {
            if (!reducer.find(r => r.row === lm.row && r.col === lm.col)) {
                return reducer.push(lm);
            }

            return reducer

        }, List(albumMedia));

        // get the next row to be added
        const maxAlbumMediaObject = am.max((a, b) => {
            if (a.row < b.row) {
                return -1;
            }
            if (a.row > b.row) {
                return 1;
            }

            return 0;
        });

        let nextRow = (maxAlbumMediaObject && maxAlbumMediaObject.row + 1) || 0;  // get the number

        // push blank rows at bottom
        return am.push(this.getEmptyMediaObjectContainer(2, nextRow, 0))
            .push(this.getEmptyMediaObjectContainer(1, nextRow, 1))

            // group the output so it can be displayed.
            .groupBy(r => r.row)  // group by row
            .map(r => r.groupBy(c => c.col))  // group by col
            .map(r => r.map(c => c.groupBy(n => n.container)))  // group by container
            .toJS();
    }

    getContainerForMedia(model) {
        const mapModel = Map(model);
        const container = mapModel.keys().next().value;
        const firstElement = List(model[container]).first();
        const row = firstElement.row;
        const col = firstElement.col;
        return this.state.containers[container]({
                row,
                col,
                tributeId: this.props.tributeId,
                albumId: this.props.albumId,
                onSaveMedia: this.handleSaveMedia,
                onCrop: this.handleCrop,
                onDelete: this.handleDelete,
                editImageBox: true
            })
    }

    render() {
        console.log(this.props.albumMedia);
        return (
            <div style={{textAlign: 'center'}}>
                <AlbumInformationTile album={this.props.album}/>

                {Map(this.groupAlbumMedia(this.props.albumMedia)).valueSeq().map((rowValue, row) => (
                    <div className={css(styles.row)} key={row}>
                        <span>
                            {rowValue[0] ?
                                this.getContainerForMedia(rowValue[0])
                                :
                                row === 0 ? this.getEmptyContainer(0, row, 0) : this.getEmptyContainer(2, row, 0)
                            }

                            <div className={css(styles.gutter)}/>

                            {rowValue[1] ?
                                this.getContainerForMedia(rowValue[1])
                                :
                                row === 0 ? this.getEmptyContainer(3, row, 1) : this.getEmptyContainer(1, row, 1)
                            }
                         </span>
                    </div>
                ))}

            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    const {albumId, tributeId} = props.location.state;
    console.log(props);
    return {
        albumId,
        tributeId,
        album: getAlbumById(state, albumId),
        albumMedia: getAlbumMediaByAlbumId(state, albumId)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const {albumId, tributeId} = props.location.state;

    return {
        getAlbum: () => {
            return dispatch(actions.albums.getAlbum(albumId));
        },
        getAlbumMedia: () => {
            return dispatch(actions.albums.getAlbumMedia(albumId));
        },
        saveAlbumMedia: (model) => {
            return dispatch(actions.albums.saveAlbumMedia({...model, tributeId, albumId}));
        },
        cropImage: (albumMediaId, crop, scale, size) => {
            dispatch(actions.albums.cropImage(albumMediaId, crop, scale, size))
        },
        removeMedia: (albumMediaId) => {
            dispatch(actions.albums.removeMedia(albumMediaId))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AlbumPage);

