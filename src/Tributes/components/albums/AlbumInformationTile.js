import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {StyleSheet, css} from 'aphrodite/no-important'
import renderHTML from 'react-render-html';
import actions from '../../actions'
import ConfirmDeleteModal from './ConfirmDeleteModal'
import Common from '../../../Common'
import * as selectors from '../../selectors'
const {
    components: {BlockTile},
    helpers: {defaultStyles}
} = Common;

const styles = StyleSheet.create({
    tile: {
        textAlign: 'center',
        position: 'relative',
        top: '50px',
        marginBottom: '100px !important',

        ':hover .editButtons': {
            display: 'block'
        }
    },

    editAlbumButton: {
        marginRight: '15px',
        cursor: 'pointer'
    },

    deleteAlbumButton: {
        cursor: 'pointer'
    },

    editButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: 10,
    },

    title: {
        ...defaultStyles.textBlue,
        fontSize: 24,
        color: '#3498DB'
    },

    description: {
        ...defaultStyles.text,
        textAlign: 'left',
        display: 'inline-block',
        color: '#3498DB',
        fontSize: '30px',
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    count: {
        color: '#3498DB'
    }
});

export class AlbumInformationTile extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showConfirmModal: false
        };

        // Bind this to functions
        this.handleEditAlbumClick = this.handleEditAlbumClick.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
        this.handleDeleteAlbumClick = this.handleDeleteAlbumClick.bind(this);
        this.allowedEdit = this.allowedEdit.bind(this);
        this.allowedDelete = this.allowedDelete.bind(this);
    }

    handleEditAlbumClick() {
        this.props.router.push({
            pathname: '/editalbum',
            state: {
                albumId: this.props.album.albumId,
                tributeId: this.props.album.tributeId,
                referrer: this.props.location
            }
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.deleteTribute()
            .then(() => {
                this.props.router.push({
                    pathname: '/mytributes'
                })
            });
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    handleDeleteAlbumClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    allowedEdit() {
        return true;
    }

    allowedDelete() {
        return true;
    }

    render() {
        const {name, description} = this.props.album || {};
        const { firstName = [], lastName = '' } = this.props.tribute || {};
        return (

            <BlockTile className={css(styles.tile)}>
                <div className={css(styles.title)}>{`${firstName} ${lastName}`}</div>
                <div className={css(styles.description)}>{renderHTML(description || '')}</div>
                <div className={css(styles.count)}><span style={{fontWeight: 'bold'}}>{this.props.count}</span> {this.props.count === 1 ? 'Moment' : 'Moments'} in this album</div>

                {/*{(this.allowedEdit() || this.allowedDelete()) &&*/}
                {/*<div className={classNames(css(styles.editButtons), 'editButtons')}>*/}
                    {/*{this.allowedEdit() && <i className={classNames(css(styles.editAlbumButton), "fa fa-pencil-square-o fa-lg")} onClick={this.handleEditAlbumClick}/> }*/}
                    {/*{this.allowedDelete() && <i className={classNames(css(styles.deleteAlbumButton), "fa fa-trash-o fa-lg")} onClick={this.handleDeleteAlbumClick}/> }*/}
                {/*</div>*/}
                {/*}*/}


                <ConfirmDeleteModal title="Delete Album" body={<div><p>Confirm delete album?</p><p style={{fontWeight: 'bold', color: 'red'}}>This action can not be undone.</p></div>}
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled} show={this.state.showConfirmModal}/>
            </BlockTile>
        );
    }
}

const mapStateToProps = (state, props) => {

    const { tributeId = {} } = props.location.state;
    console.log(tributeId);
    return {
        tributeId,
        tribute: selectors.tributeById(state, tributeId),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        deleteTribute: () => {
            return dispatch(actions.albums.deleteAlbum(props.album.albumId));
        },
        getTribute: () => dispatch(actions.tributes.getTribute(props.params.tributeSlug)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AlbumInformationTile));

