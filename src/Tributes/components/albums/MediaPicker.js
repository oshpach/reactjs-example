import React from 'react'
import {connect} from 'react-redux'
import {Modal, Button} from 'react-bootstrap'
import { withRouter } from 'react-router'
import {StyleSheet, css} from 'aphrodite/no-important'
import Dropzone from 'react-dropzone'
import classNames from 'classnames'

import actions from '../../actions'
import {tributeMediaById, tributeById} from '../../selectors'
import {defaultPersona} from '../../../Common/images'
import Common from '../../../Common'
const {components: {ImageLoader}, helpers: {windowDimension}} = Common;

const windowWidth = windowDimension().width;
const windowHeight = windowDimension().height;

const imageSize = windowWidth < 768 ? 100 : 200;

const dzStyle = {
    width: '100%',
    maxWidth: 600,
    padding: 15,
    border: '1px #E0E0E0 solid',
    boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',
    borderRadius: 5,
    margin: '30px auto 0',
    cursor: 'pointer',
    backgroundColor: 'white'
};

const dzActiveStyle = {
    borderStyle: 'dashed'
};

const styles = StyleSheet.create({
    image: {
        width: imageSize,
        height: imageSize
    },

    imageContainer: {
        margin: 5
    },

    imageBox: {
        width: imageSize,
        height: imageSize
    },

    gutter: {
        display: 'inline-block',
        width: 10,
        height: 0,

        '@media (max-width: 600px)': {
            display: 'block',
            width: 0,
            height: 10,
        }
    },

    modal: {
        width: windowWidth * 0.9,
        maxWidth: imageSize * 3 + 100
    },

    scrollContainer: {
        height: windowHeight * 0.8,
        overflow: 'auto',
        minHeight: imageSize + 100,
    },

    modalHeader:{
        padding: '5px 15px',
        borderBottom: '1px solid #e5e5e5'
    },

    modalFooter:{
        padding: '10px 15px 0',
        textAlign: 'right',
        borderTop: '1px solid #e5e5e5'
    },
    faCircle: {
        color: '#E0E0E0',
        textShadow: '-1px 0 #A9A9A9, 0 1px #A9A9A9, 1px 0 #A9A9A9, 0 -1px #A9A9A9'  // adds a border around the circle
    },

    faPicture: {
        color: 'white'
    }
});


export class MediaPicker extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: true
        };

        // Bind this to functions
        this.close = this.close.bind(this);
    }

    componentWillMount(){
        this.props.getMedia();
    }

    handleDrop = (acceptedFiles, e) => {
        acceptedFiles.forEach((file, index) => {
            this.props.saveImage(file)
                .then((data) => {
                    this.props.saveAlbumMedia({
                        tributeId: this.props.tributeId,
                        albumId: this.props.albumId,
                        mediaId: data.mediaId
                    });
                })
                .catch((err) => {
                    console.log(err)
                })
        });
        this.props.getAlbumMedia();
        this.props.getMedia();

    }

    handleImageClick(media) {
        this.props.onMediaPickerSelected(media);
    }

    close() {
        this.setState({
            show: false
        });
    }

    render() {
        const { tribute = [] } = this.props;
        return (
            <Modal
                show={this.state.show}
                onHide={this.props.onHide}
                dialogClassName={css(styles.modal)}
                onExited={this.props.onHide}
            >
                { tribute.role === 'Owner' ? <Dropzone
                    onDrop={this.handleDrop}
                    style={dzStyle}
                    activeStyle={dzActiveStyle}
                >
                    <div style={{textAlign: 'center', fontSize: '12px'}}>
                        <span className="fa-stack fa-3x">
                            <i className={classNames(css(styles.faCircle), "fa fa-circle fa-stack-2x")}/>
                            <i className={classNames(css(styles.faPicture), "fa fa-picture-o fa-stack-1x fa-inverse")}/>
                        </span>

                        <div>Drop Images here</div>
                        <div>or click to add</div>
                    </div>
                </Dropzone> : ''}
                <Modal.Header closeButton className={css(styles.modalHeader)}>
                    <Modal.Title>Select an image</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className={css(styles.scrollContainer)}>

                        {this.props.media && this.props.media.map((m, index) =>
                        <div key={m.mediaId} onClick={() => this.handleImageClick(m)} style={{display: 'inline-block', cursor: 'pointer'}}>
                            <ImageLoader
                                className={css(styles.image)}
                                containerClassName={css(styles.imageContainer)}
                                src={m.cropUri || m.uri}
                                previewSrc={defaultPersona}
                            />
                        </div>
                        )}
                    </div>
                    <Modal.Footer className={css(styles.modalFooter)}>
                        <Button onClick={this.close}>Close</Button>
                    </Modal.Footer>
                </Modal.Body>
            </Modal>
        )
    }
}

const mapStateToProps = (state, props) => {
    const {tributeId = {}} = props.location.state;
    return {
        media: tributeMediaById(state, tributeId),
        tribute: tributeById(state, tributeId)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const {tributeId, albumId} = props.location.state;
    return {
        getTribute: () => dispatch(actions.tributes.getTribute(tributeId)),
        getMedia: () => {
            return dispatch(actions.media.getTributeMediaById(tributeId))
        },
        saveAlbumMedia: (model) => {
            return dispatch(actions.albums.saveAlbumMedia(model))
        },
        saveImage: (file) => {
            return dispatch(actions.media.saveImage(tributeId, file))
        },
        getAlbumMedia: () => {
            dispatch(actions.albums.getAlbumMedia(albumId))
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MediaPicker));

