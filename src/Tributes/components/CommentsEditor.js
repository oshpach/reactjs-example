import React from 'react';
import {Editor, EditorState} from 'draft-js';
import {Field, reduxForm} from 'redux-form'

class CommentsEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {editorState: EditorState.createEmpty()};
        this.onChange = (editorState) => this.setState({editorState});
    }
    render() {
        return (
            <form>
                <Field
                    component={<Editor editorState={this.state.editorState} onChange={this.onChange} />}
                />
            </form>
        );
    }
}

export default CommentsEditor;