import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'
import {tributeConnectRequests} from '../../selectors'
import ConnectTile from './ConnectTile'

const styles = StyleSheet.create({
    avatarContainer: {
        display: 'inline-block'
    },

    textContainer: {
        display: 'inline-block',
        marginLeft: 10
    },

    noMessagesText: {
        marginTop: 20,
        textAlign: 'center',
        color: '#428bca'
    }
});

export class ConnectionRequests extends React.Component {
    componentDidMount() {
        this.props.getConnectRequests();
    }

    render() {
        const {connectRequests} = this.props;

        return (
            <div>
                {
                    connectRequests.length === 0 ?
                    <div className={css(styles.noMessagesText)}>You have no messages</div>
                    :
                    connectRequests.map(connect => <ConnectTile connectUser={connect.user} connectTribute={connect.tribute} key={connect.tributeId}/>)
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        connectRequests: tributeConnectRequests(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getConnectRequests: () => dispatch(actions.connected.getConnectRequests())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionRequests);
