import React from 'react'
import {connect} from 'react-redux'
import {Button} from 'react-bootstrap'
import {Field, reduxForm, destroy} from 'redux-form'
import Common from '../../../Common'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'

const {components: {BlockInput}} = Common;



class MessagesSearch extends React.Component{

    render(){
        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Field name="MessageSearch" component={BlockInput} type="text" placeholder="Search Messages"/>
            </form>
        )
    }

}


MessagesSearch = reduxForm({
    form: 'MessagesSearchForm',  // a unique identifier for this form
})(MessagesSearch);

export default MessagesSearch;