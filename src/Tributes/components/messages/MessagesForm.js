import React from 'react'
import {connect} from 'react-redux'
import {Button} from 'react-bootstrap'
import {Field, reduxForm, destroy} from 'redux-form'
import Common from '../../../Common'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'

const {components: {BlockInput, EditorRichText}} = Common;


class MessagesForm extends React.Component{

    render(){
        const {handleSubmit} = this.props;
        return (
            <form onSubmit={handleSubmit} style={{width: '70%', alignSelf: 'center'}}>
                <Field name="MessageSearch" component={EditorRichText} type="text" placeholder="Type message here..."/>
            </form>
        )
    }

}


MessagesForm = reduxForm({
    form: 'MessagesForm',  // a unique identifier for this form
})(MessagesForm);

export default MessagesForm;