import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Button, ButtonGroup} from 'react-bootstrap'
import actions from '../../actions'
import Common from '../../../Common'
const {components: {DisplayAvatar, BlockTile}} = Common;

const ConnectTile = props => {
    const styles = StyleSheet.create({
        container: {
            overflow: 'hidden'
        },

        buttons: {
            float: 'right',
            width: 85,
            textAlign: 'right',
            marginTop: 10
        },

        avatarContainer: {
            float: 'left',
            width: 85
        },

        textContainer: {
            margin: '10px 85px 0',
            padding: '0 20px'
        }
    });

    const {connectUser, connectTribute} = props;
    const userName = connectUser && `${connectUser.firstName} ${connectUser.lastName}`.trim();
    const tributeName = connectTribute && `${connectTribute.firstName} ${connectTribute.lastName}`.trim();

    return (
        <BlockTile>
            <div className={css(styles.container)}>
                <div className={css(styles.buttons)}>
                    <ButtonGroup vertical>
                        <Button onClick={props.connectTribute} style={{padding: '13px 0'}} bsStyle='primary'>Connect</Button>
                        <Button bsStyle='default'>dismiss</Button>
                    </ButtonGroup>
                </div>

                <div className={css(styles.avatarContainer)}>
                    <DisplayAvatar user={connectUser} size={75}/>
                </div>

                <div className={css(styles.textContainer)}>{userName} <span style={{fontSize: '0.9em'}}>would like to connect to</span> {tributeName} <span style={{fontSize: '0.9em'}}>tribute</span></div>
            </div>
        </BlockTile>
    );
};

const mapDispatchToProps = (dispatch, {connectUser, connectTribute}) => {
    return {
        connectTribute: () => dispatch(actions.connected.connectAccept(connectUser.userId, connectTribute.tributeId))
    }
};

export default connect(null, mapDispatchToProps)(ConnectTile)
