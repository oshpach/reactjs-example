import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Row, Col} from 'react-bootstrap'
import * as moment from 'moment'
import renderHTML from 'react-render-html'
import * as selectors from '../../selectors'
import actions from '../../actions'
import Common from '../../../Common'
import {defaultAvatar, EnterMess, addFile, photoMess} from '../../../Common/images'
import MessagesSearch from "./MessagesSearch";
import MessagesForm from "./MessagesForm"
import './Messages.css'

const {components: {ImageLoader}} = Common;


const styles = StyleSheet.create({
    formWrapper: {
        backgroundColor: '#F7F7F7',
        padding: '10px 10px 0 10px',
        minHeight: '70px',
        borderBottom: '1px solid #CECECE',
        "@media screen and (max-width: 768px)": {
            border: '1px solid #CECECE'
        }
    },
    nameWrapper: {
        flexGrow: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        fontSize: '14px'
    },
    image: {
        width: '50px',
        height: '50px',
        borderRadius: '50%',
        border: '1px solid #3498DB',
    },
    listOfUsers: {
        listStyle: 'none',
        display: 'flex',
        justifyContent: 'space-around',
        paddingTop: '10px',
        paddingBottom: '10px',
        cursor: 'pointer',
        transition: 'all ease 0.3s',
        paddingLeft: 10
    },
    userName: {
        margin: '0',
        padding: '0',
        alignSelf: 'flex-start',
        fontWeight: 'bold'
    },
    userComment: {
        alignSelf: 'flex-start',
        margin: '0',
        padding: '0'
    },
    listWrapper: {
        backgroundColor: '#FAFAFA',
        cursor: 'pointer',
        transition: 'all ease 0.3s',
        position: 'relative',
        height: '75px',
        "@media screen and (max-width: 768px)": {
            borderLeft: '1px solid #CECECE'
        }
    },
    divider: {
        display: 'block',
        width: '100%',
        height: '1px',
        backgroundColor: 'lightgray'
    },
    listCol: {
        backgroundColor: 'white',
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
        // boxShadow: '0 0 10px rgba(0, 0, 0, .3)',
        padding: 0
    },
    paddingRight: {
        width: '80%',
        borderRadius: 5,
        border: '1px solid #3498DB',
        '@media screen and (max-width: 768px)': {
            paddingRight: 0,
            width: '100%'
        }
    },
    userMessageLeft: {
        padding: '20px',
        // borderBottomLeftRadius: '10px',
        // borderBottomRightRadius: '10px',
        // borderTopRightRadius: '10px',
        backgroundColor: '#FAFAFA',
        maxWidth: '600px',
        marginLeft: '10px'
    },
    userMessageRight: {
        padding: '20px',
        borderBottomLeftRadius: '10px',
        borderBottomRightRadius: '10px',
        borderTopLeftRadius: '10px',
        background: 'white',
        maxWidth: '600px',
        marginRight: '10px'
    },
    marginTop: {
        '@media screen and (max-width: 768px)': {
            marginTop: '10px'
        }
    },
    messagesTitle: {
        color: '#3498DB',
        fontSize: '34px',
        textAlign: 'center'
    },
    activeUser: {
        display: 'block',
        position: 'absolute',
        width: '15px',
        height: '15px',
        backgroundColor: '#FDFDFD',
        right: '-7.5px',
        top: '30px',
        transform: 'rotate(-45deg)',
        borderLeft: '1px solid #CECECE',
        borderTop: '1px solid #CECECE',
        "@media screen and (max-width: 768px)": {
            display: 'none'
        }
    },
    rightSection: {
        paddingLeft: '15px',
        position: 'relative',
        zIndex: 800,
        backgroundColor: '#FDFDFD',
        minHeight: '400px',
        paddingRight: 0,
        borderTopRightRadius: 5,
        borderBottomRightRadius: 5,
        borderLeft: '1px solid #CECECE'
    },
    messageWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 0,
        paddingLeft: 20,
        fontSize: '14px',
        flex: 1,
        "@media screen and (max-width:450px)": {
            fontSize: '11px !important'
        }
    },
    messageForm: {
        position: 'absolute',
        bottom: 10,
        left: 0,
        right: 0,
        height: 80,
        display: 'flex',
        paddingLeft: 10,
        paddingRight: 10,
        boxShadow: '0 -5px 5px -5px rgba(0, 0, 0, 0.5)',
        background: 'white'
    },
    titleWrapper: {
        width: '80%',
        margin: '20px auto',
        "@media screen and (max-width:450px)": {
            width: '100%'
        }
    },
    isOnline: {
        bottom: 0,
        right: 0,
        borderRadius: '50%',
        backgroundColor: '#21c710',
        position: 'absolute',
        border: '1px solid white'
    },
    userSection: {
        backgroundColor: '#F7F7F7',
        minHeight: 80,
        borderBottom: '1px solid #CECECE',
        borderTopLeftRadius: 5,
        display: 'flex',
        justifyContent: 'center',
        paddingLeft: 10
    },
    imageUser: {
        width: 60,
        height: 60,
        border: '1px solid #3498DB',
        borderRadius: '50%'
    },
    containerStyle: {
        alignSelf: 'center',
        marginRight: 10,
        position: 'relative'
    },
    userSelected: {

    },
    line: {
        width: 13,
        height: 1,
        marginBottom: 10,
        backgroundColor: '#3498DB',
        display: 'inline-block'
    },
    enterMessage: {
        width: 40,
        borderRadius: '50%',
        height: 40,
        backgroundColor: '#3498DB',
        display: 'flex',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: 10
    },
    addFiles: {
        width: 40,
        borderRadius: '50%',
        height: 40,
        backgroundColor: 'white',
        display: 'flex',
        justifyContent: 'center',
        alignSelf: 'center',
        marginLeft: 10
    }
});


class Messages extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            index: 0
        };
        this.selectUser = this.selectUser.bind(this);
    }


    componentWillMount(){
        if(this.props.tributes.length === 0){
            this.props.getTributes();
        }

        this.props.getStatus('accepted');
                // this.getStatus(this.props.tributes[0].tributeId, 'accepted')
    }

    selectUser(index){
        this.setState({
            index
        });
    }

    render(){
        const {tributes = [], requestedUsers = [], acceptedUsers = []} = this.props;
        console.log(acceptedUsers);
        return(
            <div style={{marginTop: '80px'}}>
                <Row className={css(styles.titleWrapper)}>
                    <Col lg={12} md={12} sm={12} xs={12}>
                        <p className={css(styles.messagesTitle)}>
                            {/*<span style={{paddingRight: '5px'}}><i className="fa fa-envelope" aria-hidden="true"></i></span>*/}
                            <span className={css(styles.line)} style={{marginRight: 16}}></span>
                            Messages
                            <span className={css(styles.line)} style={{marginLeft: 16}}></span>
                        </p>
                    </Col>
                </Row>
                <Row style={{ margin: '20px auto'}} className={css(styles.paddingRight)}>
                    <Col lg={4} md={4} sm={5} xs={12} className={css(styles.listCol)}>
                        <div className={css(styles.userSection)}>
                            <ImageLoader
                                className={css(styles.imageUser)}
                                src={null}
                                previewSrc={defaultAvatar}
                                containerClassName={css(styles.containerStyle)}
                            >
                                <span className={css(styles.isOnline)} style={{ width: 20, height: 20}}></span>
                            </ImageLoader>
                            <span style={{alignSelf: 'center'}}>Vladyslav Posudevskyi</span>
                        </div>
                        <div className={css(styles.formWrapper)}>
                        <MessagesSearch/>
                        </div>
                        <ul style={{padding: '0', margin: 0, maxHeight: 250, overflowY: 'auto', overflowX: 'hidden'}}>
                            {acceptedUsers.length !== 0 ? acceptedUsers.map((tribute, index) =>(
                                <div key={index}>
                                    <div className={`${css(styles.listWrapper)}`}>
                                        <li className={css(styles.listOfUsers)} onClick={() => {this.selectUser(index)}}>
                                            <ImageLoader
                                                className={css(styles.image)}
                                                src={tribute.avatarUri}
                                                previewSrc={defaultAvatar}
                                                containerClassName={css(styles.containerStyle)}
                                            >
                                                <span className={css(styles.isOnline)} style={{ width: 15, height: 15}}></span>
                                            </ImageLoader>
                                            <div className={css(styles.nameWrapper)} style={{margin: 0}} id="user_messages">
                                                <p className={css(styles.userName)}>{tribute.firstName + ' '  + tribute.lastName}</p>
                                                {tribute.message ? renderHTML(tribute.message) : "There are no messages..."}
                                            </div>
                                        </li>
                                        <span className={`${this.state.index === index ? css(styles.activeUser): ''}`}></span>
                                    </div>
                                    <span className={css(styles.divider)} style={{display: index === 1 ? 'none' : ''}}></span>
                                </div>
                            )) : null}
                        </ul>
                    </Col>
                    <Col lg={8} md={8} sm={7} xs={12} className={`${css(styles.marginTop)} ${css(styles.rightSection)}`}>
                        <div style={{ paddingTop: '20px', paddingRight: 10, maxHeight: 310, overflowY: 'auto'}}>
                            {acceptedUsers.length && acceptedUsers[this.state.index].message?
                                <div style={{display: 'flex', justifyContent:'flex-start', marginBottom: 10}}>
                                    <ImageLoader
                                        containerStyle={{order: 0}}
                                        className={css(styles.image)}
                                        src={acceptedUsers[this.state.index].avatarUri}
                                        previewSrc={defaultAvatar}
                                        containerClassName={css(styles.containerStyle)}
                                    />
                                    <div className={css(styles.messageWrapper)} id="user_messages">
                                        <div>
                                            <p className={css(styles.userName)}>{acceptedUsers[this.state.index].firstName + ' '  + acceptedUsers[this.state.index].lastName}</p>
                                            {acceptedUsers[this.state.index].message ? renderHTML(acceptedUsers[this.state.index].message) : "There are no messages..."}
                                        </div>
                                        <p>{moment(acceptedUsers[this.state.index].requestedOn).calendar()}</p>
                                    </div>

                                    {/*<p style={{order: 1}} className={css(styles.userMessageLeft)}>{renderHTML(acceptedUsers[this.state.index].message)}</p>*/}
                                </div> : 'There are no messages yet...'}
                        </div>
                        <div id="message_form" className={css(styles.messageForm)}>
                            <MessagesForm/>
                            <span className={css(styles.addFiles)} style={{marginLeft: '5%'}}>
                                 <img style={{alignSelf: 'center'}} width="30px" src={photoMess} alt="Enter"/>
                            </span>
                            <span className={css(styles.addFiles)}>
                                 <img style={{alignSelf: 'center'}} width="20px" src={addFile} alt="Enter"/>
                            </span>
                            <span className={css(styles.enterMessage)}>
                                 <img style={{alignSelf: 'center'}} width="20px" src={EnterMess} alt="Enter"/>
                            </span>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = (state, props) => ({
    tributes: selectors.tributes(state),
    myTributes: selectors.myTributes(state),
    requestedUsers: selectors.tributeConnectRequests(state),
    acceptedUsers: selectors.acceptedUsersToMessage(state),
});

const mapDispatchToProps = (dispatch, props) => {

    return {
        getTributes: () => dispatch(actions.tributes.getTributes()),
        getStatus: (status) => dispatch(actions.connected.getConnectRequests(status)),
        getMyTributes: () => dispatch(actions.tributes.getMyTributes())
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Messages);