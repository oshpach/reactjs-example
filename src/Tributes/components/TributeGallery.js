import React from 'react';
import {connect} from 'react-redux'
import {destroy, initialize} from 'redux-form'
import {withRouter} from 'react-router'
import renderHTML from 'react-render-html'
import {Grid, Row, Col, DropdownButton, MenuItem, Button} from 'react-bootstrap'
import Waypoint from 'react-waypoint'
import {StyleSheet, css} from 'aphrodite/no-important'
import {userAuthenticated, getMediaUrl, getMediaUrlNew, getUserId} from '../../utils';
import * as selectors from '../selectors'
import {user} from '../../Account/Selectors'
import actions from '../actions'
import defaultAvatar from '../../Common/createTribute/images/default-avatar.png'
import {getAccount} from '../../Account/actions/account'
import TributeGalleryForm from './TributeGalleryForm'


import Common from '../../Common'
import './TributeGallery.css'
// import VideoExample from '../VideoExample'

const {components: {ImageCrop}, helpers: {windowDimension}} = Common;

const styles = StyleSheet.create({
    formWrapper: {
        width: '100%',
        "@media screen and (max-width: 949px)": {
            width: '90%'
        }

    },
    hideScroll: {
        overflow: 'hidden !important'
    },
    tributePhoto: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        cursor: 'pointer',
        zIndex: '899'
    },
    modal: {
        position: 'fixed',
        top: '0',
        left: '0',
        right: '0',
        bottom: '0',
        background: 'rgba(0, 0, 0, 0.7)',
        zIndex: '999',
        display: 'flex',
        justifyContent: 'center',
        "@media screen and (max-width: 949px)": {
            overflowY: 'auto'
        }
    },
    closeButton: {
        position: 'absolute',
        top: '10px',
        right: '20px',
        padding: '10px',
        fontSize: '30px',
        color: 'white',
        cursor: 'pointer',
        "@media screen and (max-width: 400px)": {
            top: '0',
            right: '0'
        }
    },
    modalHidden: {
        display: 'none'
    },
    modalPhoto: {
        backgroundColor: 'black',
        display: 'flex',
        overflow: 'hidden',
        flexGrow: 1,
        maxWidth: '700px',
        '@media screen and (max-width: 949px)': {
            width: '100%',
            backgroundColor: 'transparent'
        }
    },
    modalWrapper: {
        width: '90%',
        margin: '0 auto',
        minHeight: '450px',
        maxWidth: '1000px',
        height: '100%',
        maxHeight: '600px',
        display: 'flex',
        alignSelf: 'center',
        justifyContent: 'center',
        position: 'relative',
        "@media screen and (max-width: 949px)": {
            width: '70%',
            display: 'block'
        },
        "@media screen and (max-width: 768px)": {
            position: 'absolute',
            top: '50px',
            left: '50%',
            transform: 'translateX(-50%)'
        },
        "@media screen and (max-width: 400px)": {
            width: '80%',
            display: 'block'
        }
    },
    modalComment: {
        flexBasis: '300px',
        maxWidth: '300px',
        display: 'flex',
        flexDirection: 'column',
        "@media screen and (max-width: 949px)": {
            maxWidth: '100%'
        }
    },
    leftArrow: {
        position: 'absolute',
        left: '20px',
        top: '50%',
        transform: 'translateY(-50%)',
        fontSize: '30px',
        color: 'white',
        cursor: 'pointer',
        zIndex: 999,
        "@media screen and (max-width: 400px)": {
            left: '10px'
        }
    },
    rightArrow: {
        cursor: 'pointer',
        position: 'absolute',
        right: '20px',
        top: '50%',
        transform: 'translateY(-50%)',
        fontSize: '30px',
        color: 'white',
        zIndex: 999,
        "@media screen and (max-width: 400px)": {
            right: '10px'
        }
    },
    modalCommentTop: {
        backgroundColor: 'white',
        paddingBottom: '10px',
        position: 'relative'
    },
    modalCommentBottom: {
        backgroundColor: '#3498DB',
        color: 'white',
        position: 'relative',
        flexGrow: '1',
        "@media screen and (max-width: 949px)": {
            marginBottom: '30px'
        }
    },
    modalCommentAvatar: {
        display: 'inline-block',
        borderRadius: '50%',
        width: '50px',
        height: '50px',
        backgroundSize: 'cover',
        margin: '5px 0 0 15px',
        backgroundPosition: 'center center',
        float: 'left'
    },
    modalCommentName: {
        display: 'inline-block',
        margin: '15px 0 0 13px'
    },
    likes: {
        color: '#DB3534',
        margin: '0 10px 0 15px',
        fontSize: '14px'
    },
    likesCount: {
        fontSize: '12px'
    },
    likeButton: {
        display: 'inline-block',
        backgroundColor: '#E03333',
        color: 'white',
        padding: '2px 7px',
        fontSize: '12px',
        marginLeft: '10px',
        borderRadius: '3px',
        border: 'none',
    },
    comments: {
        display: 'inline-block',
        color: '#3498DB',
        margin: '0 10px 0 15px'

    },
    commentsCount: {
        fontSize: '12px',
        color: 'black',
        marginLeft: '10px'
    },
    mediaCount: {
        position: 'absolute',
        color: 'white',
        bottom: '-20px',
        left: '20%',
        "@media screen and (max-width: 949px)": {
            display: 'none'
        }
    },
    userAvatar: {
        display: 'inline-block',
        borderRadius: '50%',
        width: '30px',
        height: '30px',
        backgroundSize: 'cover',
        margin: '5px 0 0 5px',
        backgroundPosition: 'center center',
        float: 'left',
        alignSelf: 'center'
    },
    userAvatarInput: {
        display: 'inline-block',
        width: '100px',
        height: '100px',
        backgroundSize: 'cover',
        margin: '5px 0 0 15px',
        backgroundPosition: 'center center',
        float: 'left',
        alignSelf: 'center'
    },

    userName: {
        fontSize: '12px',
        display: 'inline-block',
        margin: '10px 0 0 13px',
        paddingBottom: '4px',
        textDecoration: 'underline'
    },
    userComment: {
        fontSize: '12px',
        marginLeft: '40px',
        display: 'block',
        width: '75%',
        paddingTop: '5px',
        paddingRight: '5px',
        hyphens: 'auto',
        wordBreak: 'break-all'

    },
    userWrapper: {
        width: '100%',
        position: 'relative',
        display: 'inline-block',
        marginTop: '10px'
    },
    commentInput: {
        color: 'black',
        marginLeft: '5px',
        padding: '5px',
        border: 'none',
        borderRadius: '5px',
        width: '80%',
        alignSelf: 'center',
        fontSize: '12px'
    },
    userWrapperInput: {
        margin: '20px 0 20px 0',
        position: 'absolute',
        justifyContent: 'space-between',
        bottom: '0',
        left: '0',
        right: '0',
        display: 'flex',
        "@media screen and (max-width: 949px)": {
            position: 'static',
            margin: '20px 15px 0 0',
            paddingBottom: '20px'
        }
    },
    userMargin: {
        margin: '0 0 0 5px',
        "@media screen and (max-width: 949px)": {
            marginLeft: '15px'
        }
    },
    divider: {
        display: 'block',
        width: '80%',
        margin: '0 auto',
        height: '1px',
        background: 'rgba(0, 0, 0, 0.1)'
    },
    photo: {
        alignSelf: 'center',
        "@media screen and (max-width: 949px)": {
            alignSelf: 'flex-end'
        }

    },
    commentsScroll: {
        overflowY: 'auto',
        overflowX: 'hidden',
        height: '350px',
        "@media screen and (max-width: 949px)": {
            overflowY: 'auto',
            maxHeight: '250px',
            height: '100%'
        }
    },
    animation: {
        position: 'absolute',
        top: '0',
        bottom: '0',
        color: 'transparent',
        left: '0',
        right: '0',
        background: 'rgba(52, 152, 219, 0)',
        transition: 'all ease 0.4s',
        display: 'flex',
        justifyContent: 'center',
        zIndex: 899,
        ":hover": {
            background: 'rgba(52, 152, 219, .6)',
            color: 'white'
        },
        ':hover .cropPencil': {
            display: 'block'
        }

    },
    commentsIcon: {
        paddingRight: '10px'
    },
    icons: {
        alignSelf: 'center',
        "@media screen and (max-width: 768px)": {
            display: 'none'
        }
    },
    deletePhoto: {
        position: 'absolute',
        right: '20px',
        top: '10px',
        cursor: 'pointer',
        color: '#3498DB'
    },
    editButton: {
        position: 'absolute',
        right: '5px',
        top: '5px'
    },
    cropPencil: {
        display: 'none'
    },
    loadMoreWrap: {
        textAlign: 'center',
        marginTop: '20px'
    },
    EditComment: {
        padding: '0',
        background: 'transparent !important',
        border: 'none',
        boxShadow: 'none !important',
        color: 'white',
        fontSize: '20px',
        ':hover': {
            background: 'transparent',
            border: 'none'
        },
        ':active': {
            background: 'transparent',
            border: 'none',

        },
        ':focus': {
            background: 'transparent',
            border: 'none'
        }
    },
    noComments: {
        margin: '15px 0',
        fontStyle: 'italic',
        textAlign: 'center'
    }
});


class GalleryPopUp extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            index: '',
            shouldDelete: false,
            showCropEditor: false,
            indexToEdit: '',
            cropUri: '',
            pageNumber: 0,
            moreData: false,
            showButtonLoad: true,
            comments: [],
            shouldEditComment: false,
            commentToEdit: '',
            commentId: '',
            userId: '',
            indexToHide: null
        };
        this.showModal = this.showModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.deletePhoto = this.deletePhoto.bind(this);
        this.editorWidth = this.editorWidth.bind(this);
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleImageCancelCrop = this.handleImageCancelCrop.bind(this);
        this.doShowCropEditor = this.doShowCropEditor.bind(this);
        this.getDim = this.getDim.bind(this);
        this.loadMoreMedia = this.loadMoreMedia.bind(this);
        this.loadMediaByButton = this.loadMediaByButton.bind(this);
        this.getComments = this.getComments.bind(this);
        this.editComment = this.editComment.bind(this);
        this.changeState = this.changeState.bind(this);
    }

    componentWillMount() {
        if (this.props.tributes.length === 0) {
            this.props.getTributes();
        }
        if (this.props.user.firstName === null) {
            this.props.getAccount();
        }
    }

    componentDidMount() {
        getUserId()
            .then((data) => {
                this.setState({
                    userId: data
                });
            });

    }

    editorWidth() {
        return windowDimension().width < 525 ? windowDimension().width - 30 : 480;
    }

    doShowCropEditor(e, index) {
        e.stopPropagation();
        this.setState({
            showCropEditor: true,
            indexToEdit: index
        })
    }

    handleCropImage = (crop, scale) => {
        const mediaId = this.props.media[this.state.indexToEdit].mediaId;
        const albumMediaId = this.props.media[this.state.indexToEdit].albumMediaId;

        if (this.props.access === 'albumPhoto') {
            this.props.cropImageAlbum(albumMediaId, crop, scale, {height: 480, width: 480})
                .then(() => {
                    this.props.getAlbumMedia()
                        .then((data) => {
                            console.log(this.state.indexToEdit);
                            console.log(data.albumMedia)
                        })
                })
                .catch((err) => {
                    throw err
                });
        }
        if (this.props.access === 'tributePhoto') {
            this.props.cropImage(mediaId, crop, scale)
                .then(() => {
                    this.props.getFiles();
                })
                .catch((err) => {
                    throw err
                });
        }

        this.setState({
            showCropEditor: false,
            indexToEdit: ''
        })
    }

    handleImageCancelCrop() {
        this.setState({
            showCropEditor: false,
            indexToEdit: ''
        })
    }

    nextSlide(index, media) {
        let length = media.length - 1;
        if (index === length) {
            this.setState({
                index: 0
            });
            this.getComments(this.props.media[0].mediaId);
        }
        if (index < length) {
            this.setState({
                index: index + 1
            });
            this.getComments(this.props.media[index + 1].mediaId);
        }

    }

    previousSlide(index, media) {
        let length = media.length - 1;
        if (index === 0) {
            this.setState({
                index: length
            });
            this.getComments(this.props.media[length].mediaId);
        }
        if (index > 0) {
            this.setState({
                index: index - 1
            });
            this.getComments(this.props.media[index - 1].mediaId);
        }
    }

    deletePhoto(mediaId, albumMediaId, length) {

        if (this.props.access === 'tributePhoto') {
            this.props.deleteMedia(mediaId);
        } else if (this.props.access === 'albumPhoto') {
            this.props.removeMedia(albumMediaId);
        }
        if (this.state.index > 0) {
            this.setState({
                index: this.state.index - 1
            });
        }
        if (length === 1) {
            this.setState({
                index: ''
            });
        }
        this.props.getFiles();
        this.closeModal();

    }

    closeModal() {
        const body = document.querySelector('body');
        body.classList.remove(css(styles.hideScroll));
        this.setState({
            showModal: false
        });
        this.props.getTributeMediaById(this.state.pageNumber);
    }

    getDim() {
        const width = windowDimension().width;
        if (width >= 992) {
            return 4;
        } else if (width >= 768 && width < 992) {
            return 3;
        } else if (width < 768) {
            return 2;
        }
    }

    showModal(index) {
        const body = document.querySelector('body');
        body.classList.add(css(styles.hideScroll));
        this.getComments(this.props.media[index].mediaId);
        console.log(this.props.media[index]);
        this.setState({
            showModal: true,
            index: index,
            comments: false
        });

    }

    loadMoreMedia() {
        this.setState({
            moreData: false
        });
        this.props.getTributeMediaById(this.state.pageNumber + 1)
            .then((data) => {
                this.setState({
                    moreData: data.media.length !== 0,
                    pageNumber: this.state.pageNumber + 1
                });
            });
    }

    renderWaypoint() {
        if (this.state.moreData) {
            return (
                <Waypoint onEnter={this.loadMoreMedia}/>
            );
        }
    }

    loadMediaByButton() {
        this.props.getTributeMediaById(1)
            .then((data) => {
            console.log(data);
                this.setState({
                    pageNumber: 1,
                    showButtonLoad: false,
                    moreData: data.media.length !== 0
                });
            })
    }

    getComments = (mediaId) => {
        this.props.getComment(mediaId)
            .then((data) => {
                console.log(data);
            })
            .catch((err) => {
                throw err
            });
    }


    editComment(comment, index) {
        this.props.clearInitial('CommentsForm');
        this.props.initializeForm({edit: comment.comment});
        this.setState({
            shouldEditComment: true,
            commentId: comment.commentId,
            indexToHide: index
        });
    }

    changeState() {
        this.props.clearInitial('CommentsForm');
        this.setState({
            shouldEditComment: false,
            commentId: ''
        });
    }

    deleteComment(comment) {
        console.log(comment);
        this.props.deleteComment(comment.commentId, comment.mediaId)
            .then((data) => {
                console.log(data);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    getCommentsCountByMediaId(media){
        this.props.getCommentsCountByMediaId(media);
    }

    render() {
        const {media = [], tribute = {}, comments = [], user = {}} = this.props;
        const mediaLength = media.length;
        console.log(this.props);
        return (
            <div>
                <div className={this.props.containerStyle}>
                    <div className={this.state.showModal ? css(styles.modal) : css(styles.modalHidden)}>
                        <span className={css(styles.leftArrow)} onClick={ () => {
                            this.previousSlide(this.state.index, media)
                        }}><i className="fa fa-chevron-left"/></span>
                        <span className={css(styles.closeButton)} id="closeGallery" onClick={() => {
                            this.closeModal(this.state.index, media)
                        }}><i className="fa fa-times"/></span>
                        <div className={css(styles.modalWrapper)}>
                            <div className={css(styles.modalPhoto)}>
                                {this.state.index === '' ? '' : <img className={css(styles.photo)}
                                                                     src={`${getMediaUrl(media[this.state.index].uri)}`}
                                                                     width="100%" alt="tribute"/>}
                            </div>
                            <div className={css(styles.modalComment)}>
                                <div className={css(styles.modalCommentTop)}>
                                    {userAuthenticated() && tribute.role === 'owner' ?
                                        <div className={css(styles.editButton)}>
                                            <DropdownButton style={{
                                                background: '#3498DB',
                                                border: '1px solid white',
                                                color: 'white'
                                            }} title={<span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>}
                                                            id="EditTribute">
                                                <MenuItem eventKey="1">Hide a photo</MenuItem>
                                                <MenuItem eventKey="2">Hide comments</MenuItem>
                                                <MenuItem divider/>
                                                <MenuItem onClick={ () => {
                                                    this.deletePhoto(media[this.state.index].mediaId, media[this.state.index].albumMediaId, mediaLength)
                                                } } eventKey="3">Delete</MenuItem>
                                            </DropdownButton>
                                        </div> : ''}

                                    <span className={css(styles.modalCommentAvatar)}
                                          style={{backgroundImage: `url(${getMediaUrl(tribute.avatarUri)})`}}></span>
                                    <p className={css(styles.modalCommentName)}>{`${''} ${tribute.lastName}`}</p>
                                    <p style={{clear: 'both'}}></p>
                                    <span className={css(styles.likes)}><i className="fa fa-heart"/></span>
                                    <span className={css(styles.likesCount)}>{`${124} People are loving it`}</span>
                                    <button className={css(styles.likeButton)}>Love it</button>
                                    <span className={css(styles.comments)}><i className="fa fa-comments-o"/> <span
                                        className={css(styles.commentsCount)}>{comments.length} comments</span></span>
                                </div>
                                <div className={css(styles.modalCommentBottom)}>
                                    <div className={css(styles.commentsScroll)} id="commentWrapper-24d">
                                        {comments.length ? comments.map((mes, index) =>(
                                            mes.comment ? <span className={css(styles.userWrapper)} key={index}
                                                                style={{display: (this.state.indexToHide === index && this.state.shouldEditComment) ? 'none' : 'inline-block'}}>
                                                    <span className={css(styles.userAvatar)}
                                                          style={{backgroundImage: `url(${getMediaUrl(mes.userAvatarUri)})`}}></span>
                                                    <span className={css(styles.userComment)}><span style={{
                                                        textDecoration: 'underline',
                                                        paddingRight: '5px'
                                                    }}>{`${mes.firstName} ${mes.lastName}`}</span><span
                                                        style={{display: 'inline-block',}}>{renderHTML(mes.comment)}</span></span>
                                                {this.state.userId === mes.userId ? <DropdownButton
                                                    title={<span><i className="fa fa-ellipsis-h" aria-hidden="true"></i></span>}
                                                    id="EditComment" className={css(styles.EditComment)}>
                                                    <MenuItem onClick={() => {
                                                        this.editComment(mes, index)
                                                    }}>Edit</MenuItem>
                                                    <MenuItem onClick={() => {
                                                        this.deleteComment(mes)
                                                    }}>Delete</MenuItem>
                                                </DropdownButton> : null}
                                                </span> : null
                                        )) : <div className={css(styles.noComments)}>There are no comments yet</div>}
                                    </div>
                                    <span className={css(styles.divider)}></span>
                                    <div className={css(styles.userWrapperInput)}>
                                        <span className={`${css(styles.userAvatar)} ${css(styles.userMargin)}`}
                                              style={{backgroundImage: `url(${getMediaUrl(user.avatarUri ? user.avatarUri : defaultAvatar)})`}}></span>
                                        <div className={css(styles.formWrapper)}>
                                            <TributeGalleryForm changeState={this.changeState}
                                                                commentId={this.state.commentId}
                                                                shouldEdit={this.state.shouldEditComment}
                                                                mediaId={this.state.index !== '' && media[this.state.index].mediaId}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span className={css(styles.mediaCount)}>{`${this.state.index + 1} / ${mediaLength}`}</span>
                        </div>
                        <span className={css(styles.rightArrow)} onClick={() => {
                            this.nextSlide(this.state.index, media)
                        }}><i className="fa fa-chevron-right"/></span>
                    </div>
                    <Grid style={{marginBottom: '50px'}}>
                        <Row id="hideScrollBody">

                            {media.map((mediaItem, index)=> {
                                console.log(mediaItem);
                                return (
                                    <div key={mediaItem.mediaId} className={css(styles.galleryWrapper)}
                                         style={{marginTop: this.state.showCropEditor && '4px'}}>
                                        <Col md={3} sm={4} xs={6} style={{
                                            marginBottom: this.state.indexToEdit ? '0' : '4px',
                                            textAlign: 'center',
                                            paddingLeft: '2px',
                                            paddingRight: '2px'
                                        }}>
                                            <div style={{
                                                position: 'relative',
                                                width: '100%',
                                                paddingBottom: this.state.indexToEdit === index ? 0 : '100%',
                                                // overflow: 'hidden'
                                            }}>
                                                {this.state.indexToEdit === index ?
                                                    <ImageCrop
                                                        src={getMediaUrl(mediaItem.uri) + '?' + new Date().getSeconds()}
                                                        height={this.editorWidth()}
                                                        width={this.editorWidth()}
                                                        border={0}
                                                        onCrop={this.handleCropImage}
                                                        onCancel={this.handleImageCancelCrop}
                                                        usage="style1" //Set true in order to give width:100% just to a gallery page
                                                    /> : ''}

                                                <div style={{
                                                    backgroundImage: `url(${getMediaUrlNew(mediaItem.cropUri || mediaItem.uri)}`,
                                                    display: this.state.indexToEdit === index ? 'none' : ''
                                                }} className={css(styles.tributePhoto)}>
                                                    <div className={css(styles.animation)}
                                                         onClick={this.state.showCropEditor ? '' : () => {
                                                             this.showModal(index)
                                                         }}>
                                                    <span className="cropPencil" onClick={(e) => {
                                                        this.doShowCropEditor(e, index)
                                                    }}><i className="fa fa-pencil-square-o"></i></span>
                                                        <div className={css(styles.icons)}>
                                                        <span className={css(styles.commentsIcon)}>{mediaItem.commentCount}<i
                                                            className="fa fa-comments-o"/></span>
                                                            <span className={css(styles.likesIcon)}>4 <i
                                                                className="fa fa-heart"/></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </Col>
                                        {/*<Col md={3} sm={4} xs={6} style={{paddingLeft: '2px', paddingRight: '2px', marginBottom: '4px'}}>*/}
                                        {/*<div style={{*/}
                                        {/*position: 'relative',*/}
                                        {/*width: '100%',*/}
                                        {/*overflow: 'hidden'*/}
                                        {/*}}>*/}
                                        {/*<VideoExample/>*/}
                                        {/*</div>*/}
                                        {/*</Col>*/}
                                        <div className={(index + 1) % this.getDim() === 0 ? 'clearfix' : ''}></div>
                                    </div>
                                )
                            })}
                            {this.renderWaypoint()}
                        </Row>
                        {this.state.showButtonLoad && media.length === 8 && <Row>
                            <Col md={12} sm={12} xs={12} className={css(styles.loadMoreWrap)}>
                                <Button onClick={() => {
                                    this.loadMediaByButton()
                                }} bsStyle="primary">{this.state.isLoading ? 'Loading...' : 'Load more'}</Button>
                            </Col>
                        </Row>}
                    </Grid>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    console.log(state);
    return {
        tributes: selectors.tributes(state),
        comments: selectors.sortedComments(state),
        user: user(state)
    }
};
const mapDispatchToProps = (dispatch, props) => {
    const tributeId = (props.location.state && props.location.state.tributeId);
    const albumId = (props.location.state && props.location.state.albumId);
    return {
        deleteMedia: (mediaId) => {
            return dispatch(actions.media.deleteImage(tributeId, mediaId));
        },
        getAlbumMedia: () => {
            return dispatch(actions.albums.getAlbumMedia(albumId));
        },
        removeMedia: (mediaAlbumId) => {
            dispatch(actions.albums.removeMedia(mediaAlbumId))
        },
        saveMediaComment: (mediaId, comment) => {
            dispatch(actions.media.saveMediaComment(mediaId, comment));
        },
        cropImage: (mediaId, crop, scale) => {
            return dispatch(actions.media.cropImage(mediaId, crop, scale));
        },
        cropImageAlbum: (albumMediaId, crop, scale, size) => {
            return dispatch(actions.albums.cropImage(albumMediaId, crop, scale, size));
        },
        getFiles: () => {
            dispatch(actions.media.getTributeMediaById(tributeId))
        },
        getTributeMediaById: (pageNumber) => dispatch(actions.media.getTributeMediaById(props.params.tributeUri, pageNumber)),
        getComment: (mediaId) => dispatch(actions.media.getMediaComments(mediaId)),
        saveComment: (mediaId, comment) => dispatch(actions.media.saveMediaComment(mediaId, comment)),
        editComment: (commentId, comment) => dispatch(actions.media.editMediaComment(commentId, comment)),
        deleteComment: (commentId, mediaId) => dispatch(actions.media.deleteMediaComment(commentId, mediaId)),
        getTributes: () => dispatch(actions.tributes.getTributes()),
        initializeForm: (data) => dispatch(initialize("CommentsForm", data, ["edit"])),
        clearInitial: (formName) => dispatch(destroy(formName)),
        getAccount: () => dispatch(getAccount(true)),
        getCommentsCountByMediaId: (mediaId) => dispatch(actions.media.getCommentsCountByMediaId(mediaId))

    }
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GalleryPopUp));