import React from 'react'
import TributeFormWrap from './TributeFormWrap'

class CreateTribute extends React.Component {

    render() {
        return (
            <div>
                <TributeFormWrap/>
            </div>
        );
    }
}

export default CreateTribute;
