import React from 'react'
import {connect} from 'react-redux'
import {Row, Col, Grid} from 'react-bootstrap'
import {Form, Field, FormSection, reduxForm} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
// import moment from 'moment'
import actions from '../../actions'
// import * as selectors from '../../selectors'
import {defaultAvatar, albumLogo} from '../../../Common/images'
import Common from '../../../Common'
import TabButtonToolbar from './TabButtonToolbar'

import {validTribute} from '../../validators';

import {TRIBUTE_FORM} from '../../formNames';

const {
    components: {BlockInput, BlockDatePicker, ImageCropUploader, BlockTile, EditorRichText},
    formatters: {formatDate}
} = Common;

const AddonAfterDate = (props)=> {
    return (
        <span style={{cursor: 'pointer'}} className="react-datepicker-ignore-onclickoutside" onClick={props.onClick}
              value={props.value} onChange={props.onChange} disabled={props.disabled}><i className="flaticon-time"/>
    </span>
    );
};

const styles = StyleSheet.create({
    headings: {
        fontWeight: 'bold',
        color: '#3498DB'
    },
    text: {
        color: '#3498DB',
        fontWeight: '600',
        fontSize: '15px',
        marginBottom: '10px'
    },
    container: {
        backgroundColor: '#FFFFFF',
        width: '100%',
        maxWidth: 600,
        margin: '15px auto',
        padding: 30,
        border: '#F0F0F0 solid 1px',

        '@media (max-width: 650px)': {
            border: 'none',
            padding: '30px 0'
        }
    },
    rteContainer: {
        color: '#333333',
        borderColor: '#cccccc',
        borderRadius: 6,
        backgroundColor: 'white',
    },
    formWrapper: {
        marginTop: 50,
        padding: 50,
        background: 'white',
        borderRadius: 10,
        border: '1px solid #3498DB',
        "@media screen and (max-width: 768px)": {
            padding: 10,
            marginBottom: 15
        }
    },
    photoWrapper: {
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundImage: `url(${albumLogo})`,
        display: 'flex',
        justifyContent: 'center',
        width: 200,
        height: 200,
        margin: '0 auto',
        "@media screen and (max-width: 768px)": {
            marginBottom: 15
        }
    },
    dropzoneImage: {
        width: '140px !important',
        height: '140px !important',
        border: '2px solid #3498DB !important',
        borderRadius: '0 !important'

    },
    dropzoneBox: {
        width: '140px !important',
        height: '140px !important',
        borderRadius: '0 !important',
    },
    dropzone: {
        width: '140px !important',
        height: '140px !important',
        borderRadius: '0 !important',
        border: '2px solid #3498DB !important'
    },
    containerStyle: {
        alignSelf: 'center'
    },
    descriptionWrapper: {
        "@media screen and (max-width: 768px)": {
            marginBottom: 15
        }
    }
});

class GeneralInfoForm extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            tributeId: this.props.tributeId, // this is needed since we can create a new tribute on avatar save
            previewAvatarBlob: null,
            avatarUri: null,
            previewAvatar: null,
            error: null
        };

        // Bind this to functions
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDrop = this.handleDrop.bind(this);
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleDeleteImage = this.handleDeleteImage.bind(this);
    }

    componentWillMount() {
        this.props.handleGetTributePart(this.props.formSectionName);
    }

    handleDrop(acceptedFiles) {
        if (this.state.tributeId) {
            this.props.saveAvatar(acceptedFiles)
                .then((data) => {
                    this.setState({
                        tributeId: data.tributeId,
                        avatarUri: data.avatarUri,
                        previewAvatarBlob: null,
                        avatarCropUri: null
                    });
                })
                .catch(() => {
                    console.log('**** ERROR ****')
                });
        } else {
            this.setState({
                previewAvatar: acceptedFiles,
                previewAvatarBlob: acceptedFiles.preview
            })
        }
    }

    handleDeleteImage() {
        if (this.state.tributeId) {
            this.props.deleteAvatar(this.state.tributeId)
                .then((data) => {
                    this.setState({
                        avatarUri: null,
                        avatarCropUri: null
                    });
                })
                .catch((error) => {
                    console.log('**** ERROR ****')
                });
        } else {
            this.setState({
                previewAvatarBlob: null
            })
        }
    }

    handleCropImage(crop, scale) {
        this.props.cropAvatar(this.state.tributeId, crop, scale)
            .then((data) => {
                this.setState({
                    tributeId: data.tributeId,
                    avatarCropUri: data.avatarCropUri
                });
            })
            .catch(() => {
                console.log('**** ERROR ****')
            });
    }

    createTribute (model) {
        this.props.createTribute(model)
            .then((data) => {
                this.props.initializeForm(data);
                this.props.nextStep();
            })
            .catch((err) => {
                console.log('**** ERROR ****', err);
            });
    }

    handleSubmit(model) {
        if(model.tributeId) {
            this.props.handleSaveTributePart(model, this.props.formSectionName);
        } else {
            if (this.state.previewAvatar) {
                this.props.saveAvatar(this.state.previewAvatar)
                    .then((data) => {
                        model.tributeId = data.tributeId;
                        this.props.handleSaveTributePart(model, this.props.formSectionName);
                    })
                    .catch((err) => {
                        console.log('**** ERROR ****', err);
                    });
            } else {
                this.createTribute(model);
            }
        }
    }

    render() {
        const {handleSubmit, submitting, tribute = {}} = this.props;
        const {error} = this.state;
        console.log(this.state);

        return (
            <div style={{textAlign: 'center'}}>

                {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}
                <div className={css(styles.formWrapper)}>
                    <Row>
                        <Col lg={3} md={3} xs={12}>
                            <div className={css(styles.text)}>Add a Profile Picture</div>
                            <div className={css(styles.photoWrapper)}>
                                <ImageCropUploader imageUri={tribute.avatarUri || this.state.avatarUri}
                                                   cropUri={tribute.avatarCropUri || this.state.avatarCropUri}
                                                   previewUrl={defaultAvatar}
                                                   previewAvatarBlob={this.state.previewAvatarBlob}
                                                   height={100}
                                                   width={100}
                                                   border={0}
                                                   borderRadius={100}
                                                   onDrop={this.handleDrop}
                                                   onCrop={this.handleCropImage}
                                                   onDelete={this.handleDeleteImage}
                                                   styles={styles}
                                                   containerStyle={css(styles.containerStyle)}
                                />
                            </div>
                        </Col>
                        <Col lg={9} md={9} xs={12}>
                            <Form onSubmit={handleSubmit(this.handleSubmit)}>
                                <Row>
                                    <Col lg={8} md={8} sm={6} className={css(styles.descriptionWrapper)}>
                                        <Field name="firstName"
                                               component={BlockInput}
                                               type="text"
                                               placeholder="First Name"/>
                                        <Field name="lastName"
                                               component={BlockInput}
                                               type="text"
                                               placeholder="Last Name"/>
                                        <Field name='summary' component={EditorRichText} placeholder='Few words...'/>
                                    </Col>
                                    <Col lg={4} md={4} sm={6}>
                                        <Col lg={12} md={12} sm={12} style={{padding: 0}}>
                                            <FormSection name={this.props.formSectionName}>
                                                <Field name="dateBorn"
                                                       component={BlockDatePicker}
                                                       dateFormat="MM/DD/YYYY"
                                                       type="text"
                                                       placeholder="Date born"
                                                       format={formatDate}
                                                       addonAfter={<AddonAfterDate/>}/>
                                                <Field name="datePassed"
                                                       component={BlockDatePicker}
                                                       dateFormat="MM/DD/YYYY"
                                                       type="text"
                                                       placeholder="Date passed"
                                                       format={formatDate}
                                                       addonAfter={<AddonAfterDate/>}/>
                                            </FormSection>
                                        </Col>
                                        <Col lg={12} md={12} sm={12} style={{padding: 0}}>
                                            <TabButtonToolbar prevStep={this.props.prevStep} nextStep={this.props.nextStep}
                                                              cancelEdit={this.cancelEdit} indexTab={this.props.indexTab}
                                                              lengthTabs={this.props.lengthTabs}
                                                              submitting={submitting}/>
                                        </Col>
                                    </Col>
                                </Row>
                            </Form>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}


const mapStateToProps = (state, props) => {
    return {
        // tribute: selectors.tributeById(state, props.tributeId)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        createTribute: (model) => {
            return dispatch(actions.tributes.createTribute(model))
        },
        saveAvatar: (file) => {
            return dispatch(actions.tributes.saveAvatar(props.tributeId, file))
        },
        cropAvatar: (tributeId, crop, scale) => {
            return dispatch(actions.tributes.cropAvatar(tributeId, crop, scale))
        },
        deleteAvatar: (tributeId) => {
            return dispatch(actions.tributes.deleteAvatar(tributeId))
        }
    }
};

GeneralInfoForm = reduxForm({
    form: TRIBUTE_FORM,  // a unique identifier for this form,
    destroyOnUnmount: false, //        <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount// a unique identifier for this form
    validate: validTribute
})(GeneralInfoForm);

GeneralInfoForm = connect(mapStateToProps, mapDispatchToProps)(GeneralInfoForm);

export default GeneralInfoForm;
