import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import TributeFormWrap from './TributeFormWrap'


class EditTribute extends React.Component {

    render() {

        return (
            <TributeFormWrap tributeId={this.props.tributeId || this.props.location.state.tributeId}/>
        );
    }
}

const mapStateToProps = (state, props) => {
    const tributeId = props.tributeId || props.location.state.tributeId;

    return {
        tributeId,
    };
};

export default withRouter(connect(mapStateToProps, null)(EditTribute));
