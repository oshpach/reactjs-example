import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {StyleSheet, css} from 'aphrodite/no-important'
// import {withRouter} from 'react-router'
import {Tabs, Tab} from 'react-bootstrap'
import {
    initialize,
    isInvalid,
    isValid,
    isDirty,
    isPristine,
    destroy,
    getFormValues,
} from 'redux-form';
// import {StyleSheet, css} from 'aphrodite/no-important'
// import CreateTribute from './CreateTribute'

import {TRIBUTE_FORM} from '../../formNames';

import GeneralInfoForm  from './GeneralInfoForm'
import InterestsForm  from './InterestsForm'
import EducationForm from './EducationForm'
import MilitaryForm from './MilitaryForm'
import JobForm from './JobForm'
// import ImportantDatesForm from './ImportantDatesForm'

// import * as selectors from '../../selectors'

import actions from '../../actions'
// import Common from '../../../Common'
import './CreateTribute.css'

const styles = StyleSheet.create({
    leftCircle: {
        display: 'block',
        position: 'absolute',
        left: -10,
        top: 87,
        backgroundColor: '#F0F0F0',
        width: 20,
        height: 20,
        borderRadius: '50%',
        zIndex: 1
    },
    rightCircle: {
        display: 'block',
        position: 'absolute',
        right: -10,
        top: 87,
        backgroundColor: '#F0F0F0',
        width: 20,
        height: 20,
        borderRadius: '50%',
        zIndex: 1
    },
    divider: {
        display: 'block',
        width: 50,
        height: 1,
        backgroundColor: '#3498DB',
        margin: '0 auto 30px auto'
    },
    title: {
        fontWeight: 'bold',
        color: '#3498DB',
        textTransform: 'uppercase'
    }
});


class TributeFormWrap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            indexTab: 0
        };
        this.nextStep = this.nextStep.bind(this);
        this.prevStep = this.prevStep.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.onSelect = this.onSelect.bind(this);
        this.handleGetTributePart = this.handleGetTributePart.bind(this);
        this.handleSaveTributePart = this.handleSaveTributePart.bind(this);
    }

    componentDidMount() {
        this.props.initializeForm(this.props.initialValues);
    }

    componentWillUnmount() {
        this.props.destroyForm();
    }

    tabsOptions = [
        {
            title: "Main",
            component: GeneralInfoForm,
            props: {
                formSectionName: "main",
            }
        },
        {
            title: "Interests",
            component: InterestsForm,
            props: {
                formSectionName: "interests",
            }
        },
        {
            title: "Education",
            component: EducationForm,
            props: {
                formSectionName: "education",
            }
        },
        {
            title: "Military",
            component: MilitaryForm,
            props: {
                formSectionName: "military",
            }
        },
        {
            title: "Job",
            component: JobForm,
            props: {
                formSectionName: "job",
            }
        },

        // {
        //     title: "Dates",
        //     component: ImportantDatesForm,
        //     props: {
        //         formSectionName: "dates",
        //     }
        // }
    ];

    onSelect(key) {
        const {isFormValid, isFormPristine} = this.props;

        // if (isFormValid && isFormPristine) {
            this.setState({indexTab: key});
        // }
    }

    handleSelect(key) {
        this.setState({indexTab: key});
    }

    nextStep = () => {
        if (this.state.indexTab === this.tabsOptions.length - 1) {
            this.props.router.push("/mytributes");
        } else {
            this.handleSelect(this.state.indexTab + 1);
        }
    };

    prevStep = () => {
        if (this.state.indexTab === 0) {
            this.props.router.push("/mytributes");
        } else {
            this.handleSelect(this.state.indexTab - 1);
        }
    };

    handleGetTributePart(formSectionName) {
        const model = this.props.valuesForm || {};
        const truid = model.tributeId || model.tributeUri || this.props.tributeId;

        truid && this.props.getTributePart(truid, formSectionName)
            .then((data) => {
                this.props.initializeForm((formSectionName !== "main") ? Object.assign(model, {[formSectionName]: data}) : data);
            })
            .catch((err) => {
                console.log('**** ERROR ****', err);
            });
    }

    handleSaveTributePart(model, formSectionName) {
        if (this.props.isFormDirty) {
            this.props.saveTributePart(model.tributeId, (formSectionName !== "main") ? model[formSectionName] : model, formSectionName)
                .then((data) => {
                    this.props.initializeForm((formSectionName !== "main") ? Object.assign(model, {[formSectionName]: data}) : data);
                    this.nextStep();
                })
                .catch((err) => {
                    console.log('**** ERROR ****', err);
                });
        } else {
            this.nextStep();
        }
    }

    renderTab(options, index, lengthTabs) {
        const Component = options.component;
        const commonProps = {
            valuesForm: this.props.valuesForm,
            handleGetTributePart: this.handleGetTributePart,
            handleSaveTributePart: this.handleSaveTributePart,
            initializeForm: this.props.initializeForm,
        };
        return <Component {...options.props} {...commonProps} prevStep={this.prevStep} nextStep={this.nextStep}
                          indexTab={index} lengthTabs={lengthTabs}/>;
    }

    render() {
        const {indexTab} = this.state;
        const {isFormInvalid, isFormDirty} = this.props;
        // disabled={isFormInvalid || isFormDirty}
        return (
            <div id="createTributeLists"
                 style={{margin: '80px auto 0 auto', maxWidth: '1000px', width: '100%', textAlign: 'center', position: 'relative'}}>
                <span className={css(styles.leftCircle)}></span>
                <h2 className={css(styles.title)}>Create a tribute</h2>
                <span className={css(styles.divider)}></span>
                <Tabs activeKey={indexTab} onSelect={this.onSelect} id="uncontrolled-tab-example">
                    {this.tabsOptions.map((options, index) => (
                        <Tab eventKey={index} title={options.title} key={index}>
                            {indexTab === index && this.renderTab(options, index, this.tabsOptions.length)}
                        </Tab>
                    ))}
                </Tabs>
                <span className={css(styles.rightCircle)}></span>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        isFormValid: isValid(TRIBUTE_FORM)(state),
        isFormPristine: isPristine(TRIBUTE_FORM)(state),
        isFormInvalid: isInvalid(TRIBUTE_FORM)(state),
        isFormDirty: isDirty(TRIBUTE_FORM)(state),
        valuesForm: getFormValues(TRIBUTE_FORM)(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        initializeForm: (model) => {
            return dispatch(initialize(TRIBUTE_FORM, model))
        },
        destroyForm: () => {
            return dispatch(destroy(TRIBUTE_FORM))
        },
        saveTributePart: (tributeId, model, part) => {
            return dispatch(actions.tributes.saveTributePart(tributeId, model, part))
        },
        getTributePart: (tributeId, part) => {
            return dispatch(actions.tributes.getTributePart(tributeId, part))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TributeFormWrap));
