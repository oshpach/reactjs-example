import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Button, ButtonToolbar, Row, Col} from 'react-bootstrap'
import {
    isInvalid,
    isValid,
    isDirty,
    isPristine,
} from 'redux-form';

import Common from '../../../Common'

import {TRIBUTE_FORM} from '../../formNames';


const {components:{BlockTile}} = Common;

const styles = StyleSheet.create({
    buttons: {
        width: '100%',
        marginLeft: '0 !important',
        height: 40,
        border: '1px solid #3498DB',
        color: '#3498DB',
        ":hover": {
            backgroundColor: '#3498DB',
            color: 'white'
        }
    },
    marginLeft: {
        marginLeft: '0 !important',
        height: 100,
        position: 'relative'
    },
    nextButton: {
        position: 'absolute',
        bottom: 0,
        left: 0
    }
});


class TabButtonToolbar extends React.Component {

    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {};

    }

    render() {

        const {prevStep} = this.props;
        const {submitting, indexTab, lengthTabs, isFormDirty, buttonsWrapper, prevStyle, nextStyle} = this.props;

        return (
                <ButtonToolbar className={`${css(styles.marginLeft)} ${buttonsWrapper}`}>
                        <Button
                            bsStyle='default'
                            className={`${css(styles.buttons)} ${prevStyle}`}
                            onClick={prevStep}>
                            {
                                (indexTab === 0)
                                    ? "Cancel"
                                    : ((isFormDirty) ? "Prev without saving" : "Prev")
                            }
                        </Button>
                        <Button
                            bsStyle='default'
                            type="submit"
                            className={`btn-blue ${css(styles.buttons)} ${css(styles.nextButton)} ${nextStyle}`}
                            disabled={submitting}>
                            {
                                (indexTab === (lengthTabs - 1))
                                    ? "Save"
                                    : ((isFormDirty) ? "Save and Next" : "Next")
                            }
                        </Button>
                </ButtonToolbar>
        )
    }

}

const mapStateToProps = (state, props) => {
    return {
        isFormValid: isValid(TRIBUTE_FORM)(state),
        isFormPristine: isPristine(TRIBUTE_FORM)(state),
        isFormInvalid: isInvalid(TRIBUTE_FORM)(state),
        isFormDirty: isDirty(TRIBUTE_FORM)(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {}
};

export default connect(mapStateToProps, mapDispatchToProps)(TabButtonToolbar);
