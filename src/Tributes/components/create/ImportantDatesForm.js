import React from 'react'
// import {connect} from 'react-redux'
import {Form, Field, FieldArray, reduxForm} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Row, Col} from 'react-bootstrap'
import Common from '../../../Common'
import TabButtonToolbar from './TabButtonToolbar'

import {validTribute} from '../../validators';

import {TRIBUTE_FORM} from '../../formNames';

const {
    components:{BlockInput, BlockDatePicker, EditorRichText, BlockTile},
    formatters: {formatDate}
} = Common;

const AddonAfterDate = (props)=> {
    return (
        <span style={{cursor: 'pointer'}} className="react-datepicker-ignore-onclickoutside" onClick={props.onClick}
              value={props.value} onChange={props.onChange} disabled={props.disabled}><i className="flaticon-time"/>
        <i className="fa fa-long-arrow-down"/>
    </span>
    );
};

const styles = StyleSheet.create({
    headings: {
        fontWeight: 'bold',
        color: '#3498DB'
    },
    info: {
        color: 'lightgrey',
        fontWeight: 'bold',
        fontStyle: 'italic',
        textAlign: 'center'
    },
    infoWrap: {
        textAlign: 'left',
        marginBottom: '50px'
    },
    addButton: {
        display: 'inline-block',
        borderRadius: '50%',
        border: '2px solid #3498DB',
        color: '#3498DB',
        backgroundColor: 'transparent',
        width: '50px',
        height: '50px',
        marginBottom: '15px'
    }
});


class ImportantDatesForm extends React.Component {

    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {};

        // Bind this to functions
        this.submit = this.submit.bind(this);
    }

    componentWillMount() {
        this.props.handleGetTributePart(this.props.formSectionName);
    }

    submit(model) {
        this.props.handleSaveTributePart(model, this.props.formSectionName);
    }

    renderDates({fields}) {
        return (
            <div>
                {fields.map((member, index) => (
                    <div key={index} style={{marginBottom: '15px'}}>
                        <button
                            type="button"
                            onClick={() => fields.remove(index)}
                            style={{marginBottom: '15px'}}
                        >Remove
                        </button>
                        <Row>
                            <Col md={6} sm={12}>
                                <Field name={`${member}.date`}
                                       component={BlockDatePicker}
                                       dateFormat="MM/DD/YYYY"
                                       type="text"
                                       placeholder="Important Date"
                                       format={formatDate}
                                       addonAfter={<AddonAfterDate/>}/>
                            </Col>
                            <Col md={6} sm={12}>
                                <Field
                                    name={`${member}.title`}
                                    component={BlockInput}
                                    type="text"
                                    placeholder="Title"
                                />
                            </Col>
                            <Col md={12}>
                                <Field name={`${member}.description`} component={EditorRichText}
                                       placeholder='Descriptions...'/>
                            </Col>
                        </Row>
                    </div>
                ))}
                <div className={css(styles.addPositionBlock)}>
                    <button type="button" onClick={() => fields.push({})} className={css(styles.addButton)}><i
                        className="fa fa-plus" aria-hidden="true"/></button>
                </div>
            </div>
        )
    }

    render() {

        const {handleSubmit, submitting, error} = this.props;
        return (
            <div>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <BlockTile>
                        <h1 className={css(styles.headings)}>Important Dates</h1>
                        <hr/>

                        {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                        <FieldArray name="dates" component={this.renderDates}/>
                    </BlockTile>

                    <TabButtonToolbar prevStep={this.props.prevStep} nextStep={this.props.nextStep}
                                      cancelEdit={this.cancelEdit} indexTab={this.props.indexTab}
                                      lengthTabs={this.props.lengthTabs}
                                      submitting={submitting}/>
                </Form>
            </div>
        )
    }

}

ImportantDatesForm = reduxForm({
    form: TRIBUTE_FORM,  // a unique identifier for this form,
    destroyOnUnmount: false, //        <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount// a unique identifier for this form
    validate: validTribute, // <------ unregister fields on unmount// a unique identifier for this form
})(ImportantDatesForm);


export default ImportantDatesForm;
