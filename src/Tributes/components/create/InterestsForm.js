import React from 'react'
import {Form, Field, FormSection, FieldArray, reduxForm} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Row, Col} from 'react-bootstrap'
import Common from '../../../Common'
import TabButtonToolbar from './TabButtonToolbar'

import {validTribute} from '../../validators';

import {TRIBUTE_FORM} from '../../formNames';

const {components:{BlockSelectCountry, BlockTextArea, BlockTile}} = Common;


const styles = StyleSheet.create({
    headings: {
        fontWeight: 'bold',
        color: '#3498DB'
    },
    info: {
        color: 'lightgrey',
        fontWeight: 'bold',
        fontStyle: 'italic',
        textAlign: 'center'
    },
    infoWrap: {
        textAlign: 'left',
    },
    addButton: {
        display: 'inline-block',
        borderRadius: '50%',
        border: '2px solid #3498DB',
        color: 'white',
        backgroundColor: '#3498DB',
        width: '50px',
        height: '50px',
        marginBottom: '15px'
    },
    formWrapper: {
        backgroundColor: 'white',
        borderRadius: 10,
        border: '1px solid #3498DB',
        paddingLeft: 30,
        paddingRight: 30,
        width: '80%',
        margin: '30px auto'
    },
    deleteButton: {
        width: '100%',
        height: 45,
        backgroundColor: 'transparent',
        border: '1px solid #3498DB',
        color: '#3498DB',
        ":hover": {
            color: 'white',
            backgroundColor: '#3498DB'
        },
        "@media screen and (max-width: 768px)": {
            marginBottom: 10
        }
    },
    prevButton: {
        width: 150,
        position: 'absolute',
        left: 0,
        top: 0,
        "@media screen and (max-width: 500px)": {
            position: 'static',
            width: '100%',
            marginBottom: 10
        }
    },
    nextButton: {
        width: 150,
        position: 'absolute',
        right: '0 !important',
        top: '0 !important',
        bottom: 'unset !important',
        left: 'unset !important',
        "@media screen and (max-width: 500px)": {
            position: 'static',
            width: '100%'
        }
    },
    styleRegion:{
        "@media screen and (max-width: 991px)": {
            marginTop: 15
        }
    }

});

class InterestsForm extends React.Component {

    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {};

        // Bind this to functions
        this.submit = this.submit.bind(this);
    }

    componentWillMount() {
        this.props.handleGetTributePart(this.props.formSectionName);
    }

    submit(model) {
        this.props.handleSaveTributePart(model, this.props.formSectionName);
    }

    renderWhereLived({fields}) {
        return (
            <div>
                <p className={css(styles.info)}>Country where lived</p>
                {fields.map((member, index) => (
                    <Row key={index}>
                        <Col lg={9} md={9} sm={9} className={css(styles.infoWrap)}>
                            <Field
                                name={`${member}`}
                                component={BlockSelectCountry}
                                styleRegion={css(styles.styleRegion)}
                            />
                        </Col>
                        <Col lg={3} md={3} sm={3}>
                            <button
                                type="button"
                                onClick={() => fields.remove(index)}
                                className={css(styles.deleteButton)}
                            >Remove
                            </button>
                        </Col>
                    </Row>
                ))}
                <div>
                    <button type="button" onClick={() => fields.push({})} className={css(styles.addButton)}><i
                        className="fa fa-plus" aria-hidden="true"/></button>
                </div>
            </div>
        )
    }

    render() {

        const {handleSubmit, submitting, error} = this.props;
        return (
            <div className={css(styles.formWrapper)}>
                <Form onSubmit={handleSubmit(this.submit)}>
                        <h1 className={css(styles.headings)}>Interests</h1>
                        <hr/>

                        {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                        <FormSection name={this.props.formSectionName}>
                            <FieldArray name="whereLived" component={this.renderWhereLived}/>
                            <Row>
                                <Col lg={6} md={6} sm={6}>
                                    <p className={css(styles.info)}>Books you've read</p>
                                    <Field
                                        name="booksRead"
                                        component={BlockTextArea}
                                        rows="3"
                                    />
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                    <p className={css(styles.info)}>Songs you like the most</p>
                                    <Field
                                        name="songsLike"
                                        component={BlockTextArea}
                                        rows="3"
                                    />
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                    <p className={css(styles.info)}>Movies you like the most</p>
                                    <Field
                                        name="moviesLike"
                                        component={BlockTextArea}
                                        rows="3"
                                    />
                                </Col>
                                <Col lg={6} md={6} sm={6}>
                                    <p className={css(styles.info)}>What are you interested in?</p>
                                    <Field
                                        name="otherInterests"
                                        component={BlockTextArea}
                                        rows="3"
                                    />
                                </Col>
                            </Row>
                        </FormSection>

                    <TabButtonToolbar prevStep={this.props.prevStep} nextStep={this.props.nextStep}
                                      cancelEdit={this.cancelEdit} indexTab={this.props.indexTab}
                                      lengthTabs={this.props.lengthTabs}
                                      prevStyle={`${css(styles.prevButton)} pull-left`}
                                      nextStyle={`${css(styles.nextButton)} pull-right`}
                                      submitting={submitting}/>
                </Form>
            </div>
        )
    }
}

InterestsForm = reduxForm({
    form: TRIBUTE_FORM,  // a unique identifier for this form,
    destroyOnUnmount: false, //        <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount// a unique identifier for this form
    validate: validTribute
})(InterestsForm);


export default InterestsForm;
