import React from 'react'
import {Form, Field, FieldArray, reduxForm} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Row, Col} from 'react-bootstrap'
import Common from '../../../Common'
import TabButtonToolbar from './TabButtonToolbar'

import {validTribute} from '../../validators';

import {TRIBUTE_FORM} from '../../formNames';

const {components:{BlockInput, BlockSelectCountry, BlockTile}} = Common;

const styles = StyleSheet.create({
    headings: {
        fontWeight: 'bold',
        color: '#3498DB'
    },
    info: {
        color: 'lightgrey',
        fontWeight: 'bold',
        fontStyle: 'italic',
        textAlign: 'center'
    },
    infoWrap: {
        textAlign: 'left',
    },
    addButton: {
        display: 'inline-block',
        borderRadius: '50%',
        border: '2px solid #3498DB',
        color: '#3498DB',
        backgroundColor: 'transparent',
        width: '50px',
        height: '50px',
        marginBottom: '15px'
    },
    deleteButton: {
        width: '100%',
        height: 45,
        backgroundColor: 'transparent',
        border: '1px solid #3498DB',
        color: '#3498DB',
        ":hover": {
            color: 'white',
            backgroundColor: '#3498DB'
        },
        "@media screen and (max-width: 768px)": {
            marginBottom: 10
        }
    },
    wrapper: {
        width: '80%',
        margin: '30px auto',
        background: 'white',
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 20
    },
    prevButton: {
        width: 150,
        position: 'absolute',
        left: 0,
        top: 0,
        "@media screen and (max-width: 500px)": {
            position: 'static',
            width: '100%',
            marginBottom: 10
        }
    },
    nextButton: {
        width: 150,
        position: 'absolute',
        right: '0 !important',
        top: '0 !important',
        bottom: 'unset !important',
        left: 'unset !important',
        "@media screen and (max-width: 500px)": {
            position: 'static',
            width: '100%'
        }
    },
    styleRegion:{
        "@media screen and (max-width: 991px)": {
            marginTop: 15
        }
    }
});


class EducationForm extends React.Component {

    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {};

        // Bind this to functions
        this.submit = this.submit.bind(this);
    }

    componentWillMount() {
        this.props.handleGetTributePart(this.props.formSectionName);
    }

    submit(model) {
        this.props.handleSaveTributePart(model, this.props.formSectionName);
    }

    renderEducation({fields}) {
        return (
            <div>
                {fields.map((member, index) => (
                    <Row key={index}>
                        <Col md={12} sm={12} className={css(styles.infoWrap)}>
                            <p className={css(styles.info)}>Country where studied</p>
                            <Row>
                                <Col lg={9} md={9} sm={9}>
                                    <Field
                                        name={`${member}.location`}
                                        component={BlockSelectCountry}
                                        styleRegion={css(styles.styleRegion)}
                                    />
                                </Col>
                                <Col lg={3} md={3} sm={3}>
                                    <button
                                        type="button"
                                        className={css(styles.deleteButton)}
                                        onClick={() => fields.remove(index)}
                                    >Remove
                                    </button>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={12} sm={12}>
                            <p className={css(styles.info)}>School</p>
                            <Field
                                type="text"
                                name={`${member}.school`}
                                placeholder="School"
                                component={BlockInput}
                            />
                        </Col>
                        <Col md={12} sm={12}>
                            <p className={css(styles.info)}>University</p>
                            <Field
                                type="text"
                                name={`${member}.university`}
                                placeholder="University"
                                component={BlockInput}
                            />
                        </Col>
                    </Row>
                ))}
                <div>
                    <button type="button" onClick={() => fields.push({})} className={css(styles.addButton)}><i
                        className="fa fa-plus" aria-hidden="true"/></button>
                </div>
            </div>
        )
    }

    render() {
        const {handleSubmit, submitting, error} = this.props;
        return (
            <div className={css(styles.wrapper)}>
                <Form onSubmit={handleSubmit(this.submit)}>
                    <div>
                        <h1 className={css(styles.headings)}>Education</h1>
                        <hr/>

                        {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                        <FieldArray name={this.props.formSectionName} component={this.renderEducation}/>
                    </div>

                    <TabButtonToolbar prevStep={this.props.prevStep} nextStep={this.props.nextStep}
                                      indexTab={this.props.indexTab} lengthTabs={this.props.lengthTabs}
                                      submitting={submitting}
                                      prevStyle={`${css(styles.prevButton)} pull-left`}
                                      nextStyle={`${css(styles.nextButton)} pull-right`}/>
                </Form>
            </div>
        )
    }

}

EducationForm = reduxForm({
    form: TRIBUTE_FORM,
    destroyOnUnmount: false, //        <------ preserve form data
    forceUnregisterOnUnmount: true, // <------ unregister fields on unmount// a unique identifier for this form
    validate: validTribute // <------ unregister fields on unmount// a unique identifier for this form
})(EducationForm);


export default EducationForm;
