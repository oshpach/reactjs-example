import React from 'react';
import {Modal, Button} from 'react-bootstrap'

const ConfirmDeleteModal = props => {
    return (
        <Modal show={props.show} bsSize='small'>
            <Modal.Header>
                <Modal.Title>{props.title}</Modal.Title>
            </Modal.Header>

            <Modal.Body>
                {props.body}
            </Modal.Body>

            <Modal.Footer>
                <Button bsStyle='danger' onClick={props.onDelete}>Delete</Button>
                <Button bsStyle='default' onClick={props.onCancel}>Cancel</Button>
            </Modal.Footer>

        </Modal>

    )
};

export default ConfirmDeleteModal;
