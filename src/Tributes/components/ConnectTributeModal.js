import React from 'react';
import {reduxForm, Field} from 'redux-form'
import {Modal, Grid, Button} from 'react-bootstrap'
import {StyleSheet, css} from 'aphrodite/no-important'

import Common from '../../Common'
import {defaultAvatar} from '../../Common/images'

const {components: {EditorRichText, ImageLoader}}= Common;


const styles = StyleSheet.create({
    image: {
        borderRadius: '50%',
        maxWidth: '150px',
        width: '100%',
        height: '150px'
    },
    containerStyle:{
        textAlign: 'center',
        display: 'block !important'
    },
    tributeName: {
        textAlign: 'center'
    },
    linkButton: {
        marginTop: '10px'
    },
    closeButton: {
        position: 'absolute',
        right: '10px',
        top: '10px',
        cursor: 'pointer'
    }
});


const ConnectTributeModal = (props) => {

    const close = function () {
        props.hideModal();
    };

    const submit = (value) => {
        console.log(value);
        props.connectTribute(value.connectText);
        props.hideModal();

    };
    const {handleSubmit, tribute = []} = props;
        return (
            <Grid>
                <Modal show={true} onHide={close}>
                    <Modal.Body>
                        <span className={css(styles.closeButton)} onClick={() => {props.hideModal()}}><i className="fa fa-times" aria-hidden="true"></i></span>
                        <ImageLoader
                            className={css(styles.image)}
                            src={tribute.avatarUri}
                            previewSrc={defaultAvatar}
                            containerClassName={css(styles.containerStyle)}
                        />
                        <p className={css(styles.tributeName)}>{tribute.firstName ? (tribute.firstName + ' ' + tribute.lastName) : null}</p>
                        <form onSubmit={handleSubmit(submit)}>
                            <Field name="connectText" component={EditorRichText} type="text" placeholder="Type here..."/>
                            <div className={css(styles.linkText)}>
                                <Button className={css(styles.linkButton)}
                                        bsStyle="success"
                                        type="submit"
                                        >
                                    Connect
                                </Button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </Grid>
        )

};

const ConnectModal = reduxForm({
    form: 'ConnectForm'
})(ConnectTributeModal);

export default ConnectModal;