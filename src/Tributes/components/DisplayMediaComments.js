import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import moment from 'moment'
import {List} from 'immutable'
import * as selectors from '../selectors'
import actions from '../actions'
import Common from '../../Common'

import {defaultPersona} from '../../Common/images'

const {components: {ImageLoader}} = Common;

const styles = StyleSheet.create({
    commentBlock: {
        textAlign: 'left'
    },

    userInfo: {
        verticalAlign: 'top'
    },

    userAvatar: {
        height: '32px',
        width: '32px',
        display: 'inline-block',
        verticalAlign: 'top'
    },

    userText: {
        display: 'inline-block',
        fontSize: 'small',
        padding: '0 10px'
    },

    commentText: {
        padding: '10px'
    }

});

export class DisplayMediaComments extends React.Component {
    componentWillMount() {
        this.props.getMediaComments();
    }

    render() {
        const {comments} = this.props;

        return (
            <div>
                {
                    List(comments).map((comment, index) =>
                        <div className={css(styles.commentBlock)} key={comment.commentId}>
                           <hr style={{marginTop: '10px'}}/>
                            <div className={css(styles.userInfo)}>
                                <ImageLoader className={css(styles.userAvatar)}
                                             containerStyle={{verticalAlign: 'top', display: 'inline-block'}}
                                             src={comment.user.avatarUrl}
                                             previewSrc={defaultPersona}/>
                                <div className={css(styles.userText)}>
                                    {comment.user.firstName} {comment.user.lastName}
                                    <br/>
                                    {moment(comment.createDate).format('l')}
                                </div>
                            </div>

                            <div className={css(styles.commentText)}>{comment.comment}</div>
                        </div>
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    return {
        comments: selectors.commentsByMediaId(state, props.media.mediaId),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getMediaComments: () => {dispatch(actions.media.getMediaComments(props.media.mediaId))}
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayMediaComments);