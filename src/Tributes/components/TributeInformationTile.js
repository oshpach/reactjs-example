import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {StyleSheet, css} from 'aphrodite/no-important'

import moment from 'moment'
import {Button, Tabs, Tab} from 'react-bootstrap'
import renderHTML from 'react-render-html';
import {defaultAvatar} from '../../Common/images'
import actions from '../actions'
import * as selectors from '../selectors'
import AlbumSection from './albums/AlbumSection'
import Common from '../../Common'
import ConnectModal from './ConnectTributeModal'
const {components: {ImageLoader, BlockTile}} = Common;


const styles = StyleSheet.create({
    containerStyle: {
        width: '80%'
    },
    tributeDescription: {
        position: 'absolute',
        left: '45%',
        top: '0',
        fontSize: '25px'
    },
    editButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: 10,
    },

    editTributeButton: {
        marginRight: '15px',
        cursor: 'pointer'
    },

    deleteTributeButton: {
        cursor: 'pointer'
    },

    tile: {
        textAlign: 'center',
        position: 'relative !important',
        paddingLeft: '0 !important',
        paddingRight: '0 !important',
        background: 'none',
        height: 200,
        marginBottom: '320px !important',
        ':hover .editButtons': {
            display: 'block'
        },
        "@media screen and (max-width: 600px)": {
            minHeight: 350
        }
    },

    mainTitle: {
        color: '#3498DB',
        marginTop: '0',
        textTransform: 'uppercase',
        letterSpacing: '1px',
        marginBottom: '50px',
        "@media screen and (max-width: 768px)": {
            marginBottom: '30px',
            fontSize: '26px'
        },
        "@media screen and (max-width: 450px)": {
            marginBottom: '20px',
            fontSize: '12px'
        }
    },
    image: {
        width: '20%',
        // left: '-30%',
        // position: 'relative',
        display: 'inline-block',
        borderRadius: '50%',
        border: '5px solid #F0F0F0',
        position: 'absolute',
        left: '10%',
        top: '-120px',
        "@media screen and (max-width: 768px)": {
            top: -60
        },
        "@media screen and (max-width: 600px)": {
            position: 'static',
            width: '160px'
        }
    },

    datesText: {
        color: '#428bca',
        marginTop: 0,
        marginBottom:'20px',
        '@media screen and (max-width: 900px)': {
            fontSize: "20px",
            marginBottom: '15px'
        },
        '@media screen and (max-width: 678px)': {
            fontSize: "15px",
            marginBottom: '12px'
        },
        '@media screen and (max-width: 528px)': {
            fontSize: "12px",
            marginBottom: '10px'
        },
        '@media screen and (max-width: 450px)': {
            fontSize: "10px",
            marginBottom: '6px'
        }
    },

    summary: {
        width: '73%',
        margin: '15px auto',
        textAlign: 'left',
        padding: '10px 5px',
        "@media screen and (max-width: 768px)": {
            fontSize: '14px'
        },
        "@media screen and (max-width: 450px)": {
            fontSize: '12px'
        }
    },

    connectContainer: {
        position: 'absolute',
        right: 30,
        top: 30
    },

    connectButton: {

    },

    albumButtonContainer: {
        position: 'absolute',
        bottom: '10px',
        left: '10px'
    },
    tributeInfo: {
        width: '100%',
        position: 'relative',
        height: '100%',
        maxHeight: '400px',
        marginTop: '50px',
        "@media screen and (max-width: 600px)": {
            position: 'static'
        }
    },
    albumsTitle: {
        textAlign: 'center',
        color: '#3498DB',
        marginTop: '40px',
        marginBottom: '10px',
        fontWeight: 'bold',
        fontSize: '30px'
    },
    divider: {
        display: 'block',
        width: '40px',
        height: '2px',
        backgroundColor: '#3498DB',
        margin: '0 auto 40px auto'
    },
    infoLine: {
        position: 'relative',
        bottom: '-30px',
        width: '100%',
        height: '50px',
        background: 'white',
        display: 'flex',
        justifyContent: 'center',
        "@media screen and (max-width: 768px)": {
            bottom: -60
        },
        "@media screen and (max-width: 600px)": {
            position: 'absolute',
            bottom: 0
        }
    },
    infoLineBlock: {
        display: 'flex',
        justifyContent: 'space-around',
        marginLeft: '25%',
        width: '100%',
        "@media screen and (max-width: 768px)": {
            fontSize: 12
        },
        "@media screen and (max-width: 600px)": {
            margin: '0 auto',
            padding: '0 !important',
        }
    },
    infoLineItem: {
        alignSelf: 'center',
        listStyle: 'none',
        fontWeight: 'bold',
        color: '#3498DB',
        textShadow: '0 0 10px rgba(0, 0, 0, 0.1)',
        padding: '10px 20px',
        cursor: 'pointer',
        "@media screen and (max-width: 600px)": {
            padding: '7px 17px'
        }
    },
    itemActive: {
        borderRadius: '20px',
        backgroundColor: '#3498DB',
        color: 'white'
    },
    descriptionBlock: {
        position: 'absolute',
        left: '30%',
        display: 'flex',
        height: 100
    }
});

export class TributePage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            isActive: 1
        };

        // Bind this to functions
        this.allowedEdit = this.allowedEdit.bind(this);
        this.allowedDelete = this.allowedDelete.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.showModal = this.showModal.bind(this);
        this.changeDescription = this.changeDescription.bind(this);
    }

    componentWillMount(){
        window.scrollTo(0, 0);
    }

    formatDates(born, passed) {
        const dateBorn = moment(born, moment.ISO_8601);
        const datePassed = moment(passed, moment.ISO_8601);

        if (dateBorn.isValid() && datePassed.isValid()) {
            return <div className={css(styles.datesText)}>{dateBorn.format('LL')} - {datePassed.format('LL')}</div>
        }

        if (dateBorn.isValid()) {
            return <div className={css(styles.datesText)}>Born:&nbsp;{dateBorn.format('LL')}&nbsp;
                (age {moment().diff(dateBorn, 'years')})</div>
        }

        if (datePassed.isValid()) {
            return <div>Died: {dateBorn.format('LL')}</div>
        }
    }



    allowedEdit() {
        const {tribute = {}} = this.props;
        return tribute.role && (tribute.role.toLowerCase() === 'owner' || tribute.role.toLowerCase() === 'manager')
    }

    allowedDelete() {
        const {tribute = {}} = this.props;
        return tribute.role && tribute.role.toLowerCase() === 'owner';
    }
    hideModal(){
        this.setState({
            showModal: false
        });
    }

    changeDescription(index){
        this.setState({
            isActive: index
        });
    }
    showModal(){
        this.setState({
            showModal: true
        });
    }

    render() {
        const {tribute = {}, connectTribute=''} = this.props;
        const connected = this.props.connectedToTribute === true;
        console.log(tribute);
        return (
            <div>
                {this.state.showModal ? <ConnectModal connectTribute={connectTribute} tribute={tribute} hideModal={this.hideModal}/> : null}
                <BlockTile className={css(styles.tile) + ` ${this.props.className}`}>

                    <h1 className={css(styles.mainTitle)}>{tribute.firstName} {tribute.lastName}</h1>
                    <div className={css(styles.infoLine)}>
                        <ul className={css(styles.infoLineBlock)}>
                            <li onClick={() => {this.changeDescription(1)}} className={`${css(styles.infoLineItem)} ${this.state.isActive === 1 ? css(styles.itemActive) : ''}`}>Main</li>
                            <li onClick={() => {this.changeDescription(2)}} className={`${css(styles.infoLineItem)} ${this.state.isActive === 2 ? css(styles.itemActive) : ''}`}>Education</li>
                            <li onClick={() => {this.changeDescription(3)}} className={`${css(styles.infoLineItem)} ${this.state.isActive === 3 ? css(styles.itemActive) : ''}`}>Interests</li>
                            <li onClick={() => {this.changeDescription(4)}} className={`${css(styles.infoLineItem)} ${this.state.isActive === 4 ? css(styles.itemActive) : ''}`}>Military</li>
                        </ul>
                    </div>

                    <div className={css(styles.tributeInfo)}>
                        <ImageLoader
                            className={css(styles.image)}
                            src={tribute.avatarCropUri || tribute.avatarUri}
                            previewSrc={defaultAvatar}
                            containerClassName={css(styles.containerStyle)}
                        />
                        <div className={css(styles.descriptionBlock)}>
                            <div>
                                {this.formatDates(tribute.dateBorn, tribute.datePassed)}
                            </div>
                        </div>
                    </div>

                    {/*<div className={css(styles.summary)}>*/}
                        {/*{renderHTML(tribute.summary || '')}*/}
                    {/*</div>*/}


                    {(tribute.role && tribute.role !== 'Owner') &&
                    <div className={css(styles.connectContainer)}>
                        <Button className={css(styles.connectButton)} onClick={() => {this.showModal()}} bsStyle='primary' bsSize='sm'>{tribute.role.toLowerCase() === 'friend' ? 'Disconnect' : 'Connect'}</Button>
                    </div>
                    }

                </BlockTile>
                <div>
                    {this.props.albums.length ? <div><p className={css(styles.albumsTitle)}>ALBUMS</p>
                        <span className={css(styles.divider)}></span>
                        <AlbumSection height={this.props.albums.length === 1 ? '350px' : ''} tribute={tribute}/>
                        </div> : ''}

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    console.log(props);

    return {
        tribute: selectors.tributeByUri(state, props.params.tributeUri),
        connectedToTribute: selectors.connectedToTribute(state, props.tributeId),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const {tributeUri} = props.params;
    console.log(props.tributeId);
    return {
        getTribute: () => {
            return dispatch(actions.tributes.getTribute(tributeUri));
        },
        connectTribute: (message) => {
            return dispatch(actions.connected.connectRequest(props.tributeId, message));
        },
        disconnectTribute: () => {
            return dispatch(actions.connected.connectDisconnect(props.tributeId));
        },
        getAlbums: () => {
            return dispatch(actions.albums.getAlbumsByTributeId(tributeUri));
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TributePage));

