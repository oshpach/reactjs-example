import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import TextareaAutosize from 'react-textarea-autosize'
import actions from '../actions'

const styles = StyleSheet.create({
        container: {
            borderTop: '#F0F0F0 solid 1px',
            padding: '15px 0',
            ':after': {
                content: '',
                display: 'table',
                clear: 'both'
            }
        },

        button: {
            marginLeft: '10px',
            float: 'right',
            height: '47px'
        }
    });

const commentEnterStyle = {
    resize: 'none',
    width: '100%',
    padding: '10px 15px',
    overflow: 'hidden',
};

export class EnterMediaComments extends React.Component {
    constructor(props) {
        super(props);

        // Bind this to functions
        this.handleSaveComment = this.handleSaveComment.bind(this);
    }

    handleSaveComment(e) {
        console.log(this.textInput.value);
        if (this.textInput.value && this.textInput.value.trim() !== '') {
            this.props.saveMediaComment(this.textInput.value.trim());
            this.textInput.value = '';
        }
    }

    render() {
        const {media} = this.props;

        return (
            <div className={css(styles.container)}>
                <button className={css(styles.button)} onClick={this.handleSaveComment}>Comment</button>

                <div style={{overflow: 'hidden'}}>
                    <TextareaAutosize
                        minRows={1}
                        maxRows={6}
                        placeholder="Enter a comment"
                        defaultValue={media.comment}
                        style={commentEnterStyle}
                        ref={(textInput) => this.textInput = textInput}
                    />
                </div>

                <div style={{clear:'both'}}></div>
            </div>

        )
    }
}

const mapStateToProps = (state, props) => {
    return {};
};

const mapDispatchToProps = (dispatch, props) => {

    return {
        saveMediaComment: (comment) => dispatch(actions.media.saveMediaComment(props.media.mediaId, comment))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterMediaComments);