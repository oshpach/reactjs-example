import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Link} from 'react-router'
import moment from 'moment'
import {getAlbumsByTributeId} from '../../selectors'
import Common from '../../../Common'
const {components: {BlockTile, DisplayAvatar}} = Common;

const styles = StyleSheet.create({
    textContainer: {
        display: 'inline-block',
        marginLeft: '20px',
        fontFamily: 'Railway',
        fontSize: 22,
        color: '#3498DB',
        verticalAlign: 'middle',
        '@media screen and (max-width: 432px)':{
            textAlign: 'center',
            maxWidth: '90%',
            display: 'block',
            margin: '0 auto'
        }
    },

    dates: {
        fontSize: 18,
        color: '#333333'
    },

    link: {
        color: 'black',
        textDecoration: 'none',
        ':hover': {
            color: 'black',
            textDecoration: 'none'
        }
    },

    changeTile: {
        maxWidth: '550px !important',
        paddingLeft: '10px !important',
        paddingRight: '10px !important',
    },

    displayAvatarStyle: {
        display: 'inline-block',
        '@media screen and (max-width: 432px)':{
            display: 'block',
            textAlign: 'center'
        }
    }
});

export class TributeTile extends React.Component {
    constructor(props) {
        super(props);

        // Bind this to functions
        this.handleDescriptionBlur = this.handleDescriptionBlur.bind(this);
        this.handleCommentBlur = this.handleCommentBlur.bind(this);
        this.handleAlbumSelect = this.handleAlbumSelect.bind(this);
        this.handleAllAlbumsSelect = this.handleAllAlbumsSelect.bind(this);
    }

    componentDidMount() {
        // if (this.props.albums.length === 0) {
        //     this.props.getAlbums();
        // }
    }

    handleDescriptionBlur(e) {
        if (e.target.value !== e.target.defaultValue) {
            this.props.saveFileAttributes({description: e.target.value});
        }
    }

    handleCommentBlur(e) {
        if (e.target.value !== e.target.defaultValue) {
            this.props.saveMediaComment(e.target.value);
            e.target.value = '';
        }
    }

    formatDates(born, passed) {
        const dateBorn = moment(born, moment.ISO_8601);
        const datePassed = moment(passed, moment.ISO_8601);

        if (dateBorn.isValid() && datePassed.isValid()) {
            return <div className={css(styles.datesText)}>{dateBorn.format('LL')} - {datePassed.format('LL')}</div>
        }

        if (dateBorn.isValid()) {
            return <div className={css(styles.datesText)}>Born:&nbsp;{dateBorn.format('LL')}&nbsp;
                (age {moment().diff(dateBorn, 'years')})</div>
        }

        if (datePassed.isValid()) {
            return <div>Died: {dateBorn.format('LL')}</div>
        }
    }

    handleAlbumSelect(eventKey, tributeId) {
        this.props.router.push({
            pathname: '/tribute/album',
            state: {
                albumId: eventKey,
                tributeId: tributeId,
                referrer: this.props.location
            }
        });
    };

    handleAllAlbumsSelect(event, tributeId){
        this.props.router.push({
            pathname: '/tribute/albums',
            state: {
                tributeId: tributeId
            }
        });
    }

    render() {
        const {tribute} = this.props;

        return (
            <BlockTile className={css(styles.changeTile)}>
                <Link to={{pathname: `/tribute/${tribute.tributeId}`}} className={css(styles.link)}>
                    <div className={css(styles.tileContent)}>
                        <div className={css(styles.displayAvatarStyle)}>
                            <DisplayAvatar user={tribute} size={100}/>
                        </div>
                        <div className={css(styles.textContainer)}>
                            <div><span style={{fontWeight: 'bold'}}>{tribute.firstName}</span> {tribute.lastName}</div>
                            <div className={css(styles.dates)}>{this.formatDates(tribute.dateBorn, tribute.datePassed)}</div>
                        </div>
                    </div>
                </Link>
            </BlockTile>
        );
    }
}

const mapStateToProps = (state, props) => {
    const {tributeId} = props.tribute;

    return {
        tributeId,
        albums: getAlbumsByTributeId(state, tributeId)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    // const {tributeId} = props.tribute;

    // return {
    //     getAlbums: () => {
    //         return dispatch(actions.albums.getAlbumsByTributeId(tributeId));
    //     }
    // }

    return {};
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TributeTile));

