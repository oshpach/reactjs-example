import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';
import {Grid, Row, Col} from 'react-bootstrap';
import {headerImage} from '../../../Common/images'

const styles = StyleSheet.create({
    priceWrapper: {
        backgroundImage: `url(${headerImage})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        marginTop: 50,
        minHeight: 600
    },
    blockWrapper: {
        backgroundColor: 'white',
        minHeight: 280,
        borderRadius: 5,
        boxShadow: '0 0 10px rgba(0, 0, 0, .6)',
        maxWidth: 270,
        margin: '0 auto',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        "@media screen and (max-width: 768px)": {
            margin: '10px auto'
        }
    },
    centerBlock: {
        backgroundColor: '#3498DB',
        minHeight: 280,
        transform: 'scale(1.2)',
        borderRadius: 5,
        boxShadow: '0 0 10px rgba(0, 0, 0, .6)',
        position: 'relative',
        zIndex: 200,
        maxWidth: 270,
        margin: '0 auto',
        display: 'flex',
        justifyContent: 'space-around',
        flexDirection: 'column',
        "@media screen and (max-width: 768px)": {
            transform: 'scale(1)'
        }
    },
    title: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 50,
        marginBottom: 30,
        "@media screen and (max-width: 450px)": {
            fontSize: 30
        }
    },
    subTitle: {
        textAlign: 'center',
        color: 'white',
        "@media screen and (max-width: 450px)": {
            fontSize: 16
        }
    },
    text: {
        alignSelf: 'center',
    },
    textBold: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 20
    },
    textCenter: {
        alignSelf: 'center',
        color: 'white'
    },
    textCenterBold: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: 'white'
    },
    centerWrapper: {
        width: '80%',
        margin: '0 auto',
        "@media screen and (max-width: 1000px)": {
            width: '100%'
        }
    },
    centerBlockWrapper: {
        "@media screen and (max-width: 768px)": {
            padding: 0
        }
    }
});



const Pricing = (props) => {

    return (
        <Grid className={css(styles.priceWrapper)} fluid={true}>
            <Row style={{marginTop: 20, marginBottom: 40}}>
                <Col lg={12} md={12} sm={12} xs={12}>
                    <h3 className={css(styles.subTitle)}>Upgrade your membership</h3>
                    <h1 className={css(styles.title)}>Get unlimited space</h1>
                </Col>
            </Row>
            <Row className={css(styles.centerWrapper)}>
                <Col lg={4} md={4} sm={4} xs={12} style={{padding: 0}}>
                   <div className={css(styles.blockWrapper)}>
                       <p className={css(styles.textBold)}>Free for life</p>
                       <p className={css(styles.textBold)}>100<sub>mb</sub></p>
                       <p className={css(styles.text)}><sup>$</sup>0/free</p>
                       <p className={css(styles.text)}>Limited to 100 Mb Space</p>
                   </div>
                </Col>
                <Col lg={4} md={4} sm={4} xs={12} className={css(styles.centerBlockWrapper)}>
                    <div className={css(styles.centerBlock)}>
                        <p className={css(styles.textCenterBold)} style={{fontSize: 20}}>Unlimited space</p>
                        <p className={css(styles.textCenterBold)} style={{fontSize: 30}}>&#8734;</p>
                        <p className={css(styles.textCenter)}><sup>$</sup>99/lifetime</p>
                        <p className={css(styles.textCenter)}>Unlimited space</p>
                    </div>
                </Col>
                <Col lg={4} md={4} sm={4} xs={12} style={{padding: 0}}>
                    <div className={css(styles.blockWrapper)}>
                        <p className={css(styles.textBold)}>Unlimited space</p>
                        <p className={css(styles.textBold)} style={{fontSize: 30}}>&#8734;</p>
                        <p className={css(styles.text)}><sup>$</sup>49/lifetime</p>
                        <p className={css(styles.text)}>Unlimited space</p>
                    </div>
                </Col>
            </Row>
            <Row style={{marginTop: 40}}>
                <Col lg={12} md={12} sm={12} xs={12}>
                    <p className={css(styles.subTitle)}>30-Day money back guarantee for each plan - No questions asked</p>
                </Col>
            </Row>
        </Grid>
    )

};


export default Pricing;