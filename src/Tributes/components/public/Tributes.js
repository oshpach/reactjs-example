import React from 'react'
import {Field, reduxForm, submit} from 'redux-form'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {Grid, Row, Col} from 'react-bootstrap'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'
import * as selectors from '../../selectors'
import {getMediaUrl} from '../../../utils'
import defaultAvatar from '../../../Common/createTribute/images/default-avatar.png'
import TributesTile from './TributesTile'
import './TributesSpinner.css'

const coolFont = {
    fontFamily: "'Raleway'",
    fontStyle: "normal",
    fontWeight: "normal",
    src: "url('../../../Common/fonts/Raleway/Raleway-Light.ttf') format('ttf')"
};

const styles = StyleSheet.create({
    displayNone: {
        display: 'none'
    },

    filterOpen: {
        height: '220px'
    },

    showFilterListOpen: {
        opacity: 1,
        display: 'block',
        margin: '0 auto',
        transition: 'all ease 0.4s'
    },

    filterAnimationOpen: {
        marginTop: '20px'
    },

    filter: {
        "@media screen and (max-width: 768px)": {
            height: '50px',
            cursor: 'pointer',
            overflow: 'hidden',
            transition: 'all ease 0.4s',
            position: 'relative',
            zIndex: 900
        }
    },

    filterWrapper: {
        "@media screen and (max-width: 768px)": {
            width: 'auto',
            margin: '20px auto'
        }
    },
    filterName: {
        "@media screen and (max-width: 768px)": {
            display: 'inline-block',
            textTransform: 'uppercase',
            color: 'grey',
            paddingRight: '10px',
            fontSize: '20px',
            cursor: 'pointer'
        },
        "@media screen and (min-width: 769px)": {
            display: 'none'
        }

    },
    filterMarginAnimation: {
        "@media screen and (max-width: 768px)": {
            marginTop: 0,
            marginBottom: 0,
            transition: 'all ease 0.4s'
        }
    },
    caret: {
        "@media screen and (max-width: 768px)": {
            display: 'inline'
        },
        "@media screen and (min-width: 769px)": {
            display: 'none'
        }
    },
    showFilterList: {
        "@media screen and (max-width: 768px)": {
            opacity: 0,
            display: 'none'
        }
    },


    tributePhoto: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        cursor: 'pointer'
    },
    marginBottom: {

        marginBottom: '50px',
        paddingBottom: '50px',
        backgroundColor: 'rgba(0, 0, 0, 0.1)'

    },

    tributesSearch: {
        margin: '50px auto',
        maxWidth: '550px',
        width: '100%',
        position:'relative',
        left: '-25px'
    },

    tributeSearchIcon: {
        width: '50px',
        height: '50px',
        display: 'inline-block',
        position: 'absolute',
        borderBottom: '1px solid gray',
        right: '-50px',
        top: '0',
        cursor: 'pointer',
        textAlign: 'center',
        paddingTop: '10px',
        color: '#3498DB',
        fontSize: '25px'
    },

    tributesSearchInput: {
        width: '100%',
        height: '5rem',
        fontSize: '2.5rem',
        textTransform: 'uppercase',
        backgroundColor: 'transparent',
        borderLeft: 'none',
        borderRight: 'none',
        borderTop: 'none',
        borderBottom: '1px solid grey',
        padding: '0',
        display: 'inline-block',
        borderRadius: '0',
        color: '#3498DB',
        ":focus": {
            borderLeft: 'none',
            borderRight: 'none',
            borderTop: 'none',
            borderBottom: '1px solid grey',
            outline:'none'
        }
    },
    tributeTitle: {

        textAlign: 'center'

    },

    tributesUnderline: {
        color: '#3498DB',
        width: '40px',
        backgroundColor:'#3498DB',
        height:'1px',
        marginTop: '10px'
    },

    paddingLeft: {
        paddingLeft: '50px !important'
    },

    paddingRight: {
        paddingRight: '50px !important'
    },
    tributesMargin: {
        marginBottom: "220px",
        marginTop: '50px',
        '@media screen and (max-width: 768px)':{
            marginBottom: '200px',
            marginTop: '50px'
        },
        '@media screen and (max-width: 450px)':{
            marginBottom: '50px',
            marginTop: '50px'
        }
    },
    selectCountry: {
        backgroundColor: 'white',
        border: 'none',
        color: '#3498DB',
        padding: '5px 10px',
        fontSize: '15px',
        maxWidth: '250px',
        width: '100%',
        fontFamily: coolFont,
        letterSpacing: '1px'
    },
    selectsWrapper: {
        width: '100%',
        maxWidth: '700px',
        margin: '0 auto',
        "@media screen and (max-width: 768px)": {
            width: '80%'
        }
    },
    imageFit: {
        minWidth: '100%',
        display: 'block'
    },
    containerFit: {
        display: 'block !important'
    }
});



export class Tributes extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            searchResult: '',
            showFilter: false
        };
        this.linkToTribute =  this.linkToTribute.bind(this);
    }

    componentWillMount() {
        this.props.getTributes();
    }

    showFilter(){
        this.setState({
            showFilter: !this.state.showFilter
        });
    }

    linkToTribute(tributeUri, tributeId){
        this.props.router.push({
            pathname: `/tribute/${tributeUri}`,
            state: {tributeId}
        })
    }


    submit = (model) => {
        let splited = model.searchTribute.split(' ');
        let filteredTributes = this.props.tributes.filter((current) => splited[0] === current.firstName && splited[1] === current.lastName);

        this.setState({
            searchResult: filteredTributes
        });
    };



    render() {
        const {handleSubmit} = this.props;
        console.log(this.props);
        return (
            <div>
                <Grid fluid={true} className={css(styles.tributesMargin)}>
                    <Row className={css(styles.marginBottom)}>
                        <form onSubmit={handleSubmit(this.submit)} style={{textAlign: 'center'}}>
                            <div className={css(styles.selectsWrapper)}>
                                <div className={css(styles.tributesSearch)}>
                                    <Field type="text"
                                           className={css(styles.tributesSearchInput)}
                                           component="input"
                                           name="searchTribute"
                                           placeholder="SEARCH"
                                    />
                                    <span className={css(styles.tributeSearchIcon)}><i className="fa fa fa-search"/></span>
                                </div>
                                <div className={`${css(styles.filter)} ${this.state.showFilter ? css(styles.filterOpen): ''}`}>
                                    <div className={css(styles.filterWrapper)} onClick={() => {this.showFilter()}}>
                                        <span className={css(styles.filterName)}>Filter</span>
                                        <span className={`${this.state.showFilter ? css(styles.displayNone) : ''} ${css(styles.caret)}`}><i className="fa fa-caret-down"/></span>
                                        <span className={`${this.state.showFilter ? '' : css(styles.displayNone)} ${css(styles.caret)}`}><i className="fa fa-caret-up"/></span>
                                    </div>
                                    <Col md={4} sm={4} xs={12} className={`${this.state.showFilter ? css(styles.filterAnimationOpen) : ''} ${css(styles.filterMarginAnimation)}`}>
                                        <Field type="text"
                                               component="select"
                                               name="selectCountry"
                                               className={`${this.state.showFilter ? css(styles.showFilterListOpen) : ''} ${css(styles.selectCountry)} ${css(styles.showFilterList)} `}
                                        >
                                            <option value="LOCATION">LOCATION</option>
                                            <option value="UKRAINE">UKRAINE</option>
                                            <option value="USA">USA</option>
                                            <option value="SPAIN">SPAIN</option>
                                            <option value="FRANCE">FRANCE</option>
                                            <option value="GERMANY">GERMANY</option>
                                            <option value="FINLAND">FINLAND</option>
                                        </Field>
                                    </Col>
                                    <Col md={4} sm={4} xs={12} className={`${this.state.showFilter ? css(styles.filterAnimationOpen) : ''} ${css(styles.filterMarginAnimation)}`}>
                                        <Field type="text"
                                               component="select"
                                               name="selectCountry"
                                               className={css(styles.selectCountry) + ' showFilterList'}
                                        >
                                            <option value="YEAR">YEAR</option>
                                            <option value="1900">1900</option>
                                            <option value="1940">1940</option>
                                            <option value="1998">1998</option>
                                            <option value="1999">1999</option>
                                            <option value="2000">2000</option>
                                            <option value="2001">2001</option>
                                        </Field>
                                    </Col>
                                    <Col md={4} sm={4} xs={12} className={`${this.state.showFilter ? css(styles.filterAnimationOpen) : ''} ${css(styles.filterMarginAnimation)}`}>
                                        <Field type="text"
                                               component="select"
                                               name="selectCountry"
                                               className={css(styles.selectCountry) + ' showFilterList'}
                                        >
                                            <option value="UNIVERSITY">UNIVERSITY</option>
                                            <option value="CAMBRIDGE">CAMBRIDGE</option>
                                            <option value="OXFORD">OXFORD</option>
                                            <option value="SPAIN">SPAIN</option>
                                            <option value="FRANCE">FRANCE</option>
                                            <option value="GERMANY">GERMANY</option>
                                            <option value="FINLAND">FINLAND</option>
                                        </Field>
                                    </Col>
                                </div>
                            </div>
                        </form>
                    </Row>
                    {this.state.searchResult ? this.state.searchResult.map((tributeSearch, index)=>(

                        <Col md={5} sm={12} xs={12} mdOffset={index%2 === 0 ? 1 : 0} key={index}>
                            <TributesTile tribute={tributeSearch}/>
                        </Col>
                        )) : this.props.tributes.map((tribute, index) => (
                        tribute.firstName ?
                            <div key={index}>
                                <Col md={3} sm={4} xs={6} style={{marginBottom: '30px', textAlign: 'center'}} onClick={() => {this.linkToTribute(tribute.tributeUri, tribute.tributeId)}}>
                                    <div style={{
                                        position: 'relative',
                                        width: '100%',
                                        paddingBottom: '100%',
                                        overflow: 'hidden'
                                    }}>
                                        {tribute.avatarUri ? <div className={css(styles.tributePhoto)} style={{backgroundImage: `url(${getMediaUrl(tribute.avatarUri)}`}}></div> :
                                            <div className={css(styles.tributePhoto)} style={{backgroundImage: `url(${defaultAvatar})`}}></div>}
                                    </div>
                                    <span className={css(styles.tributeTitle)}>{tribute.firstName + ' ' + tribute.lastName}</span>
                                </Col>

                            </div>
                            : <p key={index}></p>
                        )
                    )}
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        tributes: selectors.tributes(state),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getTributes: () => {
            dispatch(actions.tributes.getTributes())
        }
    };
};

Tributes = reduxForm({
    form: 'SearchTributesForm',  // a unique identifier for this form
    onSubmit: submit
})(Tributes);


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Tributes));

