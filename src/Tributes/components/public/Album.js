import React from 'react'
import {connect} from 'react-redux'
import { withRouter } from 'react-router'
import MediaPicker from '../../components/albums/MediaPicker'
import moment from 'moment'
import renderHTML from 'react-render-html'
import DisplayAvatar from '../../../Common/components/DisplayAvatar'
import { userAuthenticated} from '../../../utils'
import { DropdownButton, MenuItem, Row, Col, Grid } from 'react-bootstrap'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../../actions'
import CreateAlbumPopUp from '../albums/CreateAlbumPopUp'
import {getAlbumById, tributeByUri,tributeById, getAlbumByUri, getAlbumMediaByAlbumUri} from '../../selectors'
import TributeGallery from '../TributeGallery'
import {albumLogo} from '../../../Common/images'

const gutterSize = 20;

const styles = StyleSheet.create({

    gutter: {
        display: 'inline-block',
        width: gutterSize,
        height: 0,

        '@media (max-width: 1078px)': {
            display: 'block',
            width: 0,
            height: gutterSize,
        }
    },

    row: {
        marginBottom: gutterSize,
        // marginTop: '100px'

    },
    editButton: {
        position: 'absolute',
        top: '115px',
        right: '20px !important',
        zIndex: '899'
    },
    albumWrapper: {
        marginTop: '100px',
    },
    tributeDescription: {

        marginBottom: 50
    },
    tributeAvatar: {
        margin: '10px auto',
    },
    tributeName: {
        fontSize: '20px',
        color: '#3498DB',
        textTransform: 'uppercase',
        fontWeight: 'bold',
    },
    tributeDesc: {
        fontSize: '14px',
        fontStyle: 'italic',
        color: '#3498DB',
        paddingLeft: '10px'
    },
    DropZone: {
        marginTop: '40px',
        marginBottom: '40px'
    },
    tributeSummary: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        "@media screen and (max-width: 768px)": {
            textAlign: 'center',
            marginLeft: 20
        }
    },
    albumDesc: {
        fontSize: '14px',
        color: '#626262'
    },
    albumDescription: {
        "@media screen and (max-width: 768px)": {
            textAlign: 'center',
            marginLeft: 20
        }

    },
    albumName: {
        fontSize: '18px',
        fontWeight: 'bold',
        marginTop: 35,
        color: '#626262',
        "@media screen and (max-width: 500px)": {
            fontSize: '25px'
        }
    },
    avatarWrapper: {
        backgroundImage: `url(${albumLogo})`,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        width: 200,
        height: 200,
        display: 'flex',
        margin: '0 auto',
        justifyContent: 'center'
    },
    userAvatar: {
        borderRadius: 'unset !important',
        border: '5px solid #3498DB',
        boxShadow: 'none !important'
    },
    displayAvatarWrapper: {
        alignSelf: 'center'
    },
    photosTitle: {
        color: '#3498DB',
        fontSize: 20,
        fontWeight: 600,
        marginLeft: 90,
        marginBottom: 10
    },
    line: {
        display: 'inline-block',
        width: 13,
        height: 1,
        background: '#3498DB',
        marginBottom: 6,
        marginRight: 16
    }
});


export class Album extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showMediaPicker: false,
            showEditAlbumPopUp: false
        };

        // Bind this to functions
        this.handleEditAlbumClick = this.handleEditAlbumClick.bind(this);
        this.handleMediaSelected = this.handleMediaSelected.bind(this);
        this.handleAddPhotosClick = this.handleAddPhotosClick.bind(this);
        this.formatDates = this.formatDates.bind(this);
        this.onHide = this.onHide.bind(this);

        this.handleSaveMedia = this.handleSaveMedia.bind(this);
        this.uploadImages = this.uploadImages.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.deleteAlbum = this.deleteAlbum.bind(this);
        this.hideModal = this.hideModal.bind(this);
        this.showModal = this.showModal.bind(this);
    }


    componentWillMount(){
        window.scrollTo(0, 0);
        // this.props.getAlbums();
        if (!this.props.album) {
            this.props.getAlbum();
        }

        this.props.getAlbumMedia();

        if (!this.props.tribute) {
            this.props.getTributes();  // get tribute from db
        }
    }

    onHide(){
        this.setState({
            showMediaPicker: false
        })
    }

    handleMediaSelected = (media) => {
        this.onHide();

        let mediaId = media.mediaId;
        //Save media
        this.props.saveAlbumMedia(mediaId);
    }

    handleAddPhotosClick(){
        this.setState({
            showMediaPicker: true
        });
    }

    formatDates(born, passed) {
        const dateBorn = moment(born, moment.ISO_8601);
        const datePassed = moment(passed, moment.ISO_8601);

        if (dateBorn.isValid() && datePassed.isValid()) {
            return <div className={css(styles.datesText)}>{dateBorn.format('LL')} - {datePassed.format('LL')}</div>
        }

        if (dateBorn.isValid()) {
            return <div className={css(styles.datesText)}>Born:&nbsp;{dateBorn.format('LL')}&nbsp;
                (age {moment().diff(dateBorn, 'years')})</div>
        }

        if (datePassed.isValid()) {
            return <div>Died: {dateBorn.format('LL')}</div>
        }
    }

    uploadImages(){
        this.setState({
            show: !this.state.show
        });
    }

    hideModal(){
        this.setState({
            showEditAlbumPopUp: false
        });
    }

    showModal(){
        this.setState({
            showEditAlbumPopUp: true
        });
    }

    handleEditAlbumClick(){
        this.props.router.push({
            pathname: '/editalbum',
            state: {
                albumId: this.props.albumId,
                tributeId: this.props.tributeId
            }
        });
    }
    deleteAlbum(){
        this.props.deleteAlbum()
            .then(() => {this.props.router.push('/mytributes')})
    }

    handleDelete(albumMediaId) {
        this.props.removeMedia(albumMediaId);
    }

    handleSaveMedia(mediaId) {
        this.props.saveAlbumMedia(mediaId);
    }
    render() {
        const {albumMedia = [], tribute = {}, album = [], tributeId = '', albumId=''} = this.props;
        return (
            <div className={css(styles.albumWrapper)}>
                <CreateAlbumPopUp album={album} tribute={tribute} show={this.state.showEditAlbumPopUp} onHide={this.hideModal}/>
                {userAuthenticated() && tribute.role === 'Owner' ? <div className={css(styles.editButton)}>
                    <DropdownButton style={{background: '#3498DB', border: '1px solid white', color: 'white'}} title={<span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>} id="EditTribute">
                        <MenuItem onClick={this.showModal} eventKey="1">Modify an album</MenuItem>
                        <MenuItem onClick={this.handleAddPhotosClick} eventKey="2">Add photos</MenuItem>
                        <MenuItem divider/>
                        <MenuItem onClick={() => {this.deleteAlbum()}} eventKey="3">Delete an album</MenuItem>
                    </DropdownButton>
                </div> : ''}
                <h2 style={{textAlign: 'center', marginBottom: 40, fontWeight: 600, fontSize: 20}}><span className={css(styles.line)}></span>MY ALBUM<span style={{marginLeft: 16}} className={css(styles.line)}></span></h2>
                <div className={css(styles.tributeDescription)}>
                    <Grid fluid={true}>
                        <Row>
                            <Col lg={2} mdOffset={1} md={3} smOffset={1} sm={3} xs={12}>
                                <div className={css(styles.tributeAvatar)}>
                                    <div className={css(styles.avatarWrapper)}>
                                        <DisplayAvatar displayWrapperClass={css(styles.displayAvatarWrapper)} user={tribute} size={130} className={css(styles.userAvatar)}/>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={8} md={7} sm={7} xs={12}>
                                <div className={css(styles.tributeSummary)}>
                                    <div className={css(styles.tributeName)}>{`${tribute.firstName || ''} ${tribute.lastName || ''}`}</div>
                                    <div className={css(styles.tributeDesc)}>{this.formatDates(tribute.dateBorn, tribute.datePassed)}</div>
                                </div>
                                <div className={css(styles.albumDescription)}>
                                    <div className={css(styles.albumName)}>
                                        {album.name}
                                    </div>
                                    <div className={css(styles.albumDesc)}>
                                        {album.description ? renderHTML(album.description) : ''}
                                        Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem loremLorem lorem lorem loremLorem lorem lorem lorem Lorem lorem lorem lorem
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <p className={css(styles.photosTitle)}><span className={css(styles.line)}></span>MY PHOTOS</p>
                {this.state.showMediaPicker && <MediaPicker onHide={this.onHide} tributeId={tributeId} albumId={albumId} onMediaPickerSelected={this.handleMediaSelected}/>}
                <TributeGallery access="albumPhoto" media={albumMedia} tribute={tribute}/>
            </div>
        )
    }
}

const mapStateToProps = (state, props) => {
    console.log(state);
    const tributeId = (props.location.state && props.location.state.tributeId) || null;
    const albumId = (props.location.state && props.location.state.albumId) || null;
    const {albumUri, tributeUri} = props.params;
    return {
        albumId,
        tributeId,
        album: getAlbumById(state, albumId) || getAlbumByUri(state, albumUri),
        albumMedia: getAlbumMediaByAlbumUri(state, albumUri),
        tribute:  tributeById(state, tributeId) || tributeByUri(state, tributeUri)
    };
};

const mapDispatchToProps = (dispatch, props) => {
    const tributeId = (props.location.state && props.location.state.tributeId) || null;
    const albumId = (props.location.state && props.location.state.albumId) || null;
    const {albumUri, tributeUri} = props.params;
    return {
        getAlbum: () => {
            return dispatch(actions.albums.getAlbum(albumId || albumUri));
        },
        getAlbumMedia: () => {
            return dispatch(actions.albums.getAlbumMedia(albumUri));
        },
        getTributes: () => dispatch(actions.tributes.getTributes()),
        saveFile: (file) => {
            return dispatch(actions.media.saveImage(tributeId, file))
        },
        getFiles: () => {
            dispatch(actions.media.getTributeMediaById(tributeId))
        },
        saveAlbumMedia: (mediaId) => {
            return dispatch(actions.albums.saveAlbumMedia({mediaId, tributeId, albumId}));
        },
        removeMedia: (albumMediaId) => {
            dispatch(actions.albums.removeMedia(albumMediaId))
        },
        deleteAlbum: () => dispatch(actions.albums.deleteAlbum(albumId)),
        getTribute: () => dispatch(actions.tributes.getTribute(tributeUri)),
        getAlbums: () => {
            return dispatch(actions.albums.getAlbumsByTributeId(tributeUri));
        }


    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Album));

