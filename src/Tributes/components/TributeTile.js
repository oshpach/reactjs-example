import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'

import DisplayMediaComments from './DisplayMediaComments'
import EnterMediaComment from './EnterMediaComments'
import {defaultPersona} from '../../Common/images'

import Common from '../../Common'
import Actions from '../actions'

const {components: {ImageLoader, BlockTile}} = Common;
const {media: {saveMediaAttributes, saveMediaComment}} = Actions;

const styles = StyleSheet.create({
    tileContent: {
        width: '600px',
        margin: 'auto',

        '@media (max-width: 600px)': {
            width: '100%'
        }
    },

    title: {
        width: '100%'
    },

    image: {
        maxHeight: '950px',

        width: '600px',
        display: 'block',
        border: '#E0E0E0 solid 2px',
        margin: '25px 0',

        '@media (max-width: 600px)': {
            minWidth: '100%',
            maxWidth: '100%'
        }
    },

    textBlock: {
        margin: '25px 0 auto',
        border: '#F0F0F0 solid 1px',
        padding: '10px',
        textAlign: 'left',
        overflowY: 'auto',
        maxHeight: '160px'
    },

    link: {
        color: 'black',
        textDecoration: 'none',
        ':hover': {
            color: 'black',
            textDecoration: 'none'
        }
    },

    commentBlock: {
        textAlign: 'left'
    },

    userInfo: {
        verticalAlign: 'top'
    },

    userAvatar: {
        height: '32px',
        width: '32px',
        display: 'inline-block',
        verticalAlign: 'top'
    },

    userText: {
        display: 'inline-block',
        fontSize: 'small',
        padding: '0 10px'
    },

    commentText: {
        padding: '10px'
    }

});

export class DisplayTributeTile extends React.Component {
    constructor(props) {
        super(props);

        // Bind this to functions
        this.handleDescriptionBlur = this.handleDescriptionBlur.bind(this);
        this.handleCommentBlur = this.handleCommentBlur.bind(this);
    }

    handleDescriptionBlur(e) {
        if (e.target.value !== e.target.defaultValue) {
            this.props.saveFileAttributes({description: e.target.value});
        }
    }

    handleCommentBlur(e) {
        if (e.target.value !== e.target.defaultValue) {
            this.props.saveMediaComment(e.target.value);
            e.target.value = '';
        }
    }

    render() {
        const {media} = this.props;

        return (
            <BlockTile>
                <div className={css(styles.tileContent)}>
                    { media.description && <div className={css(styles.textBlock)}>{media.description}</div> }

                    <ImageLoader className={css(styles.image)} src={media.uri} previewSrc={media.preview || defaultPersona}/>

                    <DisplayMediaComments media={media}/>

                    <EnterMediaComment media={media} />
                </div>
            </BlockTile>
        );
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        saveFileAttributes: (attributes) => dispatch(saveMediaAttributes(props.media.mediaId, attributes)),
        saveMediaComment: (comment) => dispatch(saveMediaComment(props.media.mediaId, comment))
    };
};

export default connect(null, mapDispatchToProps)(DisplayTributeTile);

