import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {DropdownButton, MenuItem, Tabs, Tab} from 'react-bootstrap'
import Dropzone from 'react-dropzone'
import classNames from 'classnames'
import {userAuthenticated} from '../../utils'
import ConfirmDeleteModal from './ConfirmDeleteModal'
import actions from '../actions'
import * as selectors from '../selectors'
import TributeGallery from './TributeGallery'
import TributeInformationTile from './TributeInformationTile'
import CreateAlbumPopUp from './albums/CreateAlbumPopUp'


const dzStyle = {
    width: '100%',
    maxWidth: 600,
    padding: 15,
    border: '1px #E0E0E0 solid',
    boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',
    borderRadius: 5,
    margin: '30px auto 0',
    cursor: 'pointer',
    backgroundColor: 'white'
};

const dzActiveStyle = {
    borderStyle: 'dashed'
};

const styles = StyleSheet.create({
    TributeInfo: {
        marginTop: '50px !important',
        maxWidth: '100% !important',
        paddingTop: '30px !important',
        background: 'white !important',
        "@media screen and (max-width: 768px)": {
            marginTop: '50px !important'
        }
    },
    marginTop: {
        bottom: '-200px'
    },
    tributePhoto: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        cursor: 'pointer'
    },
    editButton: {
        position: 'absolute',
        right: '30px',
        top: '130px',
        zIndex: '899',
        '@media screen and (max-width: 450px)': {
            top: '105px',
            right: '10px'
        }
    },
    faCircle: {
        color: '#E0E0E0',
        textShadow: '-1px 0 #A9A9A9, 0 1px #A9A9A9, 1px 0 #A9A9A9, 0 -1px #A9A9A9'  // adds a border around the circle
    },

    faPicture: {
        color: 'white'
    },
    photosTitle: {
        textAlign: 'center',
        color: '#3498DB',
        marginTop: '40px',
        marginBottom: '30px',
        fontWeight: 'bold',
        fontSize: '30px'
    }

});


export class DisplayTribute extends React.Component {


    constructor(props){
        super(props);
        this.state = {
            showConfirmModal: false,
            shouldDelete: false,
            showCreateAlbumPopUp: false
        };
        this.handleDrop = this.handleDrop.bind(this);
        this.handleEditTributeClick = this.handleEditTributeClick.bind(this);
        this.handleCreateAlbumClick = this.handleCreateAlbumClick.bind(this);
        this.handleDeleteTributeClick = this.handleDeleteTributeClick.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }


    componentWillMount() {

        window.scrollTo(0, 0);
        // need to do this in case the page is refreshed.

        if (!this.props.tribute) {
            this.props.getTribute(); // get tribute from db
        }
        if(this.props.albums.length === 0){
            this.props.getAlbums();
        }

        if (this.props.media.length === 0) {
            this.props.getTributeMedia(0);  // get content from db
        }
    }

    handleDrop(acceptedFiles, e) {

        acceptedFiles.forEach((file, index) => {
            this.props.saveFile(file)
                .then(() => {
                    if (index + 1 === acceptedFiles.length) {
                        this.props.getFiles();
                    }
                });
        });
    }


    handleEditTributeClick() {
        this.props.router.push({
            pathname: '/edittribute',
            state: {
                tributeId: this.props.tributeId,
                referrer: this.props.location
            }
        })
    }

    handleCreateAlbumClick(){
        this.setState({
            showCreateAlbumPopUp: true
        });
    }

    handleDeleteTributeClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.deleteTribute()
            .then(() => {
                this.props.router.push({
                    pathname: '/mytributes'
                })
            });
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    hideModal(){
        this.setState({
            showCreateAlbumPopUp: false
        });
    }





    render() {
        const {media = [], tribute = {}, albums=[], album={}} = this.props;
        console.log(tribute);
        return (
            <div id="tributePage_">
                 <CreateAlbumPopUp album={Object.keys(album).length ? album : {}} tribute={tribute} show={this.state.showCreateAlbumPopUp} onHide={this.hideModal}/>
                {userAuthenticated() && tribute.role === 'Owner' ? <div className={css(styles.editButton)}>
                    <DropdownButton style={{background: '#3498DB', border: '1px solid white', color: 'white'}} title={<span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>} id="EditTribute">
                        <MenuItem onClick={this.handleEditTributeClick} eventKey="1">Modify a Tribute</MenuItem>
                        <MenuItem onClick={() => {this.handleCreateAlbumClick(tribute.tributeId)}} eventKey="2">Create an album</MenuItem>
                        <MenuItem  divider/>
                        <MenuItem onClick={this.handleDeleteTributeClick} eventKey="3">Delete</MenuItem>
                    </DropdownButton>
                </div> : ''}
                <TributeInformationTile albums={albums} fontSize="24px" className={css(styles.TributeInfo)} tributeId={tribute.tributeId}/>
                { tribute.role === 'owner' ? <Dropzone
                    onDrop={this.handleDrop}
                    style={dzStyle}
                    activeStyle={dzActiveStyle}
                >
                    <div style={{textAlign: 'center'}}>
                        <span className="fa-stack fa-3x">
                            <i className={classNames(css(styles.faCircle), "fa fa-circle fa-stack-2x")}/>
                            <i className={classNames(css(styles.faPicture), "fa fa-picture-o fa-stack-1x fa-inverse")}/>
                        </span>

                        <div>Drop Images here</div>
                        <div>or click to add</div>
                    </div>
                </Dropzone> : ''}
                {this.props.media.length ? <p className={css(styles.photosTitle)}>PHOTOS/VIDEOS</p> : ''}
                <TributeGallery access="tributePhoto" media={media} tribute={tribute}/>
                <ConfirmDeleteModal title="Delete Tribute" body={<div><p>Confirm delete tribute?</p><p style={{fontWeight: 'bold', color: 'red'}}>This action can not be undone.</p></div>}
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled} show={this.state.showConfirmModal}/>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const tributeId = (props.location.state && props.location.state.tributeId) || null;
    return {
        tributeId,
        tribute: selectors.tributeByUri(state, props.params.tributeUri),   // need to pass Slug instead of id
        media:  selectors.tributeMediaByUri(state, props.params.tributeUri),
        albums: selectors.getAlbumsByTributeId(state, props.params.tributeUri)
    };
};

const mapDispatchToProps = (dispatch, props) => {

    const tributeId = (props.location.state && props.location.state.tributeId) || null;

    return {
        getTributes: () => dispatch(actions.tributes.getTributes()),
        getTribute: () => dispatch(actions.tributes.getTribute(props.params.tributeUri)),
        getTributeMedia: (pageNumber) => dispatch(actions.media.getTributeMediaById(props.params.tributeUri, pageNumber)),
        getAlbums: () => {
            return dispatch(actions.albums.getAlbumsByTributeId(props.params.tributeUri));
        },
        deleteTribute: () => {
            return dispatch(actions.tributes.deleteTribute(tributeId));
        },
        saveFile: (file) => {
            return dispatch(actions.media.saveImage(tributeId, file))
        },
        getFiles: () => {
            dispatch(actions.media.getTributeMediaById(tributeId))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayTribute);

