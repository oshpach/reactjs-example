import React from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import classnames from 'classnames'
import {Button, Grid, Row, Col} from 'react-bootstrap'
import TributeDataRow from './TributeDataRow'
import actions from '../../actions'
import {myTributes, connectedTributes, tributes} from '../../selectors'

const styles = StyleSheet.create({
    buttonsWell: {
        maxWidth: 200,
        top: '130px',
        right: '20px',
        margin: '20px auto 10px',
        borderRadius: '2px',
        position: 'absolute',
        "@media screen and (max-width: 768px)": {
            left: '50%',
            transform: 'translateX(-50%)',
            margin: '0'
        },
        "@media screen and (max-width: 450px)": {
            fontSize: '20px'
        }
    },

    sectionText: {
        color: '#428bca',
        fontSize: '30px',
        textAlign: 'center',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        "@media screen and (max-width: 768px)": {
            marginTop: '150px'
        }
    },
    MyTributesWrapper: {
        marginTop: '100px',
        marginBottom: '70px'
    },
    createTributePlus: {
        fontSize: '20px',
        color: 'white',
        display: 'inline-block',
        marginRight: '5px',
        "@media screen and (max-width: 450px)": {
            fontSize: '15px',
            marginRight:0
        }
    },
    createButton: {
        "@media screen and (max-width: 450px)": {
            width: '150px',
            fontSize: '15px'
        }
    }
});

export class MyTributes extends React.Component {
    constructor(props) {
        super(props);

        // Bind this to functions
        this.handleClickCreateTribute = this.handleClickCreateTribute.bind(this);
    }


    componentWillMount(){
        window.scrollTo(0, 0);
        if(this.props.connectedTributes.length === 0){
            this.props.getMyTributes();
        }
    }

    handleClickCreateTribute() {
        this.props.router.push("/createtribute");
    }

    render() {
        const {myTributes, connectedTributes} = this.props;
        return (
            <div className={css(styles.MyTributesWrapper)}>
                <div className={classnames(css(styles.buttonsWell))}>
                    <Button className={css(styles.createButton)} bsSize='large' block onClick={this.handleClickCreateTribute} bsStyle="primary"><span className={css(styles.createTributePlus)}><i className="fa fa-plus"/></span> Create a Tribute</Button>
                </div>

                <h2 className={css(styles.sectionText)}>
                    {/*<i className="ii-tribute-pages" style={{fontSize: '1.5em'}} />*/}
                    <span style={{marginLeft:'10px', verticalAlign: '5px'}}>My Tributes</span>
                    <hr style={{borderTop: '2px solid #428bca', width: '50px'}}/>
                </h2>

                <Grid style={{marginTop: '40px'}}>
                    <Row>
                        {myTributes.map((tribute, index) =>
                            (<Col md={4} sm={6} xs={12} key={index}>
                                <TributeDataRow key={tribute.tributeId} tribute={tribute}/>
                            </Col> ))}
                    </Row>
                </Grid>
                {connectedTributes.length === 0 ? '' : <h2 className={css(styles.sectionText)}>
                    {/*<i className="fa fa-users" aria-hidden="true" style={{fontSize: '1.5em'}}/>*/}
                    <span style={{marginLeft:'10px', verticalAlign: '5px'}}>Connected Tributes</span>
                </h2>}

                <Grid style={{marginTop: '40px'}}>
                    <Row>
                        {connectedTributes.length ? connectedTributes.map(tribute => (
                            <Col md={4} sm={6} xs={12} key={tribute.tributeId}>
                                <TributeDataRow tribute={tribute}/>
                            </Col> )
                        ) : null}
                    </Row>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        myTributes: myTributes(state),
        tributes: tributes(state),
        connectedTributes: connectedTributes(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMyTributes: () => dispatch(actions.tributes.getMyTributes()),
        getTributes: () => dispatch(actions.tributes.getTributes())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyTributes);
