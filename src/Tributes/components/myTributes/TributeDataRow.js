import React from 'react'
import {connect} from 'react-redux'
import {withRouter, Link} from 'react-router'
// import {SplitButton, MenuItem} from 'react-bootstrap'
import {StyleSheet, css} from 'aphrodite/no-important'
import ConfirmDeleteModal from '../ConfirmDeleteModal'

import * as selectors from '../../selectors'
import actions from '../../actions'

import Common from '../../../Common'

import './TributeDataRow.css';

const {components: {DisplayAvatar, BlockTile}} = Common;

const styles = StyleSheet.create({
    tile: {
        maxWidth: '300px !important',
        borderRadius: '5px',
        position: 'relative'
    },
    tributeName: {
        display: 'inline-block',
        marginLeft: 10,
        maxWidth: '140px'
    },
    avatarContainer: {
        display: 'inline-block',
        marginLeft: 10,
        borderRadius: '50%',
        // maxWidth: '300px',
        width: '100px'
    },

    albumsButton: {
        display: 'inline-block'
    },

    albumButtonContainer: {
        display: 'inline-block',
        marginLeft: 10,
    }
});

export class TributeDataRow extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showConfirmModal: false
        };

        // Bind this to functions
        this.handleAlbumSelect = this.handleAlbumSelect.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteTributeClick = this.handleDeleteTributeClick.bind(this);
    }

    // componentDidMount(){
    //     this.props.getAlbumsByTributeId();
    // }

    handleAlbumSelect(eventKey, tributeId) {
        if (eventKey === '__create') {
            this.props.router.push({
                pathname: '/createalbum',
                state: {
                    tributeId: tributeId,
                    referrer: this.props.location
                }
            })
        } else {
            this.props.router.push({
                pathname: '/album',
                state: {
                    albumId: eventKey,
                    tributeId: tributeId,
                    referrer: this.props.location
                }
            });
        }
    };


    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.deleteTribute()
            .then(() => {
                this.props.router.push({
                    pathname: '/mytributes'
                })
            });
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    handleDeleteTributeClick() {
        this.setState({
            showConfirmModal: true
        })
    }


    render() {

        const {tribute = ''} = this.props;
        console.log(tribute);

        return (
            <BlockTile className={css(styles.tile)}>
                <Link to={{pathname: `/tribute/${tribute.tributeUri}`, state: {tributeId: tribute.tributeId}}} style={{display: 'inline-block'}}>
                    <div className={css(styles.avatarContainer)}>
                        <DisplayAvatar user={tribute} size={'100px'}/>
                    </div>

                    <div className={css(styles.tributeName)}>{tribute.firstName} {tribute.lastName}</div>
                </Link>
                <ConfirmDeleteModal title="Delete Tribute" body={<div><p>Confirm delete tribute?</p><p style={{fontWeight: 'bold', color: 'red'}}>This action can not be undone.</p></div>}
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled} show={this.state.showConfirmModal}/>
            </BlockTile>
        )
    }
}

const mapStateToProps = (state, props) => {
    const {tribute: {tributeId}} = props;

    return {
        albums: selectors.getAlbumsByTributeId(state, tributeId)
    }
};

const mapDispatchToProps = (dispatch, props) => {
    const {tribute: {tributeId}} = props;

    return {
        getAlbumsByTributeId: () => dispatch(actions.albums.getAlbumsByTributeId(tributeId)),
        deleteTribute: () => dispatch(actions.tributes.deleteTribute(tributeId))
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(TributeDataRow));

