import React from 'react'
import {connect} from 'react-redux'
import {Button} from 'react-bootstrap'
import {Field, reduxForm, destroy} from 'redux-form'
import Common from '../../Common'
import {StyleSheet, css} from 'aphrodite/no-important'
import actions from '../actions'
import './TributeGalleryForm.css'

const styles = StyleSheet.create({
    commentsForm: {
        marginLeft: '10px',
        marginRight: '10px',
        color: 'black',
        display: 'flex',
        maxHeight: '70px',
        overflowY: 'auto'
    }
});


const {
    components: {EditorRichText},
} = Common;

class TributeGalleryForm extends React.Component{




    submit = (value) => {
        if(this.props.shouldEdit){
            let comment = value.edit;
            this.props.updateComment(this.props.commentId, this.props.mediaId, comment)
                .then((data) => {
                    console.log(data);
                })
                .catch((err) => {throw err});
            this.props.changeState();
        } else {
            let comment = value.edit;
            this.props.dispatch(destroy('CommentsForm'));
            this.props.saveComment(this.props.mediaId, comment)
                .then((data) => {
                    console.log(data);
                })
                .catch((err) => {
                    throw err
                });
        }

    };

    render(){
        console.log(this.props);
        const {handleSubmit} = this.props;
        return(
            <form className={css(styles.commentsForm)} onSubmit={handleSubmit(this.submit)}>
                <Field  name="edit" type="text" component={EditorRichText} className="commentInput" placeholder="Type here..."/>
                <Button type="submit">Click</Button>
            </form>
        )
    }

}


TributeGalleryForm = reduxForm({
    form: 'CommentsForm',  // a unique identifier for this form
})(TributeGalleryForm);



TributeGalleryForm = connect(
    state => {
        return {
        initialValues: state.comments.dictionary.initial

    }},

    (dispatch) =>{
        return  {
            saveComment: (mediaId, comment) => dispatch(actions.media.saveMediaComment(mediaId, comment)),
            updateComment: (commentId, mediaId, comment) => dispatch(actions.media.editMediaComment(commentId, mediaId, comment))
        }
    }
)(TributeGalleryForm);

export default TributeGalleryForm;