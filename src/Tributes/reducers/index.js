import albums from './albums'
import albumMedia from './albumMedia'
import tributes from './tributes'
import media from './media'
import comments from './comments'
import connectedTributes from './connect'
import connectStatus from './connectStatus'

export default {
    albums,
    albumMedia,
    tributes,
    media,
    comments,
    connectedTributes,
    connectStatus
}
