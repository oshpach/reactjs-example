import {combineReducers} from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function dictionary(state = {}, action) {
    switch (action.type) {

        case actionType.SUCCESS_SAVE_TRIBUTE_AVATAR:
        case actionType.SUCCESS_DELETE_TRIBUTE_AVATAR:
        case actionType.SUCCESS_GET_TRIBUTE:
        case actionType.SUCCESS_CREATE_TRIBUTE: {
            console.log(action, state);
            // Convert Array of Objects to Object of Objects
            const obj = List(action['tributes'])
                .reduce( (result, item) => {
                    result[item['tributeId']] = {...state[item['tributeId']], ...item};
                    return result; }, {} );


            return {
                ...state,
                ...obj
            };
        }

        case actionType.REMOVE_TRIBUTE: {
            const {[action.tributeId]: removeElement, ...rest} = state;

            return {
                ...rest
            };
        }

        default:
            return state;
    }
}

export default combineReducers({
    dictionary
})
