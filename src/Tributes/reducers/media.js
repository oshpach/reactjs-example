import {combineReducers} from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function dictionary(state = {}, action) {
    switch (action.type) {

        case actionType.SUCCESS_TRIBUTE_MEDIA: {

            // Convert Array of Objects to Object of Objects



            const obj = List(action['media'])
                .reduce( (result, item) => {
                    result[item['mediaId']] = {...state[item['mediaId']], ...item};
                    return result; }, {} );


            return {
                ...state,
                ...obj
            };

        }

        case actionType.REMOVE_TRIBUTE_MEDIA: {
            const {[action.mediaId]: removeElement, ...rest} = state;

            return {
                ...rest
            };
        }

        default:
            return state;
    }
}

export default combineReducers({
    dictionary
})
