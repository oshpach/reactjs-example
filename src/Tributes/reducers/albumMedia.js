import {combineReducers} from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function dictionary(state = {}, action) {
    switch (action.type) {
        case actionType.SUCCESS_SAVE_ALBUM_MEDIA: {

            return {
                ...state,
                [action['albumMediaId']]: action['album']
            };
        }

        case actionType.SUCCESS_CROP_ALBUM_IMAGE:
        case actionType.SUCCESS_GET_ALBUM_MEDIA: {
            // Convert Array of Objects to Object of Objects
            const obj = List(action['albumMedia'])
                .reduce( (result, item) => { result[item['albumMediaId']] = item; return result; }, {} );

            return {
                ...state,
                ...obj
            };
        }

        case actionType.REMOVE_ALBUM_MEDIA: {
            const {[action.albumMediaId]: removeElement, ...rest} = state;

            return {
                ...rest
            };
        }

        default:
            return state;
    }
}

export default combineReducers({
    dictionary: dictionary
})
