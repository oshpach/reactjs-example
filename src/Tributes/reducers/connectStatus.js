import { combineReducers } from 'redux';
// import { status } from '../../Common/reducers'
import * as actionType from '../actionTypes'

function connectList(state = [], action) {
    switch (action.type) {
        case actionType.SUCCESS_GET_CONNECT_STATUS: {
            console.log(state);

            return action.tributes;
        }

        case actionType.REMOVE_CONNECT_STATUS: {
            // eslint-disable-next-line no-unused-vars
            const {[`${action.tributeId} ${action.userId}`]: remove, ...rest} = state;

            return {...rest};
        }

        case actionType.SUCCESS_CONNECT_TO_TRIBUTE: {

            console.log(state, action);

            // const newState = {...state};
            // const key = `${action.tributeId} ${action.userId}`;
            const newState = [...state.map((tribute) => {
                if(tribute.userId === action.userId){
                    tribute.status = action.status;
                    return tribute;
                }
                return tribute
            })];

            // if(! newState[key]){
            //     newState[key] = {};
            // }
            //
            // newState[key][status] = action.status;

            return [...newState];

        }
        case actionType.SUCCESS_DISCONNECT_TO_TRIBUTE: {
            // const list = List(state);
            // const index = list.indexOf(action.tributeId);
            //
            // if(index !== -1){
            //     return list.delete(index).toArray();
            // }
            //
            // return state;
            console.log(state, action);
            const newState = [...state.map((tribute) => {
                if(tribute.userId === action.userId){
                    tribute.status = action.status;
                    return tribute;
                }
                return tribute
            })];
            console.log([...newState]);

            return [...newState];
        }

        default:
            return state;
    }
}

export default combineReducers({
    list: connectList
})
