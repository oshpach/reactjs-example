import {combineReducers} from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function dictionary(state = {}, action) {
    switch (action.type) {
        case actionType.SUCCESS_CREATE_ALBUM:
        case actionType.SUCCESS_GET_ALBUM:
        case actionType.SUCCESS_GET_ALBUMS: {
            // Convert Array of Objects to Object of Objects
            const obj = List(action['albums'])
                .reduce( (result, item) => { result[item['albumId']] = item; return result; }, {} );

            return {
                ...state,
                ...obj
            };
        }

        case actionType.REMOVE_ALBUM: {

            const {[action.albumId]: albumId, ...rest} = state;

            return {
                ...rest
            };
        }

        default:
            return state;
    }
}

export default combineReducers({
    dictionary: dictionary
})
