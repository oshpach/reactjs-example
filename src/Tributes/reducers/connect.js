import { combineReducers } from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function connectArray(state = [], action) {
    switch (action.type) {
        // case actionType.SUCCESS_CONNECT_TO_TRIBUTE: {
        //     if(List(state).indexOf(action.tributeId) === -1){
        //         return [...state, ...[action.tributeId]]
        //     }
        //
        //     return state;
        // }

        // case actionType.SUCCESS_DISCONNECT_TO_TRIBUTE: {
        //     // const list = List(state);
        //     // const index = list.indexOf(action.tributeId);
        //     //
        //     // if(index !== -1){
        //     //     return list.delete(index).toArray();
        //     // }
        //     //
        //     // return state;
        //     console.log(state, action);
        //     const newState = [...state.map((tribute) => {
        //         if(tribute.userId === action.userId){
        //             tribute.status = action.status;
        //             return tribute;
        //         }
        //         return tribute
        //     })];
        //     console.log([...newState]);
        //
        //     return [...newState];
        // }

        case actionType.ADD_TRIBUTE: {
            const tributeIds = List(action.tributes)
                .filter(tribute => tribute.connected)
                .map(tribute => tribute.tributeId).toArray();

            let stateIds = List(state);

            tributeIds.forEach(tributeId => {
                if((stateIds).indexOf(tributeId) === -1){
                    stateIds = stateIds.push(tributeId)
                }
            });

            return stateIds;
        }

        default:
            return state;
    }
}

export default combineReducers({
    list: connectArray,
})