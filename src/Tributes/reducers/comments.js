import { combineReducers } from 'redux';
import * as actionType from '../actionTypes'
import {List} from 'immutable'

function dictionary(state = {}, action) {
    switch (action.type) {

        case actionType.SUCCESS_SAVE_COMMENT:
        case actionType.SUCCESS_EDIT_COMMENT:
         {
            // Convert Array of Objects to Object of Objects
            const obj = List(action['comments'])
                .reduce( (result, item) => { result[item['commentId']] = item; return result; }, {} );

            return {
                ...state,
                ...obj
            };
        }

        case actionType.SUCCESS_GET_COMMENTS:
            const obj = List(action['comments'])
                .reduce( (result, item) => { result[item['commentId']] = item; return result; }, {} );

            return {
                ...obj
            };

        case actionType.SUCCESS_GET_COUNT_OF_COMMENTS:
            const count = List(action['data'])
                .reduce( (result, item) => { result[item['mediaId']] = item['data']; return result; }, {} );

            return {
                ...count
            };


        case actionType.SUCCESS_REMOVE_COMMENT: {
            const {[action.commentId]: removeElement, ...rest} = state;

            return {
                ...rest
            };
        }

        default:
            return state;
    }
}

export const load = data => ({type: "LOAD", data});

export default combineReducers({
    dictionary
})