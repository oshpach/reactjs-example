import {actionCreator, getDataAction, postDataAction, deleteDataAction} from '../../Common/actions'
import * as actionType from '../actionTypes'

export function getAlbumsByTributeId(tributeId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/album/tribute/${tributeId}`)
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_GET_ALBUMS, response));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function getAlbum(albumId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/album/${albumId}`)
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_GET_ALBUM, response));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function createAlbum(model) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction('/album', {...model})
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_CREATE_ALBUM, {albums: [response]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve({...response});
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function deleteAlbum(albumId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return deleteDataAction(
            '/album',
            {
                albumId
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.REMOVE_ALBUM, {albumId}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function saveAlbumMedia(model) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction('/album/media', {...model})
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_SAVE_ALBUM_MEDIA, {albumId: response.albumId, albumMediaId: response.albumMediaId, album: {...response}}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function getAlbumMedia(albumId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/album/media/${albumId}`)
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_GET_ALBUM_MEDIA, response));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(response);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function cropImage(albumMediaId, crop, scale, size) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction(
            '/album/cropimage',
            {
                albumMediaId,
                crop,
                scale,
                size
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_CROP_ALBUM_IMAGE, {albumMedia: [{...data.albumMedia}]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function removeMedia(albumMediaId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));


        return deleteDataAction(`/album/removeMedia/${albumMediaId}`)
            .then(data => {
                dispatch(actionCreator(actionType.REMOVE_ALBUM_MEDIA, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

