import * as albums from './albums'
import * as connected from './connected'
import * as media from './media'
import * as tributes from './tributes'

export default {albums, connected, tributes, media}

