import { actionCreator, getDataAction, postDataAction, putDataAction, deleteDataAction } from '../../Common/actions'
import * as actionType from '../actionTypes'

export function getTributes() {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction('/tribute')
            .then((data) => {
                console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_GET_TRIBUTE, {tributes: data}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function getTribute(tributeId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/tribute/truid-${tributeId}/main`)
            .then((data) => {
                dispatch(actionCreator(actionType.SUCCESS_GET_TRIBUTE, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}


export function getMyTributes() {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction('/tribute/mine')
            .then((data) => {
                console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_GET_TRIBUTE, {tributes: data}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function createTribute(model) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction('/tribute', {...model})
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_CREATE_TRIBUTE, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}


export function saveTributePart(tributeId, model, tabName) {
    const part = tabName || 'main';
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return putDataAction(`/tribute/truid-${tributeId}/${part}`, model)
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_SAVE_TRIBUTE_PART, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}


export function getTributePart(tributeId, tabName) {
    const part = tabName || 'main';
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/tribute/truid-${tributeId}/${part}`)
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_GET_TRIBUTE_PART, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function saveAvatar(tributeId, file) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        let fd = new FormData();
        fd.append('avatarFile', file);

        return postDataAction(`/tribute/avatar/${tributeId}`, fd)
            .then((data) => {
                dispatch(actionCreator(actionType.SUCCESS_SAVE_TRIBUTE_AVATAR, {tributes: [data]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function cropAvatar(tributeId, crop, scale) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction(
            '/tribute/cropavatar',
            {
                tributeId,
                crop,
                scale
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_SAVE_TRIBUTE_AVATAR, {tributes: [data]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function deleteAvatar(tributeId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return deleteDataAction(
            '/tribute/avatar',
            {
                tributeId
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_DELETE_TRIBUTE_AVATAR, {tributes: [data]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function deleteTribute(tributeId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return deleteDataAction(
            '/tribute',
            {
                tributeId
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.REMOVE_TRIBUTE, {tributeId}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}
