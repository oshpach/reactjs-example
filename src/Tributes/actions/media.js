import {actionCreator, getDataAction, postDataAction, putDataAction, deleteDataAction, patchDataAction} from '../../Common/actions'
import * as actionType from '../actionTypes'

export function getTributeMediaById(tributeId, pageNumber) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        // const pageNumber = 0;
        const pageSize = 8;

        return getDataAction(`/media/${tributeId}/${pageNumber}/${pageSize}?${new Date().getSeconds()}`)
            .then((data) => {
            console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_TRIBUTE_MEDIA, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function getMediaComments(mediaId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction(`/media-comment/media/${mediaId}`)
            .then((data) => {
            console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_GET_COMMENTS, {comments: data}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function saveMediaComment(mediaId, comment) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction(`/media-comment/meid-${mediaId}`, `"${comment}"`)

            .then(data => {
                console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_SAVE_COMMENT, {comments: [data]}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function deleteMediaComment(commentId, mediaId) {
    return (dispatch) => {
        return deleteDataAction(`/media-comment/meid-${mediaId}/${commentId}`)
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_REMOVE_COMMENT, {commentId}));

                console.log(data);
                return Promise.resolve();
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

export function editMediaComment(commentId, mediaId, comment) {
    return (dispatch) => {
        return patchDataAction(`/media-comment/meid-${mediaId}/${commentId}`, `"${comment}"`)
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_EDIT_COMMENT, {comments: [data]}));

                return Promise.resolve(data);
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

export function getCommentsCountByMediaId(media) {

    return (dispatch) => {

        return getDataAction(`/media-comment/count/${media[0].mediaId}`)
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_GET_COUNT_OF_COMMENTS, [{data: {data, mediaId: media[0].mediaId}}]));
                return Promise.resolve(data);
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

export function saveImage(tributeId, file) {
    return (dispatch) => {

        return postDataAction(`/media/pre-signed-url/${tributeId}`, { Type: file.type, Name : file.name })
            .then(mediaData => {
                return putDataAction(mediaData.uri, file, { 'Content-Type': file.type }, false)
                    .then(data => {
                        return Promise.resolve(mediaData);
                    })
                    .catch(error => {
                        return Promise.reject(error);
                    });
                    
            })
            .catch(error => {
                return Promise.reject(error);
            });      
    }
}

export function cropImage(mediaId, crop, scale) {
    return (dispatch) => {
        return postDataAction(
            '/media/cropimage',
            {
                mediaId,
                crop,
                scale
            }
        )
            .then(data => {
                // console.log(data);
                return Promise.resolve(data);
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

export function deleteImage(tributeId, mediaId) {
    return (dispatch) => {
        return deleteDataAction(
            '/media/image',
            {
                mediaId
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.REMOVE_TRIBUTE_MEDIA, {mediaId}));

                console.log(data);
                return Promise.resolve();
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

/*
export function saveMediaAttributes(mediaId, attributes) {
    return (dispatch) => {

        dispatch(actionCreator(actionType.REQUESTING_MEDIA_ATTRIBUTES, {mediaId}));

        return dispatch(putDataAction(
            '/media/attributes',
            {
                mediaId,
                ...attributes
            }
        ))
            .then(response => {
                dispatch(actionCreator(actionType.SUCCESS_MEDIA_ATTRIBUTES, {media: response.media}));
                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.ERROR_MEDIA_ATTRIBUTES, {mediaId}));
                return Promise.reject(error);
            });
    }
}
*/
