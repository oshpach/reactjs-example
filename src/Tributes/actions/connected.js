import { actionCreator, getDataAction, postDataAction, patchDataAction } from '../../Common/actions'
import * as actionType from '../actionTypes'


export function getConnectedTributes() {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction('/tribute/connected')
            .then((data) => {
                dispatch(actionCreator(actionType.ADD_TRIBUTE, {tributes: data.tributes}));

                dispatch(actionCreator(actionType.SUCCESS_TRIBUTE));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function getConnectRequests(status) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return getDataAction('/tribute-connect/all-tributes/', {status})
            .then((data) => {
                console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_GET_CONNECT_STATUS, {tributes: data}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function connectRequest(tributeId, message) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction(
            `/tribute-connect/truid-${tributeId}`,
            {
                message
            }
        )
            .then(data => {
                console.log(data);
                dispatch(actionCreator(actionType.SUCCESS_CONNECT_TO_TRIBUTE, {...data.connectTribute}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function connectAccept(userId, tributeId) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));
        console.log(userId, tributeId);

        return patchDataAction(
            `/tribute-connect/accept/truid-${tributeId}`,
            `"${userId}"`
        )
            .then(data => {
                dispatch(actionCreator(actionType.SUCCESS_CONNECT_TO_TRIBUTE, { userId, tributeId, status: 'accepted'}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function connectDisconnect(userId, tributeId) {
    console.log(userId, tributeId);
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return patchDataAction(
            `/tribute-connect/decline/truid-${tributeId}`,
            `"${userId}"`
        )
            .then(data => {
                console.log(data);

                dispatch(actionCreator(actionType.SUCCESS_DISCONNECT_TO_TRIBUTE, { userId, tributeId, status: 'rejected'}));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

