import {Map, List} from 'immutable'

export function tributes(state) {
    return Map(state.tributes.dictionary).toArray();
}

export function myTributes(state) {
    return Map(state.tributes.dictionary)
        .filter(value => value['role'] && (value['role'].toLowerCase() === 'owner' || value['role'].toLowerCase() === 'manager'))
        .toArray();
}

export function tributeByUri(state, tributeUri) {
    return Map(state.tributes.dictionary)
        .filter((value) => value['tributeUri'] === tributeUri.toLowerCase())
        .toArray()
        .filter((item) => item)[0];
}

export function connectedTributes(state) {
    return Map(state.tributes.dictionary)
        .filter(value => value['role'] && value['role'] === 'Friend')
        .toArray();
}

export function acceptedUsers(state) {
    return List(state.connectStatus.list)
        .filter(value => {
                return value['status'] === 'Accepted';
            }
        )
        .toArray();
}

export function acceptedUsersToMessage(state) {
    console.log(state.connectStatus.list);
    const obj = state.connectStatus.list
        .reduce( (result, item) => { result[item.userId] = item; return result; }, {} );
    // let relult = [];
    // for(let i = 0; i < usersId.length; i += 1){
    //
    //     for(let j = 0; j < state.connectStatus.list.length; j += 1){
    //         if(usersId[i] === state.connectStatus.list[j].userId && state.connectStatus.list[j].message){
    //
    //             if(!relult){
    //                 relult[usersId[i]] = {
    //                     user
    //                 };
    //             }
    //             relult[usersId[i]].push(state.connectStatus.list[j].message);
    //
    //         }
    //     }
    // }
    // console.log(relult);

    return Map(obj).map((item) => item).toArray()
        .sort((a, b) => {
            if(a.requestedOn > b.requestedOn){
                return -1;
            } else if(a.requestedOn < b.requestedOn){
                return 1;
            } else {
                return 0;
            }
        });

}

export function rejectedUsers(state) {
    return List(state.connectStatus.list)
        .filter(value => {
                return value['status'] === 'Declined';
            }
        )
        .toArray();
}

export function tributeConnectRequests(state) {
    return List(state.connectStatus.list)
        .filter(value => {
                return value['status'] === 'Requested';
            }
        )
        .toArray();
}

export function tributeById(state, tributeId) {
    return state.tributes.dictionary[tributeId];
}

/*
export function myTributeById(state, tributeId) {
    const myTribute = state.tributes.dictionary[tributeId];
    return (myTribute && myTribute['role'] && myTribute['role'].toLowerCase() === 'owner') || {};
}
*/
export function getMediaById(state, mediaId) {
    return state.media.dictionary[mediaId];
}

export function myTributeBySlug(state, slug) {
    const map = Map(state.myTributes.dictionary);
    return map.find((value) => {
            return value.get('slug') === slug
        }) || {};
}

export function tributeMediaById(state, tributeId) {
    return Map(state.media.dictionary)
        .filter(value => value['tributeId'] === tributeId)
        .sort((a, b) => {

            if (a.created < b.created) {
                return 1;
            }
            if (a.created > b.created){
                return -1;
            }
            /*if(a.createDate === b.createDate) { return 0;}*/
            return 0;
        })
        .toArray();
}

export function tributeMediaByUri(state, tributeUri) {
    return Map(state.media.dictionary)
        .filter(value => value['tributeUri'] === tributeUri.toLowerCase())
        .sort((a, b) => {

            if (a.created < b.created) {
                return 1;
            }
            if (a.created > b.created){
                return -1;
            }
            /*if(a.createDate === b.createDate) { return 0;}*/
            return 0;
        })
        .toArray();
}

export function commentsByMediaId(state, mediaId) {
    let comments = Map(state.comments.dictionary)
        .filter((value) => value.mediaId === mediaId);

    //let comments = state.comments.dictionary[mediaId];
    if (comments) {
        comments = Map(comments).sort((a, b) => {
            if (a.createDate < b.createDate) {
                return -1;
            }
            if (a.createDate > b.createDate) {
                return 1;
            }
            /*if(a.createDate === b.createDate) { return 0;}*/
            return 0;
        })
    }

    return comments.toArray() || [];
}

export function sortedComments(state) {
    console.log(state.comments.dictionary);
    let comments = Map(state.comments.dictionary)
        .sort((a, b) => {
        if (a.createdOn < b.createdOn) {
            return -1;
        }
        if (a.createdOn > b.createdOn) {
            return 1;
        }
        /*if(a.createDate === b.createDate) { return 0;}*/
        return 0;
    });
    return comments.toArray();
}

export function connectedToTribute(state, tributeId) {
    const connectedTributes = state.connectedTributes.list;

    return List(connectedTributes).indexOf(tributeId) !== -1;
}

export function getAlbumById(state, albumId) {
    return state.albums.dictionary[albumId];
}

export function getAlbumByUri(state, albumUri) {
    let album =  Map(state.albums.dictionary)
        .filter((album) => album["albumUri"] === albumUri.toLowerCase())
        .toJS();
    return album[Object.keys(album)[0]];
}

export function getAlbumsByTributeId(state, tributeUri) {
    return Map(state.albums.dictionary)
        .filter(value => value["tributeUri"] === tributeUri.toLowerCase())
        .sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }

            return 0;
        })
        .toArray();
}

/*export function getAlbumMediaByAlbumId(state, albumId) {
    return Map(state.albumMedia.dictionary)
        .filter(value => value.albumId === albumId)
        .toArray();
}*/

export function getAlbumMediaByAlbumId(state, albumId) {
    return Map(state.albumMedia.dictionary)
        .filter(value => value.albumId === albumId)
        .toArray();
}

export function getAlbumMediaByAlbumUri(state, albumUri) {
    return Map(state.albumMedia.dictionary)
        .filter(value => value["albumUri"] === albumUri.toLowerCase())
        .toArray();
}


export function getAlbumMediaForPosition(state, albumId, row, col, container, position) {
    let data =  Map(state.albumMedia.dictionary)
        .find(value => value.albumId === albumId
                        && value.row === row
                        && value.col === col
                        && value.container === container
                        && value.position === position);

    data = data || {};
    const media = getMediaById(state, data.mediaId);
    return {...data, media};
}

