import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {Router, browserHistory} from 'react-router';

import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.css';
import './index.css';

import configureStore from './store';
import routes from './routes';
import {initEnvironment} from './utils';

const store = configureStore();
initEnvironment(store); // init global vars for the environment
const history = syncHistoryWithStore(browserHistory, store);

render(
    <Provider store={store}>
        <Router history={history} routes={routes(store)} />
    </Provider>,

    document.getElementById('root')
);
