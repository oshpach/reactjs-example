import React from 'react';
import {Grid, Row, Col} from 'react-bootstrap'
import {logo} from '../Common/images';
import './Footer.css';
import  Common from '../Common';

const {helpers: {windowDimension}} = Common;


const Footer = (props) => {
    console.log(windowDimension());
    return (
        <footer className={`footer ${props.className}`} style={{bottom: `${windowDimension().width < 768 ? 0 : props.bottom}`}}>
            <Grid>
                <Row className='footer-wrapper'>
                    <Col md={5} sm={5} xs={12} className='footer-left'>
                        <Row>
                            <Col md={3}>
                                <img src={logo} alt="logo"/>
                            </Col>
                            <Col md={9}>
                                <p>
                                    Lorem ipsum  Lorem ipsum  Lorem ipsum  Lorem ipsum  Lorem ipsum  Lorem ipsum  Lorem ipsum  Lorem ipsum
                                </p>
                            </Col>
                        </Row>
                    </Col>
                    <Col md={7} sm={7} xs={12} className='footer-right'>
                        <Row>
                            <Col md={4} sm={4} xs={4}>
                                <p>Features</p>
                                <p>Explore</p>
                                <p>Pricing</p>
                            </Col>
                            <Col md={4} sm={4} xs={4}>
                                <p>About Us</p>
                                <p>Projects</p>
                                <p>Blog</p>
                            </Col>
                            <Col md={4} sm={4} xs={4}>
                                <p>Terms</p>
                                <p>Privacy</p>
                                <p>Security</p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <p className="footer-center">2016 LiveOn Company. All Right Reserved.</p>
                    </Col>
                </Row>
            </Grid>
        </footer>
    )
};

export default Footer;