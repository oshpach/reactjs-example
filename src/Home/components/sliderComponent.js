import React from 'react';
import Slider from 'react-slick'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import actions from '../../Tributes/actions'
import {StyleSheet, css} from 'aphrodite/no-important'
import * as selectors from '../../Tributes/selectors'
import 'slick-carousel/slick/slick-theme.css'
import 'slick-carousel/slick/slick.css'
import './slickComponent.css'

const styles = StyleSheet.create({
    sliderPhoto: {
        width: '260px',
        height: '260px',
        overflow: 'hidden',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: 'cover'
    }
});

class SimpleSlider extends React.Component {

        constructor(props){
            super(props);
            this.linkToTribute = this.linkToTribute.bind(this);
        }

        componentWillMount() {
            this.props.getTributes();
        }

        trib = (cb) => {
            return cb;
        };

        linkToTribute(event, tribute){
            event.preventDefault();
            this.props.router.push({
                pathname: `/tribute/${tribute.tributeUri}`,
                state: {
                    tributeId: tribute.tributeId
                }
            });
        }

        render() {
            const settings = {
                className: 'center',
                centerMode: true,
                infinite: true,
                centerPadding: '0',
                slidesPerRow: 3,
                slidesToShow: 3,
                speed: 500,
                autoplay: true,
                autoplaySpeed: 3000,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            centerPadding: '20px',
                            centerMode: false,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerPadding: '20px',
                            centerMode: false,
                            infinite: true,
                            speed: 300,
                            slidesToShow: 1
                        }
                    }
                ]
            };
            let tr = this.trib(this.props.tributes);
            tr = tr.filter((tribute) => tribute.avatarUri && (tribute.firstName && tribute.lastName));
            tr = tr.slice(0, 4);

        return (
            <div>
                <Slider {...settings} className={this.props.className}>
                    {tr.length > 0 ? tr.map((tribute, index) => {
                        return (
                            <div style={{backgroundImage: `url(http://liveonmediaupload.s3.amazonaws.com/${tribute.avatarUri})`}}  onClick={(event) => this.linkToTribute(event, tribute)} key={index} className={css(styles.sliderPhoto)}>
                                <div></div>
                                <div className="tribute-title">{`${tribute.firstName} ${tribute.lastName}`}</div>
                            </div>
                        )
                    }) : <div></div>}
                </Slider>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    return {
        tributes: selectors.tributes(state),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getTributes: () => {
            dispatch(actions.tributes.getTributes())
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SimpleSlider));