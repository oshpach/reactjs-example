import React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important'
import Common from '../../Common'

const {components: {DisplayAvatar}} = Common;

const styles = StyleSheet.create({

    sliderBlock: {
        cursor: 'pointer',
    },
    photoSlider: {
        height: '200px'
    }

});


class SliderItem extends React.Component{

    render(){
        const { tribute = [] } = this.props;
        return (
            <div className={css(styles.photoSlider)}>
                <DisplayAvatar size={100} user={tribute}/>
                <p className={css(styles.sliderItemTitle)}>{`${this.props.tribute.firstName} ${this.props.tribute.lastName}`}</p>
            </div>
        )
    }

};



export default SliderItem;