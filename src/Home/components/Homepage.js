import React from 'react';
import {connect} from 'react-redux'
import {withRouter} from 'react-router';
import {Grid, Row, Col} from 'react-bootstrap'
import SimpleSlider from './sliderComponent'
import {StyleSheet, css} from 'aphrodite/no-important'
import * as selectors from '../../Tributes/selectors'
import actions from '../../Tributes/actions'
import './Homepage.css'

const styles = StyleSheet.create({
    sliderTop: {
        marginTop: '250px',
        "@media screen and (max-width: 768px)": {
            marginTop: '150px'
        },
        "@media screen and (max-width: 500px)": {
            marginTop: '100px'
        }
    },
    homeTitle: {
        textAlign: 'center',
        width: '100%',
        marginBottom: '100px'
    },
    tributePhoto: {
        position: 'absolute',
        height: '100%',
        width: '100%',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        boxShadow: '3px 3px 3px rgba(0, 0, 0, 0), -3px -3px 3px rgba(0, 0, 0, 0)'
    },
    tributeWrapper: {
        minHeight: '270px',
        marginBottom: '30px',
        position: 'relative',
        '@media screen and (max-width: 991px)': {
            minHeight: '100px'
        },
        '@media screen and (max-width: 768px)': {
            marginTop: '30px'
        }
    },
    subtitle: {
        position: 'absolute',
        top: '100%',
        right: '0',
        left: '0',
        transition: 'all ease 0.4s',
        color: 'black',
        textTransform: 'uppercase',
        letterSpacing: '1px',
        ":hover": {
            color: '#3498DB',
            cursor: 'pointer'
        },
        "@media screen and (max-width: 400px)": {
            fontSize: '15px'
        }

    },
    sloganTop: {
        marginTop: '100px',
        marginBottom: '100px',
        minHeight: '200px',
        display: 'flex',
        justifyContent: 'center',
        position: 'relative',
        background: 'linear-gradient(to top, rgba(52, 152, 219, 0.26), rgba(255, 255, 255, 0.3))',
        backgroundColor: 'rgba(52, 152, 219, .2)',
        boxShadow: '6px 0 6px rgba(52, 152, 219, .2), -6px 0 6px rgba(52, 152, 219, .2)',
        textAlign: 'center'
    },
    sloganTopTitle: {
        alignSelf: 'center',
        fontFamily: 'Raleway, Helvetica, sans-serif',
        fontWeight: 'bold',
        color: 'white',
        fontSize: '32px',
        textShadow: '3px 6px 6px rgba(55, 69, 79, .2)',
        "@media screen and (max-width: 1000px)": {
            fontSize: '28px'
        },
        "@media screen and (max-width: 700px)": {
            fontSize: '25px'
        }
    },
    marginBottom: {
        marginBottom: '150px',
        "@media screen and (max-width: 700px)": {
            marginBottom: '100px'
        }
    },
    interestingTributes: {
        textTransform: 'uppercase',
        color: '#3498DB',
        fontFamily: 'Raleway, Helvetica, sans-serif'
    }
});

class Homepage extends React.Component {

    constructor(props){
        super(props);
        this.linkToTribute = this.linkToTribute.bind(this);
    }

    componentWillMount() {
        this.props.getTributes();
    }


    linkToTribute(tribute){
        this.props.router.push({
            pathname: `/tribute/${tribute.tributeUri}`,
            state: {
                tributeId: tribute.tributeId
            }
        });
    }

    render(){
        const tributes = this.props.tributes.filter((tribute) => tribute.avatarUri !== null)
            .slice(3, 7);
        return (
            <div style={{overflowX: 'hidden'}} id="mainSlider">
                <SimpleSlider className={css(styles.sliderTop)}/>
                <Grid fluid className={css(styles.marginBottom)}>
                    <Row>
                        <Col md={12} className={css(styles.sloganTop)}>
                            <p className={css(styles.sloganTopTitle)}>Life is like riding a bicycle, to keep your balance you must keep moving.</p>
                        </Col>
                    </Row>
                    <div className={css(styles.homeTitle)}>
                        <h2 className={css(styles.interestingTributes)}>Interesting tributes</h2>
                        <span>Enjoy reading the best tributes!</span>
                        <Row style={{marginTop: '50px'}}>
                            {tributes.map((tribute) => (
                                <Col md={3} sm={4} xs={6} key={tribute.tributeId} className={css(styles.tributeWrapper)} onClick={() => {this.linkToTribute(tribute)}}>
                                    <div id="animation" style={{position:'relative', width: '100%',paddingBottom: '100%', overflow:'hidden'}}>
                                        <div className={css(styles.tributePhoto)} id="tributePhotoAnimation" style={{backgroundImage: `url(http://liveonmediaupload.s3.amazonaws.com/${tribute.avatarUri})`}}></div>
                                        <div className="homeAnimation">
                                            <button className="seeMore" id="buttonNone">Read more</button>
                                        </div>
                                        <button className="seeMore">Read more</button>
                                    </div>
                                    <span className={css(styles.subtitle)}>{tribute.firstName + ' ' + tribute.lastName}</span>
                                </Col>
                            ))}
                        </Row>
                    </div>
                </Grid>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        tributes: selectors.tributes(state),
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getTributes: () => {
            dispatch(actions.tributes.getTributes())
        }
    };
};


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Homepage));
