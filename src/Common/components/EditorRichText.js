import React from 'react';
import RichTextEditor from 'react-rte';
import {StyleSheet, css} from 'aphrodite/no-important';

const styles = StyleSheet.create({
    rteContainer: {
        color: '#333333',
        borderColor: '#cccccc',
        borderRadius: 6,
        backgroundColor: 'white',
    }
});

class EditorRichText extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: RichTextEditor.createValueFromString(props.input.value, 'html')
        };

        // Bind this to functions
        this.onChange = this.onChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // Set State
        if (nextProps.meta.pristine) {
            this.setState({
                value:  RichTextEditor.createValueFromString(nextProps.input.value, 'html')
            })
        }
    }

    onChange = (value) => {
        this.setState({value});
        if (this.props.input.onChange) {
            // Send the changes up to the parent component as an HTML string.
            // This is here to demonstrate using `.toString()` but in a real app it
            // would be better to avoid generating a string on each change.
            this.props.input.onChange(
                value.toString('html')
            );
        }
    };

    render () {
        return (
            <RichTextEditor
                value={this.state.value}
                onChange={this.onChange}
                className={css(styles.rteContainer)}
                onKeyDown={this.props.onKeyDown}
                {...this.props}
            />
        );
    }
}

export default EditorRichText