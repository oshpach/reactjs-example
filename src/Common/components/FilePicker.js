import React from 'react';
import Dropzone from 'react-dropzone';
import {css} from 'aphrodite';


/*
 styles={
     filePicker: {
        Base styles for control
            ':hover': {
                styles for when mouse over
            }
     },

     filePickerDragOver: {
        styles append to Base styles for when a drag over enters
     }
 }
 */

class FilePicker extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            isDragOver: false
        };

        // Bind this to functions
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDragEnter = this.handleDragEnter.bind(this);
        this.handleDragLeave = this.handleDragLeave.bind(this);
    }

    handleDragEnter() {
        this.setState({isDragOver: true})
    }

    handleDragLeave() {
        this.setState({isDragOver: false})
    }

    handleDrop() {
        this.setState({isDragOver: false})
    }

    render() {

        const {
            input,
            meta,           // eslint-disable-line no-unused-vars
            filePickerOnDrop,
            styles,
            ...props
        } = this.props;

        let className;
        if(styles && styles.filePicker){
            className = css(this.state.isDragOver ? [styles.filePicker, styles.filePickerDragOver] : styles.filePicker);
        }

        return (
            <Dropzone
                className={className}
                {...props}
                onDrop={(acceptedFiles, rejectedFiles, e) => {
                    if(acceptedFiles.length === 0){
                        return; // no valid dropped files
                    }

                    this.handleDrop();

                    input.onChange(acceptedFiles);

                    if (filePickerOnDrop) {
                        filePickerOnDrop(acceptedFiles, rejectedFiles, e);
                    }
                }}

                onDragEnter={(event) => {

                    const dt = event.dataTransfer;
                    if (dt.types &&
                        ( dt.types.indexOf ? dt.types.indexOf( "Files" ) !== -1 : dt.types.contains( "Files" ) )
                    ) {
                        // Drag item is a valid drop file item

                        this.handleDragEnter();
                    }
                }}

                onDragLeave={this.handleDragLeave}
            />
        );
    }
}

export default FilePicker;