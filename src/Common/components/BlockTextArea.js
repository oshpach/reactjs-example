import React from 'react';

const BlockTextArea = (field) => {

    const hasError = field.meta.error && field.meta.touched;
    const hasSuccess = !field.meta.error && field.meta.touched;
    const needInputGroup = field.addonBefore || field.addonAfter;

    return (
        <div className={(hasError ? "has-error" : "") + (hasSuccess ? " has-success" : "") + " form-group"}>
            {field.label && <label className="control-label">{field.label}</label>}

            <div className={needInputGroup ? "input-group" : null}>
                {field.addonBefore && <div className="input-group-addon">{field.addonBefore}</div>}
                <textarea className="form-control input-block-level input-lg" rows={field.rows}
                          placeholder={field.placeholder} {...field.input}/>
                {field.addonAfter && <div className="input-group-addon">{field.addonAfter}</div>}
            </div>

            {hasError && <span className="help-block text-left">{field.meta.error}</span>}
        </div>
    )
};

export default BlockTextArea