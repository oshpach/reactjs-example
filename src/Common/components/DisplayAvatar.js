import React from 'react'
import {StyleSheet, css} from 'aphrodite/no-important'
import  {Tooltip, OverlayTrigger} from 'react-bootstrap'
import '../fonts/dashboard-icons/style.css'
import defaultAvatar from '../../Common/createTribute/images/default-avatar.png'

import ImageLoader from './ImageLoader'



const DisplayAvatar = props => {
    const src = (props.user && (props.user.avatarCropUri || props.user.avatarUri));
    const size = props.size || 45;
    const tooltipText = props.user && `${props.user.firstName} ${props.user.lastName}`.trim();

    const avatarTootip = <Tooltip id=''>{tooltipText}</Tooltip>;

    const styles = StyleSheet.create({
        avatar: {
            height: size,
            width: size,
            borderRadius: '50%',
            boxShadow: '1px 1px 3px 1px rgba(0,0,0,.3)',
        },
    });

    return (
        <div className={props.displayWrapperClass}>
            <OverlayTrigger overlay={avatarTootip}>
                <div>  {/* this div is need for the overlay trigger to work */}
                    <ImageLoader
                        className={css(styles.avatar) + ` ${props.className}`}
                        src={src}
                        previewSrc={defaultAvatar}
                    />
                </div>
            </OverlayTrigger>
        </div>
    )
};

export default DisplayAvatar;
