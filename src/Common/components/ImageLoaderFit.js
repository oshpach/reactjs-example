import React from 'react';

class ImageLoaderFit extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            imageLoaded: false,
            maxRetries: 3,
            retries: 0,
            delay: 3000,
            imageSrc: props.src ? "http://liveonmediaupload.s3.amazonaws.com/" + props.src : null,
            reloadImageSrc: props.src ? "http://liveonmediaupload.s3.amazonaws.com/" + props.src : null,  // used to reload image if error
            supportsFit: window.CSS &&
            CSS.supports &&
            CSS.supports('object-fit', 'cover') &&
            CSS.supports('object-position', '0 0')
        };

        // Bind this to functions
        this.handleOnLoad = this.handleOnLoad.bind(this);
        this.handleOnError = this.handleOnError.bind(this);
    }

    componentWillMount() {
        this.setState({
            handleOnError: this.handleOnError
        });
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            imageSrc: nextProps.src ? "http://liveonmediaupload.s3.amazonaws.com/" + nextProps.src : null,
            reloadImageSrc: nextProps.src ? "http://liveonmediaupload.s3.amazonaws.com/" + nextProps.src : null, // used to reload image if error,
            imageLoaded: !nextProps.src && nextProps.previewSrc ? false : this.state.imageLoaded
        });
    }

    handleOnLoad() {
        this.setState({imageLoaded: true});
    }

    handleOnError() {
        if (this.state.retries >= this.state.maxRetries) {
            this.setState({handleOnError: null});  // remove onError
            return;
        }

        this.setState({retries: this.state.retries + 1});

        // wait and try to reload image.
        const timer = setTimeout(() => {
            this.setState({imageSrc: this.state.reloadImageSrc + `?${this.state.retries}`}); // need to change url to force reload of image
        }, this.state.delay);

        this.setState({timer: timer})
    }

    componentWillUnmount() {
        clearTimeout(this.state.timer);
    }

    render() {
        const {className, style, previewSrc, containerClassName, containerStyle} = this.props;

        const defaultContainerStyle = {display: "inline-block", height: this.state.supportsFit ? null : '480px'};

        const fit = {objectFit: 'cover'};

        const imageTag =
            this.state.supportsFit ?

            <img
             ref={(image) => this.loadImage = image}
             className={className}
             style={{...style, ...fit}}
             crossOrigin={this.props.crossOrigin || ''}
             src={this.state.imageSrc}
             alt=''
             onLoad={this.handleOnLoad}
             onError={this.handleOnError}
             />

              :

            <span
                style={{
                    backgroundImage: `url(${this.state.imageSrc})`,
                    backgroundSize: 'cover',
                    backgroundPosition: '50% 50%',
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    height: '100%',
                    width: '100%',
                }}
            />;

        const previewTag = <img
            className={className}
            style={style}
            src={previewSrc}
            alt=''
        />;


        return (
            <div className={containerClassName} style={containerStyle || defaultContainerStyle}>
                { this.state.supportsFit ?
                    <span>
                        {this.state.imageSrc && <span style={{display: this.state.imageLoaded ? null : 'none'}}>{imageTag}</span>}
                        {previewSrc && <span style={{display: !this.state.imageLoaded ? null : 'none'}}>{previewTag}</span>}
                    </span>
                :
                    imageTag
                }
             </div>
        )
    }
}

export default ImageLoaderFit;