import React from 'react';
import {Field} from 'redux-form'
import FilePicker from './FilePicker';
import {css} from 'aphrodite';
import ImageLoader from './ImageLoader'
import defaultAvatar from '../../Common/createTribute/images/default-avatar.png';

/*
styles={
    filePicker: {
        Base styles for control
            ':hover': {
                styles for when mouse over
            }
    },

    filePickerDragOver: {
        styles append to Base styles for when a drag over enters
    }
}
*/

class ReduxFormImagePicker extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            imageDropped: false,
            imageUrl: this.props.imageSrc,
            previewUrl: defaultAvatar,
        };

        // Bind this to functions
        this.handleDrop = this.handleDrop.bind(this);
        this.getImageUrl = this.getImageUrl.bind(this);
    }

    handleDrop(acceptedFiles) {
        console.log(acceptedFiles);

        this.setState({
            imageDropped: true,
            previewUrl: acceptedFiles[0].preview || defaultAvatar
        })
    }

    getImageUrl() {
        return this.state.imageDropped ? null : this.state.imageUrl;
    }

    render() {
         const {
             styles,
             imageSrc,      // eslint-disable-line no-unused-vars
             previewSrc,    // eslint-disable-line no-unused-vars
             accept,
             ...props
         } = this.props;

        return (
            <div className={css(styles.filePickerBox)}>
                <ImageLoader className={css(styles.filePickerImage)}
                             src={this.getImageUrl()}
                             previewSrc={this.state.previewUrl}/>
                <Field
                    component={FilePicker}
                    styles={styles}
                    filePickerOnDrop={this.handleDrop}
                    accept={accept ? accept : 'image/gif, image/png, image/jpeg'}
                    {...props}
                />
            </div>
        );
    }
}

export default ReduxFormImagePicker;