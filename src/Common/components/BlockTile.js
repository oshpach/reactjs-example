import React from 'react'
import {StyleSheet, css} from 'aphrodite/no-important'
import classnames from 'classnames'

const styles = StyleSheet.create({
    tile: {
        backgroundColor: '#FFFFFF',
        width: '100%',
        maxWidth: 600,
        margin: '15px auto',
        padding: '30px 15px',
        border: '#F0F0F0 solid 1px',
        boxShadow: '1px 1px 3px 1px rgba(0,0,0,.1)',

        '@media (max-width: 650px)': {
            padding: '20px 5px'
        }
    }
});

const BlockTile = props => {
        return (
            <div className={classnames(css(styles.tile), props.className)}>
                {props.children}
            </div>
        );
};

export default BlockTile;
