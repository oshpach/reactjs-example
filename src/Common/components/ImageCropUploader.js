import React from 'react'
import {connect} from 'react-redux'
import ConfirmDeleteModal from '../../Tributes/components/ConfirmDeleteModal'
import {StyleSheet, css} from 'aphrodite/no-important'
import classnames from 'classnames'

import {ImageLoader, ImageCrop, ReduxFormDropzone} from './index'

import defaultAvatar from '../createTribute/images/default-avatar.png'

const styles = StyleSheet.create({

    editImageButton: {
        marginRight: 15,
        cursor: 'pointer'
    },

    deleteImageButton: {
        cursor: 'pointer'
    },

    editButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 20,
        right: 0,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: 10,
    },

    dropzoneBox: {
        position: 'relative',
        height: 200,
        width: 200,
        margin: '0 auto',
        cursor: 'pointer',

        ':hover .editButtons': {
            display: 'block'
        }
    },

    dropzoneImage: {
        height: 200,
        width: 200,
        borderRadius: '50%'
    },

    dropzone: {
        position: 'absolute',
        top: 0,
        left: 0,
        width: 200,
        height: 200,
        borderWidth: 2,
        borderColor: 'rgb(102, 102, 102)',
        borderStyle: 'solid',
        borderRadius: '50%'
    },

    cropContainer: {
        margin: '0 auto'
    }
});


class ImageCropUploader extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            imageDropped: false,
            showCropEditor: false,
            showConfirmModal: false
        };

        // Bind this to functions
        this.handleDrop = this.handleDrop.bind(this);
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleImageCancelCrop = this.handleImageCancelCrop.bind(this);
        this.handleEditImageClick = this.handleEditImageClick.bind(this);
        this.handleDeleteImageClick = this.handleDeleteImageClick.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        // Set State
        this.setState({
            avatarUri: nextProps.imageUrl,
            avatarCropUri: nextProps.CropUrl
        });
    }

    handleDrop(acceptedFiles) {
        this.props.onDrop(acceptedFiles[0]);

        this.setState({
            imageDropped: true,
            previewUrl: acceptedFiles[0].preview || defaultAvatar
        })
    }

    handleEditImageClick() {
        this.setState({
            showCropEditor: true
        })
    }

    handleDeleteImageClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.onDelete();

        this.setState({
            avatarUri: null,
            avatarCropUri: null,
            previewUrl: defaultAvatar
        })
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    handleCropImage(crop, scale) {

        this.props.onCrop(crop, scale);

        this.setState({
            showCropEditor: false
        })
    }

    handleImageCancelCrop() {
        this.setState({
            showCropEditor: false
        })
    }

    submit(model) {
        return this.props.onSubmit(model);
    }

    render() {
        const propStyles = this.props.styles || {};


        return (
            <div className={this.props.containerStyle}>
                { (this.state.showCropEditor && this.props.imageUri) ?
                    (
                        <ImageCrop
                            crossOrigin={this.props.crossOrigin || 'anonymous'}
                            src={this.props.previewAvatarBlob || `http://liveonmediaupload.s3.amazonaws.com/${this.props.imageUri}`}
                            height={this.props.height || 200}
                            width={this.props.width || 200}
                            border={this.props.border || 0}
                            borderRadius={this.props.borderRadius || 0}
                            onCrop={this.handleCropImage}
                            onCancel={this.handleImageCancelCrop}
                            containerStyle={[styles.cropContainer, propStyles.containerStyle]}
                            cropButtonStyle={this.props.cropButtonStyle || {bsSize: 'small'}}
                            cancelButtonStyle={this.props.cancelButtonStyle || {bsSize: 'small'}}
                            createTribute={true} // set true in order to add styles just to create tribute page
                        />
                    )
                    :
                    (
                        <div className={css(styles.dropzoneBox, propStyles.dropzoneBox)}>
                            <ImageLoader
                                className={css(styles.dropzoneImage, propStyles.dropzoneImage)}
                                src={this.props.cropUri || this.props.imageUri}
                                previewAvatarBlob={this.props.previewAvatarBlob}
                                previewSrc={this.props.previewUrl}
                            />

                            <ReduxFormDropzone
                                className={css(styles.dropzone, propStyles.dropzone)}
                                multiple={false}
                                dropzoneOnDrop={this.handleDrop}
                            />

                            {(this.props.previewAvatarBlob || this.props.imageUri) &&
                                <div className={classnames(css(styles.editButtons, propStyles.editButtons), 'editButtons')}>
                                    {this.props.imageUri && <i className={classnames(css(styles.editImageButton, propStyles.editImageButton), "fa fa-pencil-square-o fa-lg")}
                                       onClick={this.handleEditImageClick}/>}
                                    <i className={classnames(css(styles.deleteImageButton, propStyles.deleteImageButton), "fa fa-trash-o fa-lg")}
                                       onClick={this.handleDeleteImageClick}/>
                                </div>
                            }
                        </div>
                    )
                }

                <ConfirmDeleteModal title="Delete Avatar" body="Confirm delete avatar?"
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled}
                                    show={this.state.showConfirmModal}/>
            </div>
        );
    }
}

export default connect()(ImageCropUploader);

