import React from 'react';
import Dropzone from 'react-dropzone';
import {css} from 'aphrodite';

class ReduxFormDropzone extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            isDragOver: false
        };

        // Bind this to functions
        this.handleDrop = this.handleDrop.bind(this);
        this.handleDragEnter = this.handleDragEnter.bind(this);
        this.handleDragLeave = this.handleDragLeave.bind(this);
    }

    handleDragEnter() {
        this.setState({isDragOver: true})
    }

    handleDragLeave() {
        this.setState({isDragOver: false})
    }

    handleDrop() {
        this.setState({isDragOver: false})
    }

    render() {
        const {
            //input,          // need to add this to remove it from the props below
            meta,           // need to add this to remove it from the props below
            dropzoneOnDrop,
            styles,
            ...props
        } = this.props;

        let className;
        if(styles && styles.dropzone){
            className = css(this.state.isDragOver ? [styles.dropzone, styles.dropzoneDragOver] : styles.dropzone);
        }

        return (
            <Dropzone
                className={className}
                {...props}
                onDrop={(acceptedFiles, rejectedFiles, e) => {
                    if(acceptedFiles.length === 0){
                        return; // no valid dropped files
                    }

                    this.handleDrop();

                    //input.onChange(acceptedFiles);

                    if (dropzoneOnDrop) {
                        dropzoneOnDrop(acceptedFiles, rejectedFiles, e);
                    }
                }}

                onDragEnter={(event) => {

                    const dt = event.dataTransfer;
                    if (dt.types &&
                            ( dt.types.indexOf ? dt.types.indexOf( "Files" ) !== -1 : dt.types.contains( "Files" ) )
                        ) {
                        // Drag item is a valid drop file item

                        this.handleDragEnter();
                    }
                }}

                onDragLeave={this.handleDragLeave}
            />
        );
    }
}

export default ReduxFormDropzone;