import React from 'react';
import {FormGroup, Row, Col} from 'react-bootstrap'
import {CountryDropdown, RegionDropdown} from 'react-country-region-selector';

class BlockSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            country: this.props.input.value.country || '',
            region: this.props.input.value.region || ''
        };

    }

    selectCountry = (val) => {
        this.setState({country: val});

        this.props.input.onChange({country: val, region: this.state.region});

    };

    selectRegion = (val) => {
        this.setState({region: val});

        this.props.input.onChange({country: this.state.country, region: val});
    };


    render() {

        const {input: {value}, styleRegion} = this.props;

        return (
            <FormGroup controlId="formControlsSelect">
                <Row>
                    <Col md={7}>
                        <CountryDropdown
                            classes="form-control input-lg"
                            value={value.country}
                            onChange={this.selectCountry}
                        />
                    </Col>
                    <Col md={5} className={styleRegion}>
                        <RegionDropdown
                            classes="form-control input-lg"
                            country={value.country}
                            value={value.region}
                            onChange={this.selectRegion}
                        />
                    </Col>
                </Row>
            </FormGroup>
        )
    }

}

export default BlockSelect;