import React, {Component} from 'react';
import Truncate from 'react-truncate';

class ReadMoreText extends Component {
    constructor(...args) {
        super(...args);

        this.state = {readMore: true};

        this.toggleLines = this.toggleLines.bind(this);
    }

    toggleLines(event) {
        event.preventDefault();

        this.setState({
            readMore: !this.state.readMore
        });
    }

    render() {
        let {children, moreText, lessText, lines, styles} = this.props;

        const divStyle = {
            width: '100%',
            wordWrap: 'break-word'
        };

        return (
            <div style={{...divStyle, ...styles.base}}>
                <Truncate
                    lines={this.state.readMore && lines}
                    ellipsis={(
                        <span>... <a style={styles.moreText} onClick={this.toggleLines}>{moreText}</a></span>
                    )}
                >
                    {children}
                </Truncate>
                {lessText && !this.state.readMore && <span>&nbsp;<a style={styles.lessText || styles.moreText} onClick={this.toggleLines}>{lessText}</a></span>}
            </div>
        );
    }
}

ReadMoreText.defaultProps = {
    lines: 3,
    moreText: 'more',
    lessText: 'less'
};

export default ReadMoreText;