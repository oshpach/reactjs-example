import React from 'react';
// import {change} from 'redux-form'
import {FormGroup, FormControl} from 'react-bootstrap'

class BlockSelectSmoking extends React.Component{

    render(){
        return (
            <FormGroup controlId="formControlsSelect">
                {/*<ControlLabel>Multiple select</ControlLabel>*/}
                <FormControl componentClass="select">
                    <option value="Bad">Bad</option>
                    <option value="Neutral">Neutral</option>
                    <option value="Good">Good</option>
                </FormControl>
            </FormGroup>
        )
    }
}

export default BlockSelectSmoking;