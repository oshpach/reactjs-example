import React from 'react';

const BlockInput = (field) => {

    const hasError = field.meta.error && field.meta.touched;
    const hasSuccess = !field.meta.error && field.meta.touched;
    const needInputGroup = field.addonBefore || field.addonAfter;

    function makeArray(errors){
        return Array.isArray(errors) ? errors : [errors];
    }

    return (
        <div
            className={(hasError ? "has-error" : "") + (hasSuccess ? " has-success" : "") + (field.feedback ? " has-feedback" : "") + " form-group"}>
            {field.label && <label className="control-label">{field.label}</label> }

            <div className={needInputGroup ? "input-group" : null}>

                {field.addonBefore && <div className="input-group-addon">{field.addonBefore}</div>}
                <input
                    className="form-control input-block-level input-lg"
                    type={field.type}
                    placeholder={field.placeholder}
                    onClick={field.onClick}
                    {...field.input}
                    value={field.input.value || ''}  // this fixes a bug where a null initial value will throw an warning in debug mode
                />

                {field.addonAfter && <div className="input-group-addon" onClick={field.addonAfterClick}>{field.addonAfter}</div>}
                {field.feedback && <span className={"form-control-feedback " + field.feedback}></span>}
            </div>

            {hasError && <div className="help-block text-left">
                {makeArray(field.meta.error).map((error, index) => {
                    return <div key={index}>{error}</div>;
                })}
            </div>}
        </div>
    )
};

export default BlockInput