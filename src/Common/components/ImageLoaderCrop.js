import React from 'react'
import {connect} from 'react-redux'
import ConfirmDeleteModal from '../../Tributes/components/ConfirmDeleteModal'
import {StyleSheet, css} from 'aphrodite/no-important'
import classnames from 'classnames'
import {ImageLoaderFit as ImageLoader, ImageCrop} from './index'

const styles = StyleSheet.create({

    editImageButton: {
        marginRight: 15,
        cursor: 'pointer'
    },

    deleteImageButton: {
        cursor: 'pointer'
    },

    editButtons: {
        display: 'none',
        borderRadius: 5,
        position: 'absolute',
        top: 20,
        right: 0,
        backgroundColor: '#FAFAFA',
        border: '#E0E0E0 solid 2px',
        padding: 10,
    },

    imageContainer: {
        //position: 'relative',
        height: 200,
        width: 200,
        margin: '0 auto',
        cursor: 'pointer',

        ':hover .editButtons': {
            display: 'block'
        }
    },

    imageLoaderClassName: {
        height: 200,
        width: 200,
        borderRadius: 0
    },

    cropContainer: {
        margin: '0 auto'
    }
});

class ImageCropUploader extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            showCropEditor: false,
            showConfirmModal: false
        };

        // Bind this to functions
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleImageCancelCrop = this.handleImageCancelCrop.bind(this);
        this.handleEditImageClick = this.handleEditImageClick.bind(this);
        this.handleDeleteImageClick = this.handleDeleteImageClick.bind(this);
        this.handleDeleteConfirmed = this.handleDeleteConfirmed.bind(this);
        this.handleDeleteCanceled = this.handleDeleteCanceled.bind(this);
    }

    handleEditImageClick() {
        this.setState({
            showCropEditor: true
        })
    }

    handleDeleteImageClick() {
        this.setState({
            showConfirmModal: true
        })
    }

    handleDeleteConfirmed() {
        this.setState({
            showConfirmModal: false
        });

        this.props.onDelete(this.props.albumMediaId);
    }

    handleDeleteCanceled() {
        this.setState({
            showConfirmModal: false
        });
    }

    handleCropImage(crop, scale) {

        this.props.onCrop(crop, scale);

        this.setState({
            showCropEditor: false
        })
    }

    handleImageCancelCrop() {
        this.setState({
            showCropEditor: false
        })
    }

    submit(model) {
        return this.props.onSubmit(model);
    }

    render() {
        const propStyles = this.props.styles || {};

        return (
            <div>
                { (this.state.showCropEditor && this.props.uri) ?
                    (
                        <ImageCrop
                            crossOrigin={this.props.crossOrigin || 'anonymous'}
                            src={`http://liveonmediaupload.s3.amazonaws.com/${this.props.uri}`}
                            height={this.props.height || 200}
                            width={this.props.width || 200}
                            border={this.props.border || 0}
                            borderRadius={this.props.borderRadius || 0}
                            onCrop={this.handleCropImage}
                            onCancel={this.handleImageCancelCrop}
                            containerStyle={[styles.cropContainer, this.props.containerStyle]}
                            cropButtonStyle={this.props.cropButtonStyle || {bsSize: 'small'}}
                            cancelButtonStyle={this.props.cancelButtonStyle || {bsSize: 'small'}}
                        />
                    )
                    :
                    (
                        <div className={css(styles.imageContainer, this.props.imageContainerClassName)}>
                            <ImageLoader
                                className={css(styles.imageLoaderClassName, this.props.imageLoaderClassName)}
                                src={this.props.cropUri || this.props.uri}
                                previewSrc={this.props.previewUrl}
                            />

                            {this.props.uri &&
                            <div className={classnames(css(styles.editButtons, propStyles.editButtons), 'editButtons')}>
                                <i className={classnames(css(styles.editImageButton, propStyles.editImageButton), "fa fa-pencil-square-o fa-lg")}
                                   onClick={this.handleEditImageClick}/>
                                <i className={classnames(css(styles.deleteImageButton, propStyles.deleteImageButton), "fa fa-trash-o fa-lg")}
                                   onClick={this.handleDeleteImageClick}/>
                            </div>
                            }
                        </div>
                    )
                }

                <ConfirmDeleteModal title="Delete Avatar" body="Confirm delete avatar?"
                                    onDelete={this.handleDeleteConfirmed} onCancel={this.handleDeleteCanceled}
                                    show={this.state.showConfirmModal}/>
            </div>
        );
    }
}

export default connect()(ImageCropUploader);

