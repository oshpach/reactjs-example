import React from 'react'
import {connect} from 'react-redux'
import {Button} from 'react-bootstrap'
import AvatarEditor from 'react-avatar-editor'
import Slider from 'rc-slider'
import {StyleSheet, css} from 'aphrodite/no-important'
import './imageCrop.css'

require('./rc-slider.css');

/*
 props:
 {
 src             src of image to be cropped
 height:         int - height of the image crop
 width:          int - width of the image crop
 sliderPadding   int - padding for the slider sides
 onCrop:         function(recCrop, scale) - called when crop is press
 onCancel:       function() - called when cancel is press
 }
 */

const styles = StyleSheet.create({
    slider: {
        margin: '0 auto 30px auto',
        // width: '50%'
    },

    cropButton: {
        float: 'left'
    },

    cancelButton: {
        float: 'right'
    },
    sliderToCreate: {
        width: '50%'
    },
    buttonsWrap: {
        width: '50%',
        margin: '0 auto',
        paddingBottom: '20px'
    }
});

export class ImageCrop extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            scale: 1,
            height: this.props.height || 500,
            width: this.props.width || 500,
            sliderPadding: this.props.sliderPadding || 70
        };

        this.handleScaleOnChange = this.handleScaleOnChange.bind(this);
        this.handleCrop = this.handleCrop.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleScaleOnChange(value) {
        this.setState({
            scale: value
        })
    }

    handleCrop() {
        const crop = this.editor.getCroppingRect();
        this.props.onCrop(crop, this.state.scale);
    }

    handleCancel() {
        this.props.onCancel();
    }

    render() {
        const propStyles = this.props.styles || {};

        const stateStyles = StyleSheet.create({
            sliderBox: {
                position: 'absolute',
                left: '0',
                right: '0',
                zIndex: 998,
                padding: '15px 36px',
                background: 'rgba(0, 0, 0, 0.9)',
                marginBottom: '4px'
            },
            //
            container: {
                position: 'relative',
                fontSize: '0',
                lineHeight: 'unset'
            }
        });

        return (
            <div className={css(stateStyles.container, this.props.containerStyle)}>
                <AvatarEditor
                    ref={(editor) => this.editor = editor}
                    crossOrigin={this.props.crossOrigin || 'anonymous'}
                    image={this.props.src}
                    width={this.state.height}
                    height={this.state.height}
                    border={this.props.border || 0}
                    borderRadius={this.props.borderRadius || 0}
                    color={[255, 255, 255, 0.8]} // RGBA
                    scale={this.state.scale}
                    style={{backgroundColor: '#404040', 'cursor': 'move', width: (this.props.usage === 'style1') && '100%'}}
                />

                <div className={(this.props.usage === 'style1') && css(stateStyles.sliderBox)}>
                    <Slider className={css(styles.slider) + ` ${this.props.createTribute && css(styles.sliderToCreate)}`}
                            min={0.1}
                            max={2}
                            defaultValue={this.state.scale}
                            marks={{'0.1': 'zoom out', 1: '', 2: 'zoom in'}}
                            step={0.01}
                            onChange={this.handleScaleOnChange}
                    />

                    <div className={this.props.createTribute && css(styles.buttonsWrap)}>
                        <Button
                            className={css(styles.cropButton)}
                            onClick={this.handleCrop}
                            bsStyle={(propStyles.cropButtonStyle && propStyles.cropButtonStyle.bsStyle) || 'primary'}
                            bsSize={(propStyles.cropButtonStyle && propStyles.cropButtonStyle.bsSize) || 'sm'}
                        >
                            Crop
                        </Button>

                        <Button
                            className={css(styles.cancelButton)}
                            onClick={this.handleCancel}
                            bsStyle={(propStyles.cropButtonStyle && propStyles.cropButtonStyle.bsStyle) || 'default'}
                            bsSize={(propStyles.cropButtonStyle && propStyles.cropButtonStyle.bsSize) || 'sm'}
                        >
                            Cancel
                        </Button>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(ImageCrop);
