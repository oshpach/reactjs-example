import moment from 'moment'

export default value => {
    const dateFormats = [moment.ISO_8601, "M/D/YYYY", "M-D-YYYY"];

    return moment(value, dateFormats, true).isValid()
}
