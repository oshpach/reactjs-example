import defaultAvatar from './default-avatar.png'
import defaultPersona from './personas.png'
import logo from './logo.svg'
import menuIcon from './menuicon.png'
import headerImage from './header-image.png'
import {albumLogo, PhotoCamera} from './albums'
import createTribute from './createTribute.svg'
import addFile from './addFile.svg'
import EnterMess from './EnterMess.svg'
import photoMess from './photoMess.svg'

export {defaultAvatar, defaultPersona, menuIcon, logo, headerImage, albumLogo, PhotoCamera, createTribute,photoMess, addFile, EnterMess}