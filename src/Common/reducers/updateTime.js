const initialState = new Date(0);  // epox date

export default function updateTime({update, clear}) {

    return function (state = initialState, action) {
        switch (action.type) {
            case update:
                return new Date();

            case clear:
                return initialState;

            default:
                return state;
        }
    }
}
