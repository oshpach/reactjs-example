export default function list(type, initialState = []) {

    const key = type.key || 'id';

    return function ({add, remove, clear}) {

        return function (state = initialState, action) {
            switch (action.type) {
                case add: {
                    let arr = [...state];

                    action[type.value].forEach(value => {
                        arr.push(value[key]);
                    });

                    return arr;
                }

                case remove: {
                    return [...state].splice(action.index, 1);
                }

                case clear: {
                    return [];
                }

                default:
                    return state;
            }
        }
    }
}
