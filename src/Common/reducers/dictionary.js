export default function dictionary(type, initialState = {}) {

    const key = type.key || 'id';

    return function (actionTypes) {

        const add = actionTypes.add || actionTypes.ADD;
        const remove = actionTypes.remove || actionTypes.REMOVE;
        const clear = actionTypes.clear || actionTypes.CLEAR;

        return function (state = initialState, action) {
            switch (action.type) {
                case add: {
                    let obj = {...state};

                    action[type.value].forEach(value => {
                        obj[value[key]] = value;
                    });

                    return {
                        ...state,
                        ...obj
                    };
                }

                case remove: {
                    // eslint-disable-next-line no-unused-vars
                    const {[action[key]]: remove, ...rest} = state;

                    return {...rest};
                }

                case clear: {
                    return initialState;
                }

                default:
                    return state;
            }
        }
    }
}
