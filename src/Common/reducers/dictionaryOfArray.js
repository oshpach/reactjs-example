import {List} from 'immutable'

export default function dictionaryOfArray(type, initialState = {}) {

    const key = type.key || 'id';
    const entityKey = type.entityKey || null;

    return function (actionTypes) {

        const add = actionTypes.add || actionTypes.ADD;
        const remove = actionTypes.remove || actionTypes.REMOVE;

        return function (state = initialState, action) {
            switch (action.type) {
                case add: {
                    let obj = {...state};

                    action[type.value].forEach(value => {
                        if(obj[value[key]]) {

                            if(!entityKey){
                                obj[value[key]] = [...obj[value[key]], value];
                            } else {
                                let list = List(obj[value[key]]);
                                const index = list.findIndex((entity) => {
                                    return entity[entityKey] === value[entityKey]
                                });
                                if (index > -1) {
                                    obj[value[key]] = list.set(index, value);
                                } else {
                                    obj[value[key]] = [...obj[value[key]], value];
                                }
                            }
                        } else {
                            obj[value[key]] = [value];
                        }
                    });

                    return obj;
                }

                case remove: {
                    // eslint-disable-next-line no-unused-vars
                    const {[action.remove]: remove, ...rest} = state;

                    return {...rest};
                }

                default:
                    return state;
            }
        }
    }
}
