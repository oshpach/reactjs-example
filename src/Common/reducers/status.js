const initState = {
    value: null,
    error: null
};

export default function(actionTypes, initialState = initState) {

    const requesting = actionTypes.requesting || actionTypes.REQUESTING;
    const success = actionTypes.success || actionTypes.SUCCESS;
    const error = actionTypes.error || actionTypes.ERROR;

    return function (state = initialState, action) {
        switch (action.type) {

            case requesting:
                return {
                    ...state,
                    value: requesting,
                    error: null
                };

            case success:
                return {
                    ...state,
                    value: success,
                    error: null
                };

            case error:
                // eslint-disable-next-line no-unused-vars
                const {type, ...rest} = action;
                return {
                    ...state,
                    value: error,
                    ...rest
                };

            default:
                return state;
        }
    }
}