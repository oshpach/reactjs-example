import moment from 'moment'

export default (value, previousValue) => {
    if(!value){
        return value
    }

    if (!previousValue || value.length < previousValue.length) {
        // typing backwards
        return value
    }

    const dateFormats = [moment.ISO_8601, "M/D/YYYY", "M-D-YYYY"];
    return moment(value, dateFormats, true).isValid() ? moment(value, dateFormats, true).format('l') : value;
}
