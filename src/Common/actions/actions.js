import axios from "axios"
import {authHeaders} from '../../utils'

export function actionCreator(actionType, data = {}) {
    return {
        type: actionType,
        ...data
    };
}

function dataAction(method, url, params = null, headers = {}, authHeader = true) {


    const config = {
        url,
        method,
        params: method === 'get' || method === 'delete' ? params : null,
        data: method === 'put' || method === 'post' || method === 'patch' ? params : null
    };

    return authHeaders()
        .then((data) => {
            config.headers = authHeader ? {...data, ...headers} : headers;

            return axios.request(config)
                .then(response => {
                    return Promise.resolve(response.data);
                })
                .catch((error) => {
                    return Promise.reject(error);
                });
        });

}

export function getDataAction(url, params = null, headers = {}, authHeader = true) {
    return dataAction('get', url, params, headers, authHeader);
}

export function postDataAction(url, params = null, headers = {}, authHeader = true) {
    return dataAction('post', url, params, headers, authHeader);
}

export function putDataAction(url, params = null, headers = {}, authHeader = true) {
    return dataAction('put', url, params, headers, authHeader);
}

export function deleteDataAction(url, params = null, headers = {}, authHeader = true) {
    return dataAction('delete', url, params, headers, authHeader);
}
export function patchDataAction(url, params = null, headers = {}, authHeader = true) {
    return dataAction('patch', url, params, headers, authHeader);
}