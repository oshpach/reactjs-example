export default (error) => {
    return ((error && error.response && error.response.data)
    || (error && error.message && {_error: error.message})
    || {_error: "Unexpected error please try again"});
}
