const styles = {

    text: {
        fontFamily: 'Railway'
    },

    textDark: {
        fontFamily: 'Railway',
        color: 'black',
    },

    textLight: {
        fontFamily: 'Railway',
        color: '#cccccc',
    },

    textBlue: {
        fontFamily: 'Railway',
        color: '#3498db',
    }
};

export default styles;