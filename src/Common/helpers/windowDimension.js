export default function windowDimension() {
    const w = window;
    const d = document;
    const body = d.getElementsByTagName('body')[0];

    const width = w.innerWidth || d.documentElement.clientWidth || body.clientWidth;
    const height = w.innerHeight || d.documentElement.clientHeight || body.clientHeight;

    return {width, height};
}