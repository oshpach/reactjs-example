import React from 'react';
import { Route, IndexRoute } from 'react-router';

import { requireAuth } from './utils';

import Templates from './Templates'
import Home from './Home';

import Tributes from './Tributes';
import Account from './Account';

import NotFound from './NotFound';

import VideoExample from './Tributes/VideoExample'
import { getTribute } from "./Tributes/actions/tributes";


function onEnterGetTributeByUri(store) {
    return function (nextState, replace, callback) {
        let state = store.getState();
        console.log(state);
        store.dispatch(getTribute(nextState.params.tributeUri))
            .catch((err) => replace('/tributes'))
            .then(() => callback())  // acts like a finally
    }
}

const routes = (store) => {
    return (
        <Route>
            <Route path="/" name="Home">
                <Route component={Templates.components.HomepageLayout}>
                    <IndexRoute component={Home.components.Homepage} />
                    <Route path="/index" name="Home" component={Home.components.Homepage} />
                    <Route path="/index.html" component={Home.components.Homepage} />

                    <Route path="/account/verify/:username/:token" component={Account.components.VerifyAccount} />
                    <Route path="/account" component={Account.components.AccountSettings} />
                    <Route path="/userAccount" components={Account.components.AccountPage}/>
                    <Route path="/price" name="Price" components={Tributes.components.Pricing}/>

                    <Route path="/Tributes" component={Tributes.components.Tributes} />
                    <Route path="/album/:tributeUri/:albumUri" component={Tributes.components.Album} />

                    <Route onEnter={onEnterGetTributeByUri(store)} path="/Tribute/:tributeUri" component={Tributes.components.Tribute} />

                    <Route path="/VideoExample" component={VideoExample} />
                </Route>

                <Route onEnter={requireAuth(store)}>
                    <Route component={Templates.components.MainLayout}>
                        <Route path="/dashboard" name="Dashboard" component={Tributes.components.MyTributes} />

                        <Route path="/createTribute" name="Create tribute" component={Tributes.components.CreateTribute} />
                        <Route path="/editTribute" name="Edit tribute" component={Tributes.components.EditTribute} />

                            <Route path="/mytributes" name="My Tributes" component={Tributes.components.MyTributes}/>
                            <Route path="/friends" name="Friends" component={Account.components.Friends}/>
                            <Route path="/accountsettings" name="Account Settings" component={Account.components.AccountSettings}/>
                            <Route path="/messages" name="Account Messages" component={Tributes.components.Messages}/>
                        <Route path="/notifications" name="Notifications" component={Tributes.components.Notifications}/>


                        <Route path="/createalbum" name="Create Album" component={Tributes.components.CreateAlbum} />
                        <Route path="/editalbum" name="Create Album" component={Tributes.components.EditAlbum} />
                        <Route path="/album" name="Album Page" component={Tributes.components.AlbumPage} />

                        <Route path="/tribute" component={Tributes.components.TributePage} />
                    </Route>
                </Route>
            </Route>

            <Route path="*" component={NotFound} />
        </Route>
    )
};

export default routes;

