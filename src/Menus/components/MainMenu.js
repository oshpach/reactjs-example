import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router';
import { Image } from 'react-bootstrap';
import { StyleSheet, css } from 'aphrodite/no-important'
import Account from '../../Account/index';
import {userAuthenticated} from '../../utils/index';
import { logo } from '../../Common/images/index'
import './MainMenu.css'

const { components: { Login, Register } } = Account;
    // actions: { logOut, showLoginModal, showRegistrationModal } } = Account;

const styles = StyleSheet.create({
    navigationMenuLink: {
        backgroundColor: 'transparent',
        color: 'white',
        border: 'none',
        fontWeight: 'bold',
        "@media screen and (max-width: 868px)": {
            fontSize: '14px'
        },
        "@media screen and (max-width: 768px)": {
            fontSize: '16px',

        }

    },
    logoNew: {
        "@media screen and (max-width: 768px)": {
            display: 'none !important'
        }
    },
    logoSmall: {
        textAlign: 'center',
        transition: 'all ease 0.3s',
        position: 'relative',
        "@media screen and (min-width: 767px)": {
            display: 'none'
        }
    }
});


class MainMenu extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            smallMenu: false,
            moveLogo: false
        };
        // Bind this to functions
        this.handleRegisterClick = this.handleRegisterClick.bind(this);
        this.getMenuItems = this.getMenuItems.bind(this);
        this.logOut = this.logOut.bind(this);
        this.linkTo = this.linkTo.bind(this);
        this.menuAnimation = this.menuAnimation.bind(this);
        this.moveLogo = this.moveLogo.bind(this);
    }

    componentDidMount(){
       this.menuAnimation();
    }

    menuAnimation(){
        const closeGallery = document.querySelector('#closeGallery');
        const gallery = document.querySelector('#hideScrollBody');
        const searchButton = document.querySelector('.search-button');
        const homeButton = document.querySelector('.home-button');
        const navigationBlock = document.querySelector('#nav');
        const logoWrapper = document.querySelector('#wrapper');
        const searchBlock = document.querySelector('.navigation-search');
        const navigationList = document.querySelector('#menu');
        const logo = document.querySelector('#logo');
        let values = [searchButton, homeButton, navigationBlock, searchBlock, navigationList, logo];
        let valuesLength = values.length;
        let top_Height;

        const home1 = document.querySelector('#menu-toggler');
        const menuLists = document.querySelector('.navigation-menu');
        const body = document.querySelector('body');

        const filterButton = document.querySelector('.filter-wrapper');
        const showFilterList = document.querySelectorAll('.showFilterList');
        const filterSection = document.querySelector('#filter');
        const marginSections = document.querySelectorAll('.filter-margin-animation');
        const LIST_LENGTH = showFilterList.length;


        if (filterButton) {
            filterButton.addEventListener("click", () => {
                for (let i = 0; i < LIST_LENGTH; i += 1) {
                    showFilterList[i].classList.toggle('openFilter');
                    marginSections[i].classList.toggle('openFilter');
                }
                filterSection.classList.toggle('openFilter');
            });
        }

        home1.addEventListener("click", () => {
            if (navigationBlock.classList.contains('active')) {
                navigationBlock.classList.remove('active');
                logoWrapper.classList.remove('active');
                navigationList.classList.remove('active');
            }
            if(navigationBlock.classList.contains('menuIsOpened')){
                searchButton.classList.remove('displayNone');
            } else {
                searchButton.classList.add('displayNone');
            }
            navigationBlock.classList.toggle('menuIsOpened');
            menuLists.classList.toggle('menuIsActive');
            body.classList.toggle('noScroll');
        });
        if (navigationBlock) {
            searchButton.addEventListener("click", () => {

                if (navigationBlock.classList.contains('active')) {
                    for (let i = 0; i < valuesLength; i += 1) {
                        values[i].classList.remove('active');
                        values[i].classList.add('searchIsActive');
                    }
                } else if (navigationBlock.classList.contains('searchIsActive') && (top_Height > 50)) {
                    for (let i = 0; i < valuesLength; i += 1) {
                        values[i].classList.remove('searchIsActive');
                        values[i].classList.add('active');
                    }

                } else {
                    for (let i = valuesLength - 1; i >= 0; i -= 1) {
                        values[i].classList.toggle('searchIsActive');
                    }
                }
            });
        }
        if (navigationBlock) {
            window.addEventListener('scroll', () => {

                if(!navigationBlock.classList.contains('menuIsOpened')){
                    top_Height = window.scrollY;
                    if (top_Height > 50) {


                        if (!navigationBlock.classList.contains('searchIsActive')) {
                            for (let i = 0; i < valuesLength; i += 1) {
                                values[i].classList.add('active');
                            }
                        }
                    }
                    if (top_Height <= 50) {
                        for (let i = 0; i < valuesLength; i += 1) {
                            values[i].classList.remove('active');
                        }
                    }
                }

            });
        }

        //hide scroll after clicking on image
        if(closeGallery){
            gallery.addEventListener("click", () => {
                body.classList.add('hideScroll');
            });

            closeGallery.addEventListener("click", () => {
                body.classList.remove('hideScroll');
            });
        }
    }

    handleRegisterClick() {
        alert('Click');
        this.props.showLoginModal();
    }

    logOut(){
        this.props.logOut()
            .then(() => {window.location.href = '/';});
    }

    linkTo(url, event){
        const navigationList = document.querySelector('#menu');
        const navigationBlock = document.querySelector('#nav');
        const body = document.querySelector('body');
        if(navigationBlock.classList.contains('menuIsOpened')){
            body.classList.toggle('noScroll');
            navigationBlock.classList.remove('menuIsOpened');
            navigationList.classList.remove('menuIsActive');
        }
        event.preventDefault();
        this.props.router.push({
            pathname: url
        });
    }


    getMenuItems = () => {
        let url = this.props.pathname !== '/';
        let menuItems = [];
            if(!userAuthenticated()){
                menuItems.push(<li className="navigation-menu-list" key={1}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/tributes", e)}} href="">TRIBUTES</a></li>);
                menuItems.push(<li className="navigation-menu-list" key={2}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/news", e)}} href="">NEWS</a></li>);
                menuItems.push(<li className={`navigation-menu-list ${css(styles.logoNew)}`}><Image onClick={(e) => {this.linkTo('/', e)}} src={logo} className={url ? `logo logo-small ${css(styles.navigationMenuLink)}` : `logo ${css(styles.navigationMenuLink)}`} id="logo" /></li>)
                menuItems.push(<li className="navigation-menu-list" key={3}><button className={css(styles.navigationMenuLink)} onClick={this.props.showLoginModal}>LOGIN</button></li>);
                menuItems.push(<li className="navigation-menu-list" key={4}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/about", e)}} href="" key={4}>ABOUT</a></li>);
            }

            if(userAuthenticated()){
                menuItems.push(<li className="navigation-menu-list" key={1}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/tributes", e)}} href="">TRIBUTES</a></li>);
                menuItems.push(<li className="navigation-menu-list" key={2}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/mytributes", e)}} href="">MY TRIBUTES</a></li>);
                menuItems.push(<li className={`navigation-menu-list ${css(styles.logoNew)}`}><Image onClick={(e) => {this.linkTo('/', e)}} src={logo} className={url ? `logo logo-small ${css(styles.navigationMenuLink)}` : `logo ${css(styles.navigationMenuLink)}`} id="logo" /></li>)
                menuItems.push(<li className="navigation-menu-list" key={3}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/messages", e)}} href="">MESSAGES</a></li>);
                menuItems.push(<li className="navigation-menu-list account-button" key={4}><a className={css(styles.navigationMenuLink)}><span className="account-button">ACCOUNT</span></a>
                    <ul className='DropDownMenu'>
                        <li className="DropDownMenu-item" key={5}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/friends", e)}} href="">FRIENDS</a></li>
                        <li className="DropDownMenu-item" key={6}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/notifications", e)}} href="">NOTIFICATIONS</a></li>
                        <li className="DropDownMenu-item" key={7}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/account", e)}} href="">MY PROFILE</a></li>
                        <li className="DropDownMenu-item" key={8}><a className={css(styles.navigationMenuLink)} onClick={(e) => {this.linkTo("/settings", e)}}  href="">SETTINGS</a></li>
                        <li className="DropDownMenu-item" key={9} onClick={this.logOut}>LOG OUT</li>
                    </ul>
                </li>);
        }
        return menuItems;

    }

    moveLogo(){
        this.setState({
            moveLogo: !this.state.moveLogo
        });
    }

    render() {
        const { pathname } = this.props.location;
        let url = pathname !== '/';
        return (
            <div>
                <Login /><Register />
                <div className={url ? 'navigation-small navigation' : 'navigation'} id="nav">
                    <div className="navigation-search">
                        <input type="text" style={{ color: 'white' }} />
                        <button><i className="fa fa-search" aria-hidden="true" /></button>
                    </div>
                    <span id="menu-toggler"></span>
                    <Link to="/">
                        <span className={url ? 'home-button-small home-button' : 'home-button'}>
                            <i className="fa fa-home" id="home-button" aria-hidden="true" />
                            <span className="first-line line"></span>
                            <span className="second-line line"></span>
                            <span className="third-line line"></span>
                        </span>
                    </Link>
                    <div className={css(styles.logoSmall)} style={{bottom: this.state.moveLogo ? -30 : 20}}>
                        <Image onClick={(e) => {this.linkTo('/', e)}} src={logo} className={url ? `logo logo-small ${css(styles.navigationMenuLink)}` : `logo ${css(styles.navigationMenuLink)}`} id="logo" />
                    </div>
                    <span onClick={this.moveLogo} className={url ? 'search-button search-button-small' : 'search-button'}><i className="fa fa-search" aria-hidden="true" /></span>
                    {/*<div onClick={(e) => {this.linkTo('/', e)}} className={url ? 'logoWrapper logoWrapper-small' : 'logoWrapper'} id="wrapper">*/}
                        {/*<Image src={logo} className={url ? 'logo logo-small' : 'logo'} id="logo" />*/}
                    {/*</div>*/}
                    <ul className={url ? "navigation-menu navigation-menu-small" : "navigation-menu"} id="menu">
                        {this.getMenuItems().map((item) => item)}
                    </ul>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    const { login } = state.account;
    const { register } = state.account;
    return {
        showLogin: login.showLoginModal,
        showRegister: register.showRegistrationModal
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        showLoginModal: () => {
            dispatch(Account.actions.account.showLoginModal())
        },
        showRegistrationModal: () => {
            dispatch(Account.actions.account.showRegistrationModal())
        },
        logOut: () => {
            return dispatch(Account.actions.account.logOut());
        }
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainMenu));