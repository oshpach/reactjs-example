import React, {Component} from 'react'
import {connect} from 'react-redux'

import MainMenu from '../../Menus/components/MainMenu'
import actions from '../../Account/actions'
import Footer from '../../Footers/Footer'


class homepageLayout extends Component {
    constructor(props) {
        super(props);

        this.props.getAccount();
    }

    render() {
        const {user} = this.props;
        return (
            <div>
                <MainMenu user={user}/>
                <div className="appContent">
                        {this.props.children}
                    <div style={{height: '3em'}}/>
                </div>
                <Footer bottom="-200px"/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {auth} = state;
    return {
        user: auth ? auth.user : null,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAccount: () => {dispatch(actions.account.getAccount())} // Get logged in user. the user can be logged in and refresh the page.
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(homepageLayout);
