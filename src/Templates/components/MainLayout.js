import React, {Component} from 'react'
import {connect} from 'react-redux'
import Footer from '../../Footers/Footer'
import MainMenu from '../../Menus/components/MainMenu'
import Account from '../../Account'


class mainLayout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            mainMenu: (this.props.route && this.props.route.mainMenu) || true,
            user: this.props.user
        };

        this.props.getAccount();
    }

    render() {
        return (
            <div>
                { this.state.mainMenu && <MainMenu user={this.state.user}/> }
                <div className="appContent">
                    <div className="container-fluid">

                        {this.props.children}

                    </div>
                    <div style={{height: '3em'}}/>
                </div>
                <Footer bottom="-200px"/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const {auth} = state;
    return {
        user: auth ? auth.user : null,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getAccount: () => {dispatch(Account.actions.account.getAccount())} // Get logged in user. the user can be logged in and refresh the page.
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(mainLayout);
