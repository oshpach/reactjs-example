import React, {Component} from 'react'
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import classnames from 'classnames'
import Sidebar from 'react-sidebar'
import Menus from '../../Menus'
const {components: {SidebarMenu}} = Menus;

const styles = StyleSheet.create({
    sidebarHandle: {
        marginTop: 25,
        marginLeft: 4
    }
});

const sidebarStyles = {
    root: {
        top: 51
    },

    sidebar: {
        width: 60,
        backgroundColor: '#3498DB',
        color: 'white'
    },

    overlay: {
        overflow: 'hidden'
    },

    content: {
        overflow: 'hidden'
    }
};

const sidebarHandleHideStyle = {
    display: 'none'
};

const sidebarHandleShowStyle = {
    display: 'block',
    position: 'fixed',
    top: 56,
    left: 0,
    right: 0,
    bottom: 0,
    width: 20,
    color: 'white',
    backgroundColor: '#3498DB',
    overflow: 'visible',
    cursor: 'pointer'
};

const mainContentStyle = {
    position: 'absolute',
    top: 0,
    left: 0, //sidebarHandleShowStyle.width + 1,
    right: 0,
    bottom: 0,
    marginLeft: sidebarHandleShowStyle.width + 1,
    overflowY: 'auto',
    overflowX: 'hidden',
    WebkitOverflowScrolling: 'touch'
};
class SidebarLayout extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sidebarOpen: false,
            sidebarDocked: false
        };

        this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    componentWillMount() {
        const mql = window.matchMedia(`(min-width: 768px)`);
        mql.addListener(this.mediaQueryChanged);
        this.setState({mql: mql, sidebarDocked: mql.matches});
    }

    componentWillUnmount() {
        this.state.mql.removeListener(this.mediaQueryChanged);
    }

    mediaQueryChanged() {
        this.setState({sidebarDocked: this.state.mql.matches});
    }

    onSetSidebarOpen(open) {
        this.setState({sidebarOpen: open});
    }

    render() {
        return (
            <Sidebar
                styles={sidebarStyles}
                sidebar={<SidebarMenu/>}
                open={this.state.sidebarOpen}
                docked={this.state.sidebarDocked}
                touch={false}
                onSetOpen={this.onSetSidebarOpen}>
                <div>
                    <div style={this.state.sidebarDocked || this.state.sidebarOpen ? sidebarHandleHideStyle : sidebarHandleShowStyle} onClick={() => this.onSetSidebarOpen(true)}>
                        <i className={classnames(css(styles.sidebarHandle), "fa fa-ellipsis-v fa-3x")} aria-hidden="true"/>
                    </div>
                    <div style={ mainContentStyle}>
                        {this.props.children}
                    </div>
                </div>
            </Sidebar>
        );
    }
}

export default connect()(SidebarLayout);
