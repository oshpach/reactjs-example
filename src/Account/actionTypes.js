export const REQUEST_BEGIN = '@@liveon/account/REQUEST_BEGIN';
export const REQUEST_END = '@@liveon/account/REQUEST_END';

export const GET_ACCOUNT_REQUEST = '@@liveon/account/GET_ACCOUNT_REQUEST';
export const GET_ACCOUNT_SUCCESS = '@@liveon/account/GET_ACCOUNT_SUCCESS';
export const GET_ACCOUNT_FAILURE = '@@liveon/account/GET_ACCOUNT_FAILURE';

export const UPDATE_ACCOUNT_REQUEST = '@@liveon/account/UPDATE_ACCOUNT_REQUEST';
export const UPDATE_ACCOUNT_SUCCESS = '@@liveon/account/UPDATE_ACCOUNT_SUCCESS';
export const UPDATE_ACCOUNT_FAILURE = '@@liveon/account/UPDATE_ACCOUNT_FAILURE';

export const UPDATE_PASSWORD_REQUEST = '@@liveon/account/UPDATE_PASSWORD_REQUEST';
export const UPDATE_PASSWORD_SUCCESS = '@@liveon/account/UPDATE_PASSWORD_SUCCESS';
export const UPDATE_PASSWORD_FAILURE = '@@liveon/account/UPDATE_PASSWORD_FAILURE';

export const DELETE_AVATAR_SUCCESS = '@@liveon/account/DELETE_ACCOUNT_AVATAR_SUCCESS';
export const CROP_AVATAR_SUCCESS = '@@liveon/account/CROP_AVATAR_SUCCESS';

export const SHOW_LOGIN_MODAL = '@@liveon/account/SHOW_LOGIN_MODAL';
export const HIDE_LOGIN_MODAL = '@@liveon/account/HIDE_LOGIN_MODAL';

export const SHOW_REGISTRATION_MODAL = '@@liveon/account/SHOW_REGISTRATION_MODAL';
export const HIDE_REGISTRATION_MODAL = '@@liveon/account/HIDE_REGISTRATION_MODAL';