import * as actionType from '../actionTypes'

const initialState = {
    firstName: null,
    lastName: null,
    avatarUrl: null,
    status: null,
    timestamp: null
};

export default function account(state = initialState, action) {
    switch (action.type) {

        case actionType.GET_ACCOUNT_SUCCESS: {
            const userInfo = {
                firstName: action.firstName,
                lastName: action.lastName,
                avatarUri: action.avatarUri,
                avatarCropUri: action.avatarCropUri,
                email: action.email
            };

            return {
                ...state,
                ...userInfo
            };
        }

        case actionType.UPDATE_ACCOUNT_SUCCESS:
            return {
                ...state,
                email: action.email,
                firstName: action.firstName,
                lastName: action.lastName,
                status: actionType.UPDATE_ACCOUNT_SUCCESS,
                timestamp: null
            };

        case actionType.CROP_AVATAR_SUCCESS:
            return {
                ...state,
                avatarCropUri: action.avatarCropUri
            }

        case actionType.DELETE_AVATAR_SUCCESS:
            return {
                ...state,
                avatarUri: null,
                avatarCropUri: null
            }


        default:
            return state;
    }
}
