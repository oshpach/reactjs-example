import * as actionType from '../actionTypes'

const initialState = {
    showLoginModal: false
};

export default function login(state = initialState, action) {
    switch (action.type) {
        case actionType.SHOW_LOGIN_MODAL:
            return {
                ...state,
                showLoginModal: true
            };

        case actionType.HIDE_LOGIN_MODAL:
            return {
                ...state,
                showLoginModal: false
            };

        default:
            return state;
    }
}
