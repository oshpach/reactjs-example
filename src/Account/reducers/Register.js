import * as actionType from '../actionTypes'

const initialState = {
    email: "",
    firstName: "",
    lastName: "",
    status: null,
    showRegistrationModal: false
};

export default function register(state = initialState, action) {
    switch (action.type) {

        case actionType.SHOW_REGISTRATION_MODAL:
            return {
                ...state,
                showRegistrationModal: true
            };

        case actionType.HIDE_REGISTRATION_MODAL:
            return {
                ...state,
                showRegistrationModal: false
            };

        default:
            return state;
    }
}
