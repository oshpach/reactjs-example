import React from 'react';
import {connect} from 'react-redux';
import {Button, Alert, Row, Col,DropdownButton, MenuItem} from 'react-bootstrap';
import {Field, reduxForm, initialize, SubmissionError} from 'redux-form';
import {StyleSheet, css} from 'aphrodite/no-important';
import isEmail from 'sane-email-validation';
import Common from'../../Common'
import {Map, is as areSame} from 'immutable'

import actions from '../actions'
import * as actionTypes from '../actionTypes'
import defaultAvatar from '../../Common/createTribute/images/default-avatar.png';
import * as selectors from '../Selectors';
import ChangePassword from './ChangePassword'

const {components: {BlockInput, BlockTile, ImageCropUploader}, helpers: {responseFormError}} = Common;

const styles = StyleSheet.create({
    avatarText: {
        color: '#3498DB',
        fontWeight: '600',
        fontSize: '15px',
        textAlign: 'center',
        marginBottom: 15
    },
    userSettings: {
        maxWidth: '750px !important',
        position: 'relative'
    },
    editButton: {
        position: 'absolute',
        right: '10px',
        top: '10px',
        zIndex: '899'
    }
});

const loadingAlert = (
    <Alert bsStyle="info">Loading...</Alert>
);

const savingAlert = (
    <Alert bsStyle="success">Saving...</Alert>
);

const savedAlert = (
    <Alert bsStyle="success"><strong>Saved</strong></Alert>
);

class PersonalDetails extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            avatarUri: this.props.userInfo && this.props.userInfo.avatarUri,
            avatarCropUrl: this.props.userInfo && this.props.userInfo.avatarCropUri,
            showModal: false

        };

        // Bind this to functions
        this.handleDrop = this.handleDrop.bind(this);
        this.handleCropImage = this.handleCropImage.bind(this);
        this.handleDeleteImage = this.handleDeleteImage.bind(this);
        this.showModal = this.showModal.bind(this);
        this.hideModal = this.hideModal.bind(this);
    }

    componentWillMount() {
        this.props.getAccount();
    }

    componentWillReceiveProps(nextProps) {
        // Set State
        if (!areSame(Map(this.props.userInfo), Map(nextProps.userInfo))) {
            this.setState({
                avatarUri: nextProps.userInfo.avatarUri,
                avatarCropUri: nextProps.userInfo.avatarCropUri
            });
        }
    }

    handleDrop(acceptedFiles) {
        this.props.saveAvatar(acceptedFiles)
            .then((data) => {
                this.setState({
                    avatarUri: data.avatarUri,
                    avatarCropUri: null
                });
            });
    }

    handleDeleteImage() {
         this.props.deleteAvatar(this.state.tributeId)
            .then(() => {
                this.setState({
                    avatarUri: null,
                    avatarCropUri: null,
                })
            })
            .catch((error) => {
                alert(error)
            });
    }

    handleCropImage(crop, scale) {
        this.props.cropAvatar(crop, scale)
            .then((data) => {
                this.setState({
                    avatarCropUri: data.avatarCropUri,
                });
            })
            .catch(() => {
                console.log('**** ERROR ****')
            });
    }


    showModal(){
        this.setState({
            showModal: true
        });
    }

    hideModal(){
        this.setState({
            showModal: false
        });
    }

    render() {
        const {handleSubmit, error, userInfo} = this.props;
        console.log(this.props);

        return (
            <div>
                <BlockTile className={css(styles.userSettings)}>
                    <ChangePassword modalVisible={this.state.showModal} hideModal={this.hideModal}/>
                    <div className={css(styles.editButton)}>
                        <DropdownButton style={{background: '#3498DB', border: '1px solid white', color: 'white'}} title={<span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>} id="EditTribute">
                            <MenuItem  onClick={this.showModal} eventKey="1">Change a password</MenuItem>
                            <MenuItem  divider/>
                            <MenuItem  onClick={this.handleDeleteTributeClick} eventKey="2">Delete an account</MenuItem>
                </DropdownButton>
                    </div>
                        <Row>
                            <Col lg={4} lgOffset={1} md={4} sm={6} xs={12}>
                                <div className={css(styles.avatarText)}>Profile Avatar</div>

                                <ImageCropUploader imageUri={userInfo.avatarUri}
                                                   cropUri={userInfo.avatarCropUri}
                                                   previewUrl={defaultAvatar}
                                                   height={200}
                                                   width={200}
                                                   border={0}
                                                   borderRadius={100}
                                                   onDrop={this.handleDrop}
                                                   onCrop={this.handleCropImage}
                                                   onDelete={this.handleDeleteImage}
                                />
                            </Col>
                            <Col lg={6} md={8} sm={6} xs={12}>
                                <form onSubmit={handleSubmit}>
                                    {userInfo.status === actionTypes.GET_ACCOUNT_REQUEST && loadingAlert}
                                    {userInfo.status === actionTypes.UPDATE_ACCOUNT_REQUEST && savingAlert}
                                    {userInfo.status === actionTypes.UPDATE_ACCOUNT_SUCCESS && savedAlert}

                                    {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                                    <Field name="firstName" label="First name" component={BlockInput} type="text"
                                           placeholder="Enter first name"/>

                                    <Field name="lastName" label="Last name" component={BlockInput} type="text"
                                           placeholder="Enter last name"/>

                                    <Field name="email" label="Email address" component={BlockInput} type="text"
                                           placeholder="Enter email"/>

                                    <div className="text-right btn-group-inline">
                                        <Button type="submit" bsStyle='primary'>Save changes</Button>
                                    </div>
                                </form>
                            </Col>
                        </Row>
                </BlockTile>
            </div>
        )
    }
}

const validate = values => {
    const errors = {};
    if (!values.firstName) {
        errors.firstName = 'First name is required';
    } else if (values.firstName.length >= 50) {
        errors.firstName = 'First name must be less than 50 characters';
    }

    if (!values.lastName) {
        errors.lastName = 'Last name is required';
    } else if (values.lastName.length >= 50) {
        errors.lastName = 'Last name must be less than 50 characters';
    }

    if (!values.email) {
        errors.email = 'Email is required';
    } else if (!isEmail(values.email)) {
        errors.email = 'Invalid email address';
    }

    return errors;
};

export const PersonalDetailsForm = reduxForm({
    form: 'PersonalDetails',  // a unique identifier for this form
    validate
})(PersonalDetails);


const mapStateToProps = (state) => {
    return {
        userInfo: selectors.user(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: model => {
            return dispatch(actions.account.updateAccount(model))
                .catch((error) => {
                    throw new SubmissionError(responseFormError(error));
                })
        },

        getAccount: () => {
            return dispatch(actions.account.getAccount(true))
                .then((model) => dispatch(initialize('PersonalDetails', model)));
        },

        saveAvatar: (file) => {return dispatch(actions.account.saveAvatar(file))},

        cropAvatar: (crop, scale) => {return dispatch(actions.account.cropAvatar(crop, scale))},

        deleteAvatar: () => {return dispatch(actions.account.deleteAvatar())},
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PersonalDetailsForm);
