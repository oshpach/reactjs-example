import React from 'react';
import { connect } from 'react-redux';
import { Button, Alert } from 'react-bootstrap';
import { Field, reduxForm, reset, SubmissionError } from 'redux-form';
import Common from '../../Common'
import actions from '../actions'

const { components: { BlockInput, BlockTile }, helpers: { responseFormError } } = Common;

class AccountAccess extends React.Component {

    savingAlert = (
        <Alert bsStyle="success">Saving...</Alert>
    );

    savedAlert = (
        <Alert bsStyle="success"><strong>Saved</strong></Alert>
    );

    render() {

        const { handleSubmit, error } = this.props;

        return (
            <BlockTile>
                <form onSubmit={handleSubmit}>
                    {error && <p className="bg-danger" style={{ padding: '15px' }}><strong>{error}</strong></p>}
                    {/*{userInfo.status === UPDATE_PASSWORD_REQUEST && this.savingAlert}
                    {userInfo.status === UPDATE_PASSWORD_SUCCESS && this.savedAlert}*/}

                    <Field name="oldPassword" label="Old Password" component={BlockInput} type="password"
                        placeholder="Old Password" />

                    <Field name="newPassword" label="New Password" component={BlockInput} type="password"
                        placeholder="New Password" />

                    <Field name="confirmPassword" component={BlockInput} type="password"
                        placeholder="Confirm password" />

                    <div className="text-right">
                        <Button type="submit" bsStyle='primary'>Save changes</Button>
                    </div>
                </form>
            </BlockTile>
        )
    }
}

const validate = values => {
    const errors = {};

    if (!values.oldPassword) {
        errors.oldPassword = 'Old password is required';
    }

    if (!values.newPassword) {
        errors.newPassword = 'New password is required';
    } else if (values.newPassword.length < 6) {
        errors.newPassword = 'Password needs to be at lease 6 characters';
    }

    if (values.confirmPassword !== values.newPassword) {
        errors.confirmPassword = "Password doesn't match";
    }

    return errors;
};

export let AccountAccessForm = reduxForm({
    form: 'AccountAccess',  // a unique identifier for this form
    validate
})(AccountAccess);

const mapStateToProps = (state) => {
    return {
        userInfo: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onSubmit: model => {
            return dispatch(actions.account.updatePassword(model))
                .then(() => dispatch(reset('AccountAccess')))
                .catch((error) => {
                    throw new SubmissionError(responseFormError(error));
                })
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountAccessForm);
