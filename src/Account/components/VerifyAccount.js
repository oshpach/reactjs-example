import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {Alert, Button} from 'react-bootstrap'
import {verifyAccount, resendVerificationCode} from '../actions/account'
import Common from '../../Common'

const {components: {BlockTile}} = Common;

class VerifyAccount extends React.Component {
    constructor(props) {
        super(props);

        // Set Initial State
        this.state = {
            status: 'verifying'
        };

        this.resendCode = this.resendCode.bind(this);
        this.navigateHome = this.navigateHome.bind(this);
        this.navigateLogin = this.navigateLogin.bind(this);
    }

    componentDidMount() {
        this.props.verifyAccount()
            .then((result) => {
                this.setState({
                    status: 'success'
                });
            })
            .catch(error => {
                this.setState({
                    status: 'error',
                    error: error
                });
            })
    };

    resendCode(){
        this.setState({
            status: 'resendingCode',
            error: null
        });

        this.props.resendVerificationCode()
            .then(() => {
                this.setState({
                    status: 'receivedCode',
                    error: null
                });
            })
            .catch((error) => {
                this.setState({
                    status: 'errorReceivingCode',
                    error: error
                });
            })
    }

    navigateHome() {
        this.props.router.push({
            pathname: '/'
        })
    }

    navigateLogin() {
        this.props.router.push({
            pathname: '/',
            state: {showLogin: true}
        })
    }

    render() {
        return (
            <BlockTile>
                {this.state.status === 'verifying' &&
                    <div style={{textAlign: 'center', cursor: 'wait'}}>
                        <Alert bsStyle='info'>Verifying email address</Alert>
                    </div>
                }

                {this.state.status === 'success' &&
                    <div style={{textAlign: 'center'}}>
                        <Alert bsStyle='success'>Your email address was successfully verified.</Alert>
                        <Button onClick={this.navigateLogin} bsStyle="primary" bsSize="large">Login</Button>
                    </div>
                }

                {this.state.status === 'error' &&
                    <div style={{textAlign: 'center'}}>
                        <Alert bsStyle='danger'>{(this.state.error && this.state.error.message) || 'There was an error verifying your email address'}</Alert>
                        <Button onClick={this.resendCode} bsStyle="primary" bsSize="large">Resend verification code</Button>
                    </div>
                }

                {this.state.status === 'resendingCode' &&
                    <div style={{textAlign: 'center', cursor: 'wait'}}>
                        <Alert bsStyle='info'>Resending email verification code</Alert>
                    </div>
                }

                {this.state.status === 'receivedCode' &&
                    <div style={{textAlign: 'center'}}>
                        <Alert bsStyle='success'>Please check your email and click the link to complete your registration</Alert>
                        <Button onClick={this.navigateHome} bsStyle="primary" bsSize="large">Home</Button>
                    </div>
                }

                {this.state.status === 'errorReceivingCode' &&
                <div style={{textAlign: 'center'}}>
                    <Alert bsStyle='danger'>{(this.state.error && this.state.error.message) || 'There was an error requesting a new code. Please try again.'}</Alert>
                    <Button onClick={this.navigateHome} bsStyle="primary" bsSize="large">Home</Button>
                </div>
                }
            </BlockTile>
        );
    };
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        verifyAccount: () => {
            return dispatch(verifyAccount(props.params.username, props.params.token))
        },

        resendVerificationCode: () => {
            return dispatch(resendVerificationCode(props.params.username))
        }
    }
};

export default withRouter(connect(null, mapDispatchToProps)(VerifyAccount));
