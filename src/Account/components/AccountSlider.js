import React from 'react';
import Slider from 'react-slick';
import {StyleSheet, css} from 'aphrodite/no-important'


const styles = StyleSheet.create({
    sliderPhoto: {
        width: '260px',
        height: '260px',
        overflow: 'hidden',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center center',
        backgroundSize: 'cover',
        backgroundColor: 'lightgrey',
        margin: '10',
        border: '1px solid #CECECE'
    }
});

class AccountSlider extends React.Component {


    render() {
        const settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };
        return (
            <Slider {...settings}>
                <div className={css(styles.sliderPhoto)}><h3>1</h3></div>
                <div className={css(styles.sliderPhoto)}><h3>2</h3></div>
                <div className={css(styles.sliderPhoto)}><h3>3</h3></div>
                <div className={css(styles.sliderPhoto)}><h3>4</h3></div>
                <div className={css(styles.sliderPhoto)}><h3>5</h3></div>
                <div className={css(styles.sliderPhoto)}><h3>6</h3></div>
            </Slider>
        );
    }
}

export default AccountSlider;