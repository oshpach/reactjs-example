import React from 'react';
import { connect } from 'react-redux';
import {Grid, Row, Col, Tabs, Tab} from 'react-bootstrap'
import {StyleSheet, css} from 'aphrodite/no-important'
import {user} from '../../Account/Selectors'
import Common from '../../Common'
import AccountSlider from './AccountSlider'
import './AccountPage.css'


import {getAccount} from '../../Account/actions/account'

const {components: {ImageLoader}} = Common;


const styles = StyleSheet.create({

    accountWrapper: {
        marginTop: 70,
        marginBottom: 20
    },
    userAvatar: {
        borderRadius: '50%'
    },
    leftSidebar: {
        textAlign: 'center',
        paddingTop: '20px'
    },
    rightSidebar: {
        backgroundColor: '#FDFDFD',
        minHeight: '400px'
    },
    userName: {
        marginTop: 10
    },
    userMenu: {

    },
    userMenuWrapper: {
        width: '100%',
        padding: 0,
        margin: 0
    },
    userMenuItem: {
        width: '100%',
        listStyle: 'none',
        minHeight: '50px',
        display: 'flex',
        justifyContent: 'center',
        cursor: 'pointer',
        position: 'relative'
    },
    userMenuText: {
        alignSelf: 'center',
        fontSize: '16px',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#3498DB',
        fontStyle: 'italic'
    },
    divider: {
        width: '90%',
        display: 'block',
        height: '1px',
        backgroundColor: 'lightgrey',
        margin: '0 auto'
    },
    activeUser: {
        display: 'block',
        position: 'absolute',
        width: '15px',
        height: '15px',
        backgroundColor: '#FDFDFD',
        right: '-7.5px',
        top: '17.5px',
        transform: 'rotate(-45deg)',
        borderLeft: '1px solid #CECECE',
        borderTop: '1px solid #CECECE',
        "@media screen and (max-width: 768px)": {
            display: 'none'
        }
    },
    rightSidebarTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#3498DB',
        textTransform: 'uppercase'
    },
    pageTitle: {
        color: '#3498DB',
        fontSize: '34px'
    }

});


class AccountPage extends React.Component{


    constructor(props){
        super(props);
        this.state = {
            index: 1
        };
        this.changePage = this.changePage.bind(this);
    }


    componentWillMount(){
        this.props.getAccount();
    }

    changePage(index){
        this.setState({
            index
        });
    }

    render(){
        const {user = {}} = this.props;
        console.log(this.props);
        return(
            <Grid>
                <Row className={css(styles.accountWrapper)}>
                    <Col lg={12} md={12} sm={12} xs={12}>
                        <h2 className={css(styles.pageTitle)}><span style={{marginRight: 5}}><i className="fa fa-user" aria-hidden="true"></i></span>User Page</h2>
                    </Col>
                </Row>
                <Row>
                    <Col lg={4} md={4} style={{padding: 0,minHeight: '400px', backgroundColor: '#F7F7F7', border: '1px solid #CECECE'}}>
                        <div className={css(styles.leftSidebar)}>
                            <ImageLoader src={user.avatarUri} className={css(styles.userAvatar)} alt=""/>
                            <p className={css(styles.userName)}>{`${user.firstName} ${user.lastName}`}</p>
                        </div>
                        <div className={css(styles.userMenu)}>
                            <ul className={css(styles.userMenuWrapper)}>
                                <li style={{listStyle: 'none', marginBottom: '10px'}}>
                                    <span className={css(styles.divider)}></span>
                                </li>
                                <li className={css(styles.userMenuItem)} onClick={() => {this.changePage(1)}}>
                                    <span className={css(styles.userMenuText)}>About</span>
                                    <span className={this.state.index === 1 ? css(styles.activeUser): ''}></span>
                                </li>
                                <li className={css(styles.userMenuItem)} onClick={() => {this.changePage(2)}}>
                                    <span className={css(styles.userMenuText)}>Tributes</span>
                                    <span className={this.state.index === 2 ? css(styles.activeUser) : ''}></span>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col lg={8} md={8} className={css(styles.rightSidebar)} id="rightSection">
                        <h1 className={css(styles.rightSidebarTitle)}>{this.state.index === 1 ? 'ABOUT' : 'TRIBUTES'}</h1>
                        <Tabs style={{display: this.state.index === 1 ? 'block' : 'none'}}>
                            <Tab eventKey={1} title="Interests">

                            </Tab>
                            <Tab eventKey={2} title="Education"></Tab>
                            <Tab eventKey={3} title="Military"></Tab>
                            <Tab eventKey={4} title="Job"></Tab>
                        </Tabs>
                        <div style={{display: this.state.index === 2 ? 'block' : 'none', width: '80%', margin: '0 auto'}}>
                            <AccountSlider/>
                        </div>
                    </Col>
                </Row>
            </Grid>
        )
    }

}

const mapStateToProps = (state, props) => {
    return {
        user: user(state)
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getAccount: () => dispatch(getAccount(true))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountPage);