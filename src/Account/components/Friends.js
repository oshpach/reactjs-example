import React from 'react';
import {connect} from 'react-redux'
import {StyleSheet, css} from 'aphrodite/no-important'
import {Button, Tabs, Tab, Row, Col} from 'react-bootstrap'
import Common from '../../Common'
import {defaultAvatar} from '../../Common/images'
import actions from '../../Tributes/actions'
import * as selectors from '../../Tributes/selectors'

import './Friends.css'


const {components : {ImageLoader}} = Common;


const styles = StyleSheet.create({
    friendsWrapper: {
        display: 'flex',
        width: '90%',
        margin: '100px auto',
        minHeight: '400px',
        padding: '0 20px'
    },
    friendsAccepted: {
        width: '100%',
        textAlign: 'center',
    },
    friendsRequested: {
        width: '100%',
        textAlign: 'center',
    },
    friendsRejected: {
        width: '100%',
        textAlign: 'center',
    },
    friendsTitle: {
        fontWeight: 'bold',
        color: '#3498DB',
        textTransform: 'uppercase'
    },
    friendBlock: {
        display: 'flex',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        minHeight: '120px',
        maxWidth: '320px',
        boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
        marginBottom: '10px',
        borderRadius: '5px',
        margin: '0 auto'
    },
    friendPhoto: {
        width: '90px',
        height: '90px',
        borderRadius: '50%',
        "@media screen and (max-width: 400px)": {
            width: '70px',
            height: '70px'
        }
    },
    friendPhotoContainer: {
        alignSelf: 'center'
    },
    friendDescription: {
        alignSelf: 'center',
        textAlign: 'center'
    },
    connectTitle: {
        fontSize: '9px',
        fontWeight: 'bold',
        fontStyle: 'italic'
    },
    userTitle: {
        fontSize: '13px',
        "@media screen and (max-width: 450px)": {
            fontSize: '12px'
        }
    },
    buttonsBlock: {
        fontSize: '9px',
        '@media screen and (max-width: 450px)': {
            fontSize: '8px'
        }
    }
});


class Friends extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            users: []
        };
        this.getStatus = this.getStatus.bind(this);
    }


    componentWillMount(){
        if(this.props.tributes.length === 0){
            this.props.getTributes();
        }
        this.props.getMyTributes()
            .then((data) => {
                    this.props.getStatus('requested');
                    // this.getStatus(this.props.tributes[0].tributeId, 'accepted')
            });
    };




    getStatus(status){
        this.props.getStatus(status);
    };

    getTributeByTributeId(tributeId){
        let tributesById = this.props.myTributes.filter((tribute) => tribute.tributeId === tributeId);
        return tributesById;
    }

    acceptStatus(userId, tributeId){
        this.props.acceptStatus(userId, tributeId)
    }
    disconnectStatus(userId, tributeId){
        this.props.disconnectStatus(userId, tributeId);
    }

    handleSelect = (key) => {
        if(key === 2 && this.props.acceptedUsers.length === 0){
            this.props.getStatus('accepted');
        } else if(key === 1 && this.props.tributeConnectRequests.length === 0){
            this.props.getStatus('requested');
        } else if(key === 3 && this.props.rejectedUsers.length === 0){
            this.props.getStatus('declined');
        }
    }


    render(){
        const {tributeConnectRequests=[], acceptedUsers = [], rejectedUsers = []} = this.props;
        console.log(this.props);
        return (
            <div className={css(styles.friendsWrapper)}>
                <Tabs id="friends_tab" style={{width: '100%'}} onSelect={this.handleSelect}>
                    <Tab eventKey={1} title="Requested" style={{marginTop: '40px'}}>
                        <Row>
                            {tributeConnectRequests.length ? tributeConnectRequests.map((tribute, index) => (
                                <Col lg={3} md={4} sm={6} xs={12} key={index} style={{marginTop: '10px'}}>
                                    <div className={css(styles.friendBlock)}>
                                        <ImageLoader
                                            src={tribute.avatarCropUri}
                                            previewSrc={defaultAvatar}
                                            className = {css(styles.friendPhoto)}
                                            containerClassName={css(styles.friendPhotoContainer)}
                                        />
                                        <div className={css(styles.friendDescription)}>
                                            <p className={css(styles.userTitle)}>{`${tribute.firstName} ${tribute.lastName}`}</p>
                                            <p className={css(styles.connectTitle)}>Wants to connect: {`${this.getTributeByTributeId(tribute.tributeId)[0].firstName} ${this.getTributeByTributeId(tribute.tributeId)[0].lastName}`}</p>
                                            <div>
                                                <Button className={css(styles.buttonsBlock)} onClick={() => {this.acceptStatus(tribute.userId, tribute.tributeId)}} style={{marginRight: '10px'}} bsStyle="success">Accept</Button>
                                                <Button className={css(styles.buttonsBlock)} onClick={() => {this.disconnectStatus(tribute.userId, tribute.tributeId)}} bsStyle="danger">Reject</Button>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            )) : <p>There are no tributes yet...</p>}
                        </Row>
                    </Tab>
                    <Tab eventKey={2} title="Accepted" style={{marginTop: '40px'}}>
                        {acceptedUsers.length ? acceptedUsers.map((tribute, index) => (
                            <Col lg={3} md={4} sm={6} xs={12} key={index} style={{marginTop: '10px'}}>
                                <div className={css(styles.friendBlock)}>
                                    <ImageLoader
                                        src={tribute.avatarCropUri}
                                        previewSrc={defaultAvatar}
                                        className = {css(styles.friendPhoto)}
                                        containerClassName={css(styles.friendPhotoContainer)}
                                    />
                                    <div className={css(styles.friendDescription)}>
                                        <p className={css(styles.userTitle)}>{`${tribute.firstName} ${tribute.lastName}`}</p>
                                        <p className={css(styles.connectTitle)}>Connected to: {`${this.getTributeByTributeId(tribute.tributeId)[0].firstName} ${this.getTributeByTributeId(tribute.tributeId)[0].lastName}`}</p>
                                        <div style={{paddingRight: '2px'}}>
                                            <Button className={css(styles.buttonsBlock)} style={{marginRight: '2px'}}  bsStyle="primary">Message</Button>
                                            <Button className={css(styles.buttonsBlock)} onClick={() => {this.disconnectStatus(tribute.userId, tribute.tributeId)}} bsStyle="danger">Delete</Button>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        )) : <p>You have no tributes ...</p>}
                    </Tab>
                    <Tab eventKey={3} title="Rejected" style={{marginTop: '40px'}}>
                        {rejectedUsers.length ? rejectedUsers.map((tribute, index) => (
                            <Col lg={3} md={4} sm={6} xs={12} key={index}>
                                <div className={css(styles.friendBlock)}>
                                    <ImageLoader
                                        src={tribute.avatarCropUri}
                                        previewSrc={defaultAvatar}
                                        className = {css(styles.friendPhoto)}
                                        containerClassName={css(styles.friendPhotoContainer)}
                                    />
                                    <div className={css(styles.friendDescription)}>
                                        <p>{`${tribute.firstName} ${tribute.lastName}`}</p>
                                        <p className={css(styles.connectTitle)}>Wanted to connect: Jack Shaw</p>
                                    </div>
                                </div>
                            </Col>
                        )): <p>There are no tributes.</p>}
                    </Tab>
                </Tabs>
            </div>
        )
    }

}

const mapStateToProps = (state, props) => {
    return {
        tributes: selectors.tributes(state),
        myTributes: selectors.myTributes(state),
        tributeConnectRequests: selectors.tributeConnectRequests(state),
        acceptedUsers: selectors.acceptedUsers(state),
        rejectedUsers: selectors.rejectedUsers(state)
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getTributes: () => dispatch(actions.tributes.getTributes()),
        getStatus: (status) => dispatch(actions.connected.getConnectRequests(status)),
        getMyTributes: () => dispatch(actions.tributes.getMyTributes()),
        acceptStatus : (userId, tributeId) => dispatch(actions.connected.connectAccept(userId, tributeId)),
        disconnectStatus: (userId, tributeId) => dispatch(actions.connected.connectDisconnect(userId, tributeId))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Friends);