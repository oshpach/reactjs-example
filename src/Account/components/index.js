export {default as Login} from './Login';
export {default as Register} from './Register';
export {default as AccountSettings} from './AccountSettings';
export {default as VerifyAccount} from './VerifyAccount';
export {default as PersonalDetails} from './PersonalDetails';
export {default as Friends} from './Friends';
export {default as AccountPage} from './AccountPage'

