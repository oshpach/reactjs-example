import React from 'react'
import {Modal, Grid} from 'react-bootstrap'
import AccountAccess from './AccountAccess'

const ChangePassword = (props) =>{
    const close = () => {
        props.hideModal();
    };
    return (
        <Grid>
            <Modal onHide={close} show={props.modalVisible}>
                <Modal.Body>
                    <span onClick={close} style={{position: 'absolute', top: '0', right: '10px', cursor: 'pointer'}}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </span>
                    <AccountAccess/>
                </Modal.Body>
            </Modal>
        </Grid>
    )
};

export default ChangePassword;