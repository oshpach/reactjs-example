import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router'
import {Modal, Alert, Button} from 'react-bootstrap';
import {Field, reduxForm, SubmissionError} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import {register, showRegistrationModal, hideRegistrationModal, showLoginModal} from '../actions/account';

import Common from'../../Common'

import isEmail from 'sane-email-validation';

import '../../Common/fonts/flaticon/flaticon.css';

const {
    helpers: {responseFormError},
    components: {BlockInput}
} = Common;

const styles = StyleSheet.create({
    strikeBox: {
        display: 'block',
        textAlign: 'center',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        margin: '35px auto',

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            margin: '19px auto'
        }
    },

    strike: {
        position: 'relative',
        display: 'inline-block',
        ':before': {
            content: '""',
            position: 'absolute',
            top: '50%',
            width: 9999,
            height: 1,
            background: 'red',
            right: '100%',
            marginRight: 55
        },
        ':after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            width: 9999,
            height: 1,
            background: 'red',
            left: '100%',
            marginLeft: 55
        },
    },

    flaticon: {
        marginRight: 30,
        verticalAlign: 'sub',

        ':before': {
            fontSize: '1.5em !important'
        },

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            marginRight: 15
        }
    },

    blockButton: {
        padding: '20px 0',
        color: 'white',
        fontSize: '2em',
        textTransform: 'uppercase',

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            fontSize: '0.9em'
        }
    },

    facebookButton: {
        backgroundColor: '#3b5998',
        color: 'white',
        ':hover': {
            backgroundColor: '#3b5998',
            color: 'white',
        }
    },

    googleButton: {
        backgroundColor: '#db3236',
        color: 'white',
        ':hover': {
            backgroundColor: '#db3236',
            color: 'white',
        }
    },

    buttonLoginSignup: {
        padding: '20px 0',
        textTransform: 'uppercase',
        fontSize: '1.5em'
    },

    linkText: {
        textAlign: 'center',
        marginTop: 15,
        fontSize: '1.2em'
    },

    linkButton: {
        textDecoration: 'underline',
        paddingLeft: 5,
        fontSize: '1.1em'
    }
});


export class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            registered: false
        };

        // Bind this to functions
        this.closeModal = this.closeModal.bind(this);
        this.submit = this.submit.bind(this);
    }

    closeModal() {
        this.props.hideModal();
    };

    submit(model) {
        return this.props.register(model)
            .then((result) => {
                this.setState({
                    registered: true
                })
            })
            .catch(error => {
                throw new SubmissionError(responseFormError(error));
            })
    };

    render() {
        const {error, handleSubmit, pristine, submitting} = this.props;

        return (

            <div>
                <Modal show={this.props.modalVisible} onHide={this.closeModal}>

                    <Modal.Body>
                        {this.state.registered &&
                            <div style={{textAlign: 'center'}}>
                                <Alert bsStyle='success'>Please check your email and click the link to complete your registration</Alert>
                                <Button onClick={this.closeModal} bsStyle="primary" bsSize="large">OK</Button>
                            </div>
                        }

                        {!this.state.registered &&
                            <form onSubmit={handleSubmit(this.submit)}>
                                {/*

                                 <Button block bsSize="large" className={css(styles.blockButton, styles.facebookButton)}>
                                 <i className={classnames('flaticon-facebook-logo', css(styles.flaticon))}/>Sign up with Facebook
                                 </Button>

                                 <Button block bsSize="large" className={css(styles.blockButton, styles.googleButton)}>
                                 <i className={classnames('flaticon-social-media', css(styles.flaticon))}/>Sign up with Google
                                 </Button>

                                 <div className={css(styles.strikeBox)}>
                                 <span className={css(styles.strike)}>Or</span>
                                 </div>
                                 */}

                                {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                                <Field name="firstName"
                                       component={BlockInput}
                                       type="text"
                                       placeholder="First Name"
                                       addonAfter={<span className="fa fa-user fa-2x fa-fw"/>}
                                />

                                <Field name="lastName"
                                       component={BlockInput}
                                       type="text"
                                       placeholder="Last Name"
                                       addonAfter={<span className="fa fa-user fa-2x fa-fw"/>}
                                />

                                <Field name="email"
                                       component={BlockInput}
                                       type="email"
                                       placeholder="Email Address"
                                       addonAfter={<span className="fa fa-envelope  fa-2x fa-fw"/>}
                                />

                                <Field name="password"
                                       component={BlockInput}
                                       type="password"
                                       placeholder="password"
                                       addonAfter={<span className="fa fa-lock  fa-2x fa-fw"/>}
                                />

                                <Button type="submit"
                                        block
                                        bsSize="large"
                                        bsStyle="primary"
                                        disabled={pristine || submitting || error !== undefined}
                                        className={css(styles.buttonLoginSignup)}
                                >
                                    Sign Up
                                </Button>

                                <div className={css(styles.linkText)}>
                                    Already a member?
                                    <Button className={css(styles.linkButton)}
                                            bsStyle="link"
                                            onClick={this.props.showLoginModal}>Login
                                    </Button>
                                </div>

                            </form>
                        }
                    </Modal.Body>

                </Modal>
            </div>
        );
    };
}

const validate = values => {

    const errors = {};

    if (!values.firstName) {
        errors.firstName = 'First name is required';
    }

    if (!values.lastName) {
        errors.lastName = 'Last name is required';
    }

    if (!values.email) {
        errors.email = 'Email is required';
    } else if (!isEmail(values.email)) {
        errors.email = 'Invalid email address';
    }

    if (!values.password) {
        errors.password = 'Password is required';
    } else if (values.password.length < 6) {
        errors.password = 'Password needs to be at lease 6 characters';
    }

    return errors;
};

const mapStateToProps = (state) => {
    const {register} = state.account;
    return {
        modalVisible: register.showRegistrationModal
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        register: inputs => {
            return dispatch(register(inputs))
        },
        showModal: () => {
            dispatch(showRegistrationModal())
        },
        hideModal: () => {
            dispatch(hideRegistrationModal())
        },

        showLoginModal: () => {
            dispatch(hideRegistrationModal());
            dispatch(showLoginModal())
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'RegisterForm',  // a unique identifier for this form
    validate
})(Register)));
