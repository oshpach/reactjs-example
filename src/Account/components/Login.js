import React from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router'
import {SubmissionError} from 'redux-form'
import {Modal, Button, Grid, Row, Col} from 'react-bootstrap'
import {Field, reduxForm} from 'redux-form'
import {StyleSheet, css} from 'aphrodite/no-important'
import FacebookLogin from 'react-facebook-login'
import isEmail from 'sane-email-validation'
import {login, showLoginModal, hideLoginModal, showRegistrationModal} from '../actions/account'

import Common from'../../Common'
import '../../Common/fonts/flaticon/flaticon.css';
import './facebookLogIn.css'

const {helpers: {responseFormError}, components: {BlockInput}} = Common;

const styles = StyleSheet.create({
    strikeBox: {
        display: 'block',
        textAlign: 'center',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        margin: '35px auto',

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            margin: '19px auto'
        }
    },

    strike: {
        position: 'relative',
        display: 'inline-block',
        ':before': {
            content: '""',
            position: 'absolute',
            top: '50%',
            width: 9999,
            height: 1,
            background: 'red',
            right: '100%',
            marginRight: 55
        },
        ':after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            width: 9999,
            height: 1,
            background: 'red',
            left: '100%',
            marginLeft: 55
        },
    },

    facebookButton: {
        "@media screen and (max-width: 767px)": {
            marginTop: '5px'
        }
    },

    flaticon: {
        marginRight: 30,
        verticalAlign: 'sub',

        ':before': {
            fontSize: '1.5em !important'
        },

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            marginRight: 15
        }
    },

    blockButton: {
        padding: '20px 0',
        color: 'white',
        fontSize: '2em',
        textTransform: 'uppercase',

        /* Small Devices, Tablets */
        '@media only screen and (max-width : 768px)': {
            fontSize: '0.9em'
        }
    },

    googleButton: {
        backgroundColor: '#db3236',
        color: 'white',
        ':hover': {
            backgroundColor: '#db3236',
            color: 'white',
        }
    },

    buttonLoginSignup: {
        padding: 'calc(.34435vw + 13.38843px) calc(.34435vw + 18.38843px)',
        border: '1px solid #3498DB',
        fontSize: 'calc(.27548vw + 12.71074px)',
        borderRadius: '0',
        width: '100%',
        background: '#3498DB',
        fontWeight: '700',
        textTransform: 'uppercase',
        color: 'white'
    },

    linkText: {
        textAlign: 'center',
        marginTop: 15,
        fontSize: '1.2em'
    },


    linkButton: {
        textDecoration: 'underline',
        paddingLeft: 5,
        fontSize: '1.1em'
    }

});

const responseFacebook = (response) => {
    console.log(response);
};

const Login = props => {
    const close = function () {
        props.hideModal();
    };

    const {error, handleSubmit, pristine, submitting} = props;

    return (
        <Grid>
            <Modal show={props.modalVisible} onHide={close}>
                <Modal.Body>
                    <form onSubmit={handleSubmit}>
                        {/*
                         <Button block bsSize="large" className={css(styles.blockButton, styles.facebookButton)}>
                         <i className={classnames('flaticon-facebook-logo', css(styles.flaticon))}/>Login with Facebook
                         </Button>

                         <Button block bsSize="large" className={css(styles.blockButton, styles.googleButton)}>
                         <i className={classnames('flaticon-social-media', css(styles.flaticon))}/>Login with Google
                         </Button>

                         <div className={css(styles.strikeBox)}>
                         <span className={css(styles.strike)}>Or</span>
                         </div>
                         */}

                        {error && <p className="bg-danger" style={{padding: '15px'}}><strong>{error}</strong></p>}

                        <Field name="email" component={BlockInput} type="email" placeholder="Email Address"/>

                        <Field name="password" component={BlockInput} type="password" placeholder="Password"/>
                        <Row>
                            <Col lg={6} md={6} sm={6} xs={12}>
                                <button
                                    type="submit"
                                    disabled={pristine || submitting}
                                    className={css(styles.buttonLoginSignup)}
                                >
                                    Login
                                </button>
                            </Col>
                            <Col lg={6} md={6} sm={6} xs={12} className={css(styles.facebookButton)}>
                                <FacebookLogin
                                    appId="1938778613074577"
                                    autoLoad={true}
                                    fields="name,email,picture"
                                    icon={<i className="fa fa-facebook"></i>}
                                    callback={responseFacebook}
                                />
                            </Col>
                        </Row>
                        <div className={css(styles.linkText)}>
                            Don't have an account?
                            <Button className={css(styles.linkButton)}
                                    bsStyle="link"
                                    onClick={props.showRegistrationModal}>Sign Up
                            </Button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        </Grid>
    );
};

const validate = values => {
    const errors = {};
    if (!values.email) {
        errors.email = 'Email is required';
    } else if (!isEmail(values.email)) {
        errors.email = 'Invalid email address';
    }

    if (!values.password) {
        errors.password = 'Password is required';
    }

    return errors;
};

const loginForm = reduxForm({
    form: 'LoginForm',  // a unique identifier for this form
    validate
})(Login);

const mapStateToProps = (state) => {
    const {login} = state.account;
    return {
        modalVisible: login.showLoginModal
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        onSubmit: model => {
            return dispatch(login(model))
                .then(() => {
                    if (ownProps.location.state && ownProps.location.state.redirectPath) {
                        ownProps.router.push(ownProps.location.state.redirectPath);
                        ownProps.location.state.redirectPath = null; // set it to null after redirect
                    } else {
                        ownProps.router.push('/mytributes');
                    }
                })
                .catch((error) => {
                    throw new SubmissionError(responseFormError(error));
                });
        },

        showModal: () => {
            dispatch(showLoginModal())
        },

        hideModal: () => {
            dispatch(hideLoginModal())
        },

        showRegistrationModal: () => {
            dispatch(hideLoginModal());
            dispatch(showRegistrationModal());
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(loginForm));
