import React from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'react-bootstrap';
import {StyleSheet, css} from 'aphrodite/no-important'
import PersonalDetails from './PersonalDetails'
import './AccountOverflowX.css'

const styles = StyleSheet.create({
    userAccountWrapper: {
        marginTop: '70px',
        position: 'relative'
    },
    userAccountTitle: {
        textAlign: 'center',
        fontWeight: 'bold',
        textTransform: 'uppercase',
        color: '#3498DB'
    }
});

class AccountSettings extends React.Component {



    render() {
        return (
            <Row className={css(styles.userAccountWrapper) + ' AccountSettings'}>
                <Col xs={12}>
                    <h2 className={css(styles.userAccountTitle)}>
                        <i className="ii-settings"/>
                        <span>Account Settings</span>
                    </h2>

                    <Col xs={12}>
                        <PersonalDetails/>
                    </Col>
                </Col>
            </Row>
        );
    };
}

export default connect(null, null)(AccountSettings);
