import { actionCreator, getDataAction, postDataAction, deleteDataAction } from '../../Common/actions'
import * as actionType from '../actionTypes'
import * as aws from '../aws/aws_cognito';
const jwtDecode = require('jwt-decode');

export function showLoginModal() {
    return {
        type: actionType.SHOW_LOGIN_MODAL,
        showLoginModal: true
    };
}

export function hideLoginModal() {
    return {
        type: actionType.HIDE_LOGIN_MODAL,
        showLoginModal: false
    };
}

export function showRegistrationModal() {
    return {
        type: actionType.SHOW_REGISTRATION_MODAL,
        showRegistrationModal: true
    };
}

export function hideRegistrationModal() {
    return {
        type: actionType.HIDE_REGISTRATION_MODAL,
        showRegistrationModal: false
    };
}

export function getAccount(fromServer) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        if (fromServer === true) {
            return getDataAction('/account')
                .then(userInfo => {

                    dispatch(actionCreator(actionType.GET_ACCOUNT_SUCCESS, userInfo));

                    return Promise.resolve(userInfo);
                })
                .catch(error => {
                    // dispatch failure action
                    dispatch(actionCreator(actionType.REQUEST_END));

                    return Promise.reject(error);
                });
        }

        return aws.getIdToken()
            .then((token) => {
                const decode = jwtDecode(token);
                const model = {
                    email: decode['email'],
                    firstName: decode['give_name'],
                    lastName: decode['family_name'],
                    avatarUri: decode['picture']
                }

                return Promise.resolve(model);
            })
            .catch(() => {
                return Promise.reject({});
            })
    }
}

export function updateAccount(model) {
    console.log(model);
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return postDataAction('/account', model)
            .then(userInfo => {

                dispatch(actionCreator(actionType.UPDATE_ACCOUNT_SUCCESS, userInfo));

                return Promise.resolve(userInfo);
            })
            .catch(error => {
                // dispatch failure action
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

//* ***************************************************** *//

export function saveAvatar(file) {
    return (dispatch) => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        let fd = new FormData();
        fd.append('avatarFile', file);

        return postDataAction('/account/avatar', fd)
            .then((data) => {
                dispatch(actionCreator(actionType.GET_ACCOUNT_SUCCESS, data));

                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(data);
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error)
            });
    }
}

export function cropAvatar(crop, scale) {
    return (dispatch) => {
        return postDataAction('/account/cropavatar',
            {
                crop,
                scale
            }
        )
            .then(data => {
                dispatch(actionCreator(actionType.CROP_AVATAR_SUCCESS, { avatarCropUri: data.avatarCropUri }));

                return Promise.resolve(data);
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}

export function deleteAvatar() {
    return (dispatch) => {
        return deleteDataAction('/account/avatar')
            .then(data => {
                dispatch(actionCreator(actionType.DELETE_AVATAR_SUCCESS));

                return Promise.resolve(data);
            })
            .catch(error => {
                return Promise.reject(error);
            });
    }
}


export function updatePassword(model) {
    return dispatch => {

        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return aws.changePassword(model.oldPassword, model.newPassword)
            .then(() => {
                dispatch(actionCreator(actionType.REQUEST_END));
                return Promise.resolve();
            })
            .catch((e) => {
                return Promise.reject(e);
            })
    }
}

//* *************************************** *//

export function login(model) {
    return (dispatch) => {

        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        const email = model.email && model.email.toLowerCase();
        const password = model.password;

        return aws.signInUser(email, password)
            .then(response => {
                dispatch(actionCreator(actionType.HIDE_LOGIN_MODAL));
                dispatch(actionCreator(actionType.REQUEST_END));
            })
            .then(() => {
                return postDataAction('/account/signin');  // do this to pass token
            })
            .then(() => {
                return Promise.resolve();
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function logOut() {
    return dispatch => {
        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return aws.signOutUser()
            .then(() => {
                dispatch(actionCreator(actionType.REQUEST_END));
                return Promise.resolve();
            })
            .catch((error) => {
                dispatch(actionCreator(actionType.REQUEST_END));
                return Promise.reject(error);
            });
    };
}

export function register(model) {
    return dispatch => {

        dispatch(actionCreator(actionType.REQUEST_BEGIN));
        
        const email = model.email && model.email.toLowerCase();     
        
        return aws.signUpUser(email, model.password, model.firstName, model.lastName)
            .then(response => {
                //dispatch(hideRegistrationModal());
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(response);
            })
            .catch(error => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.reject(error);
            });
    }
}

export function verifyAccount(username, token) {
    return dispatch => {

        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return aws.confirmRegistration(username, token)
            .then(response => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(response);
            })
            .catch(error => {
                actionCreator(actionType.REQUEST_END);

                return Promise.reject(error);
            });
    }
}

export function resendVerificationCode(username) {
    return dispatch => {

        dispatch(actionCreator(actionType.REQUEST_BEGIN));

        return aws.resendConfirmationCode(username)
            .then(response => {
                dispatch(actionCreator(actionType.REQUEST_END));

                return Promise.resolve(response);
            })
            .catch(error => {
                actionCreator(actionType.REQUEST_END);

                return Promise.reject(error);
            });
    }
}