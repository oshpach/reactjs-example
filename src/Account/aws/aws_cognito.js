// AWS Cognito for authenticating user
// https://github.com/aws/amazon-cognito-identity-js
import {
    CognitoUserPool,
    CognitoUserAttribute,
    CognitoUser,
    AuthenticationDetails
} from 'amazon-cognito-identity-js';

// const uuid = require('uuid/v4');

const environment = process.env.REACT_APP_ENVIRONMENT || 'development';
const poolData = require(`./config.${environment}.js`);
const userPool = new CognitoUserPool(poolData);

export function signUpUser(email, password, firstName, lastName) {
    return new Promise((resolve, reject) => {
        try {
            const attributeList = [
                new CognitoUserAttribute({
                    Name: 'given_name',
                    Value: firstName,
                }),

                new CognitoUserAttribute({
                    Name: 'family_name',
                    Value: lastName,
                })
            ];

            userPool.signUp(email, password, attributeList, null, (error, result) => {
                if (error) {
                    return reject(error);
                }

                return resolve(result);
            });
        }
        catch (e) {
            return reject(e);
        }
    });
}

export function confirmRegistration(username, token) {
    return new Promise((resolve, reject) => {
        try {
            const userData = {
                Username: username,
                Pool: userPool
            };

            const cognitoUser = new CognitoUser(userData);

            cognitoUser.confirmRegistration(token, false, function (error, result) {
                if (error) {
                    return reject(error);
                }

                return resolve(result);
            });
        }
        catch (e) {
            return reject(e);
        }
    });
}

export function signInUser(username, password) {
    return new Promise((resolve, reject) => {
        try {
            const authenticationData = {
                Username: username,
                Password: password
            };

            const authenticationDetails = new AuthenticationDetails(authenticationData);

            const userData = {
                Username: username,
                Pool: userPool
            };

            const cognitoUser = new CognitoUser(userData);

            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: (result) => {
                    console.log('access token + ' + result.getAccessToken().getJwtToken());

                    return resolve(result);
                },

                onFailure: (error) => {
                    return reject(error);
                }
            });
        }
        catch (e) {
            return reject(e);
        }
    });
}

export function signOutUser() {
    return new Promise((resolve, reject) => {
        try {

            const cognitoUser = userPool.getCurrentUser();

            if (cognitoUser !== null) {
                cognitoUser.signOut();
            }

            return resolve();
        }
        catch (e) {
            return reject(e);
        }
    });
}

export function getIdToken() {
    return new Promise((resolve, reject) => {
        try {
            const cognitoUser = userPool.getCurrentUser();

            if (cognitoUser === null) {
                return reject();
            }

            cognitoUser.getSession(function (error, session) {
                if (error) {
                    return reject(error);
                }

                return resolve(session.getIdToken().getJwtToken());
            });
        }
        catch (e) {
            return reject(e);
        }
    })
}

export function getAccessToken() {
    return new Promise((resolve, reject) => {
        try {
            const cognitoUser = userPool.getCurrentUser();

            if (cognitoUser === null) {
                return reject;
            }

            cognitoUser.getSession(function (error, session) {
                if (error) {
                    return reject(error);
                }

                return resolve(session.getAccessToken().getJwtToken());
            });
        }
        catch (e) {
            return reject(e);
        }
    })
}

export function resendConfirmationCode(username) {
    return new Promise((resolve, reject) => {
        try {
            const userData = {
                Username: username,
                Pool: userPool
            }

            const cognitoUser = new CognitoUser(userData);

            cognitoUser.resendConfirmationCode(function (error, session) {
                if (error) {
                    return reject(error);
                }

                return resolve();
            });
        }
        catch (e) {
            return reject(e);
        }
    })
}

export function changePassword(oldPassword, newPassword) {
    return new Promise((resolve, reject) => {
        try {
            const cognitoUser = userPool.getCurrentUser();

            if (cognitoUser === null) {
                return reject();
            }

            cognitoUser.changePassword(oldPassword, newPassword, function (error, session) {
                if (error) {
                    return reject(error);
                }

                return resolve();
            });
        }
        catch (e) {
            return reject(e);
        }
    })
}

export function isUserAuthenticated() {
    const cognitoUser = userPool.getCurrentUser();
    return cognitoUser && cognitoUser.username !== '';
}